# Question and Answer Multimodal (QAM)
This is the code for the API side of QAM. It runs on node.js.

# Setting Up
1) Install node.js and npm (https://www.npmjs.com/get-npm) and ensure you can run `npm` and `node` commands via command line
2) Clone the code (`git clone https://github.com/Dartteon/QAM.git`) and navigate to project root via command line
3) In cmd line, run `npm run initdb`
4) In cmd line, run `npm start`
