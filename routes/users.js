const express = require('express');
const app = express();
const authController = require('../controllers/auth');
const userController = require('../controllers/user');
const sessionController = require('../controllers/session');

app.route('/api/users/invite-by-emails')
    .post(authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        userController.generateAccountsFromEmails);

app.route('/api/users')
    .post(userController.postUsers)
    .get(authController.localAuthenticated, userController.getUser);

app.route('/api/editUser')
    .post(userController.editUser)
    .get(authController.localAuthenticated, userController.getUser);

app.route('/api/verify/:code')
    .get(userController.verifyEmail);

app.route('/api/anon')
    .post(userController.generateAnonUserAndToken);

app.route('/api/login')
    .post(authController.localAuthenticated, userController.login);

app.route('/api/reset-password')
    .post(userController.resetPassword);

app.route('/api/users/getUsersFromSession')
    .post(userController.getUsersFromSession);

//Main authentication middleware
app.route('/api/authenticateJwt')
    .post(authController.jwtAuthenticated, userController.verifyJwtSession);

module.exports = app;