const express = require('express');
const app = express();
const authController = require('../controllers/auth');
const sessionController = require('../controllers/session');
const questionController = require('../controllers/question');

app.route('/api/questions')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserInSession,
        questionController.postQuestion
    );

app.route('/api/questions/:questionId/upvote')
    .post(
        authController.jwtAuthenticated,
        questionController.validateQuestionId,
        questionController.upvote
    );

app.route('/api/questions/:questionId/unvote')
    .post(authController.localAuthenticated,
        questionController.validateQuestionId,
        questionController.unvote
    );

app.route('/api/questions/markAnswered')
    .post(
        authController.jwtAuthenticated,
        // questionController.validateQuestionId,
        sessionController.validateUserIsAdmin,
        questionController.markAnswered
    );

app.route('/api/questions/:questionId')
    .get(questionController.validateQuestionId,
        questionController.getQuestion);

app.route('/api/questions/getQuestionsFromSession')
    .post(questionController.getQuestionsFromSession);

module.exports = app;