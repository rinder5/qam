const express = require('express');
const app = express();
const path = require('path');

app.route('/')
    .get(function(req, res){
        res.sendFile(path.join(__dirname, '../frontend/index.html'));
    });
app.route('/login')
    .get(function(req, res){
        res.sendFile(path.join(__dirname, '../frontend/login.html'));
    });
app.route('/verify/:code')
    .get(function(req, res){
        res.sendFile(path.join(__dirname, '../frontend/verify.html'));
    });
app.route('/create-session')
    .get(function(req, res){
        res.sendFile(path.join(__dirname, '../frontend/create-session.html'));
    });
app.route('/session/:sessionCode')
    .get(function(req, res){
        res.sendFile(path.join(__dirname, '../frontend/session.html'));
    });
app.route('/closedsession/:sessionCode')
    .get(function(req, res){
        res.sendFile(path.join(__dirname, '../frontend/closedsession.html'));
    });

module.exports = app;