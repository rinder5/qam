const express = require('express');
const app = express();
const authController = require('../controllers/auth');
const sessionController = require('../controllers/session');
const userController = require('../controllers/user');

app.route('/api/sessions/exists/:sessionAccessCode') //TODO: clean this link
    .get(sessionController.getDoesSessionExist);

app.route('/api/sessions')
    .post(
        authController.jwtAuthenticated,
        userController.verifyIsUserVerified,
        sessionController.postSession
    )
    .get(
        authController.jwtAuthenticated,
        sessionController.getSessionsOwned
    );

app.route('/api/viewSessions')
    .post(
        authController.jwtAuthenticated,
        sessionController.findSessionsOwned
    )

app.route('/api/sessions/findAdminUserSession')
    .post(sessionController.findAdminUserSession);

app.route('/api/sessions/set-invite-only')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        sessionController.setSessionIsInviteOnly
    );
app.route('/api/sessions/set-require-verification')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        sessionController.setSessionIsRequireVerification
    );
app.route('/api/sessions/set-voicechat-enabled')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        sessionController.setSessionIsVoicechatEnabled
    );
app.route('/api/sessions/set-require-voice-approval')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        sessionController.setSessionIsRequireVoiceApproval
    );
app.route('/api/session/sendVoiceConnectionRequest')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.requestVoiceConnection
    );
app.route('/api/session/closeVoiceConnectionRequest')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.closeVoiceConnection
    );
app.route('/api/session/kickVoiceConnectionRequest')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.kickVoiceConnection
    );
app.route('/api/session/acceptVoiceConnectionRequest')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.acceptVoiceConnection
    );
app.route('/api/sessions/change-dates')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        sessionController.setSessionDates
    );
app.route('/api/sessions/end-immediately')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        sessionController.endSession
    );

app.route('/api/sessions/send-session-report')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        sessionController.triggerSessionReport
    );

    
app.route('/api/sessions/updateUserSession')
    .post(authController.jwtAuthenticated, sessionController.updateUserSession);

app.route('/api/sessions/updateGroupSession')
    .post(authController.jwtAuthenticated, sessionController.updateGroupSession);

app.route('/api/sessions/:sessionId')
    .get(authController.localAuthenticated, sessionController.getSession);

app.route('/api/closedsession/:sessionId')
    .get(authController.localAuthenticated, sessionController.getSession);

app.route('/api/sessions/:sessionId/join')
    .post(authController.localAuthenticated, sessionController.addUserToSession);

module.exports = app;