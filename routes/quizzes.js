const express = require('express');
const app = express();
const authController = require('../controllers/auth');
const sessionController = require('../controllers/session');
const quizController = require('../controllers/quiz');

app.route('/api/submitquiz')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        quizController.postQuiz
    );
app.route('/api/createquiz')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        quizController.createQuiz
    );
app.route('/api/updatequiz')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        quizController.updateQuiz
    );
app.route('/api/requiz')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserIsAdmin,
        quizController.postRequiz
    );
app.route('/api/quizzes/response')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserInSession,
        quizController.postQuizResponse
    );
app.route('/api/quizzes/groupResponse')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserInSession,
        quizController.postGroupResponse
    );
app.route('/api/quizzes/getQuizzesFromSession')
    .post(
        quizController.getQuizzesFromSession
    );
app.route('/api/quizzes/getQuizzesFromSessionByGroup')
    .post(
        quizController.getQuizzesFromSessionByGroup
    );
app.route('/api/quizzes/getSpecificQuiz')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserInSession,
        quizController.getSpecificQuiz
    );
app.route('/api/quizzes/joingroup')
    .post(
        authController.jwtAuthenticated,
        sessionController.validateSessionExistence,
        sessionController.validateUserInSession,
        quizController.joinGroup
    );

module.exports = app;