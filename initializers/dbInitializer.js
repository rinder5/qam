const Sequelize = require("sequelize");
const config = require('../config/config.js');
const fs = require('fs');
const sqlScript = fs.readFileSync('qamDbInitialize.sql').toString();
const loggingEnabled = config.logging.enabled;

sqlScript
    .replace(/(\r\n|\n|\r)/gm," ") // remove newlines
    .replace(/\s+/g, ' ') // excess white space
    .split(";") // split into all statements
    .map(Function.prototype.call, String.prototype.trim)
    .filter(function(el) {return el.length != 0}); // remove any empty ones

const dbConfigs = {
    host: config.db_host,
    port: config.db_port,
    dialect: 'mysql',
    dialectOptions: {
        multipleStatements: true
    },
    logging: false
};

let sequelize = new Sequelize(
    config.database, config.db_username, config.db_password, dbConfigs
);

async function recreateDatabase() {
    const dropQuery = 'DROP DATABASE IF EXISTS ' + config.database;
    const dropQueryRes = await sequelize.query(dropQuery);
    if (!dropQueryRes[0].warningStatus && loggingEnabled) {
        console.log('QAM Database Dropped');
    }
    const createQuery = 'CREATE DATABASE IF NOT EXISTS ' + config.database;
    const createQueryRes = await sequelize.query(createQuery);
    if (!dropQueryRes[0].warningStatus && loggingEnabled) {
        console.log('QAM Database Created');
    }
}

//Needed since the initial sequelize connection didn't specify a database
async function recreateSequelizeConnection() {
    sequelize = await new Sequelize(
        config.database, config.db_username, config.db_password, dbConfigs
    );
}

//Not needed anymore, since we can just recreateDatabase()
async function dropTables() {
    const tableNames = ['quiz_option', 'quiz_response', 'quiz', 'user_session', 'vote', 'question' , 'session', 'user'];
    for (let i = 0; i < tableNames.length; i++) {
        const res = await sequelize.query('DROP TABLE IF EXISTS ' + tableNames[i] + ';');
        if (!res[0].warningStatus && loggingEnabled) {
            console.log(tableNames[i] + ' Table Dropped');
        }
    }
}

async function createTables() {
    //Execute from loaded SQL script
    await sequelize.query(sqlScript);
    if (loggingEnabled) {
        console.log('All Tables Created');
    }
}

async function execute() {
    await dropTables();
    // await recreateSequelizeConnection();
    await createTables();
    if (loggingEnabled) {
        console.log('Db Resetted - [' + config.database + ']');
    }
}

module.exports.execute = execute;
