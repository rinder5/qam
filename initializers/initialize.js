const dbInitializer = require('./dbInitializer.js');
const config = require('../config/config.js');

async function initialize() {
    printWarnings();
    await dbInitializer.execute();
    console.log('Initialize Successful');
    process.exit();
}

function printWarnings() {
    console.log('Initializing QAM...');
    console.log('Please ensure databases exist on MySQL server ['
        + config.configs.dev.database + '] , [' + config.configs.test.database + ']');
}

initialize();