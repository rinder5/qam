const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'dev'; // 'dev' | 'test' | 'production'
console.log('QAM Config Env = \'' + env + '\'');

const dev = {
    port : 80,

    database : "qam",
    db_username : "root",
    db_password : "123asd",
    db_host : "localhost",
    db_port : 3306,
    jwtSecret: "potato",
    jwtExpiresInSeconds: 10800,
    sequelizeLoggingEnabled: false,

    logging: {
        enabled: true
    },

    mailService: {
        shouldSendMail: true,
        sendgrid_api_key: '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',
        senderAddress : 'QAM <qam.nus@gmail.com>'
    }
};

const production = {
    port : 80,

    database : "qam",
    db_username : "root",
    db_password : "123asd",
    db_host : "localhost",
    db_port : 3306,
    jwtSecret: "potato",
    jwtExpiresInSeconds: 10800,
    sequelizeLoggingEnabled: false,

    logging: {
        enabled: true
    },

    mailService: {
        shouldSendMail: true,
        sendgrid_api_key : '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',
        senderAddress : 'QAM <qam.nus@gmail.com>'
    }
};

const test = {
    port : 3002,

    database : "qam_test",
    db_username : "root",
    db_password : "",
    db_host : "localhost",
    db_port : 3306,
    jwtSecret: "potato",
    jwtExpiresInSeconds: 10800,
    sequelizeLoggingEnabled: false,

    logging: {
        enabled: false
    },

    mailService: {
        shouldSendMail: false,
        sendgrid_api_key : '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',
        senderAddress : 'QAM <qam.nus@gmail.com>'
    }
};

const config = {
    dev,
    test,
    production
};

module.exports = config[env];
module.exports.configs = config;