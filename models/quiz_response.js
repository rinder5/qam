"use strict";

module.exports = function(sequelize, DataTypes) {
    const QuizResponse = sequelize.define("QuizResponse", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        quiz_id: DataTypes.INTEGER,
        user_id: DataTypes.INTEGER,
        text: DataTypes.STRING
    },{
        freezeTableName: true,
        tableName: 'quiz_response'
    });

    //Associations
    QuizResponse.associate = function(models) {
        QuizResponse.belongsTo(models.Quiz, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'quiz_id',
                allowNull: false
            }
        });

        QuizResponse.belongsTo(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'user_id',
                allowNull: false
            }
        });
    };
    return QuizResponse;
};
