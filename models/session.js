"use strict";
const UserSession = require('../models').UserSession;

module.exports = function(sequelize, DataTypes) {
    const Session = sequelize.define("Session", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        session_name: DataTypes.STRING,
        session_description: DataTypes.STRING,
        session_code: DataTypes.STRING,
        session_type: DataTypes.ENUM('QAM', 'BRANCHING_QUIZ', 'GROUP_QUIZ'),
        access_token: DataTypes.STRING,
        invite_only: DataTypes.BOOLEAN,
        require_verification: DataTypes.BOOLEAN,
        has_ended: DataTypes.BOOLEAN,
        start_time: DataTypes.DATE,
        end_time: DataTypes.DATE,
        voicechat_enabled: DataTypes.BOOLEAN,
        require_voice_approval: DataTypes.BOOLEAN
    },{
        freezeTableName: true,
        tableName: 'session'
    });

    Session.associate = function(models) {
        Session.hasMany(models.User, {
            through: UserSession,
            foreignKey: {
                name: 'id',
                allowNull: false
            }
        });
    };

    return Session;
};
