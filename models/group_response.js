"use strict";

module.exports = function(sequelize, DataTypes) {
    const GroupResponse = sequelize.define("GroupResponse", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        quiz_id: DataTypes.INTEGER,
        group_number: DataTypes.INTEGER,
        text: DataTypes.STRING
    },{
        freezeTableName: true,
        tableName: 'group_response'
    });

    //Associations
    GroupResponse.associate = function(models) {
        GroupResponse.belongsTo(models.Quiz, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'quiz_id',
                allowNull: false
            }
        });
    };
    return GroupResponse;
};
