"use strict";
const bcrypt = require('bcryptjs');
const UserSession = require('../models').UserSession;

module.exports = function(sequelize, DataTypes) {
    const User = sequelize.define("User", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        user_type: DataTypes.STRING,
        username: DataTypes.STRING,
        password: DataTypes.STRING,
        email: DataTypes.STRING,
        is_anonymous: DataTypes.BOOLEAN,
        is_verified: DataTypes.BOOLEAN
    },{
        freezeTableName: true,
        tableName: 'user'
    });

    User.prototype.verifyPassword = function(password, done) {
        bcrypt.compare(password, this.password, function(err, isMatch) {
            if (err) return done (err);
            done (null, isMatch);
        });
    };

    //Associations
    User.associate = function(models) {
        User.hasMany(models.Session, {
            through: UserSession,
            onDelete: "CASCADE",
            foreignKey: {
                name: 'id',
                allowNull: false
            }
        });

        User.hasMany(models.Question, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'user_id',
                allowNull: false
            }
        });

        User.hasMany(models.UserSession, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'user_id',
                allowNull: false
            }
        });
    };

    return User;
};
