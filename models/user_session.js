"use strict";

module.exports = function(sequelize, DataTypes) {
    const UserSession = sequelize.define("UserSession", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        user_id: DataTypes.INTEGER,
        session_id: DataTypes.INTEGER,
        user_group: DataTypes.INTEGER,
        next_question: DataTypes.INTEGER,
        relation_type: DataTypes.ENUM('ADMIN', 'SPEAKER', 'PARTICIPANT', 'INVITED_PARTICIPANT'),
        group_started: DataTypes.BOOLEAN
    },{
        freezeTableName: true,
        tableName: 'user_session'
    });

    UserSession.associate = function(models) {
        UserSession.belongsTo(models.Session, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'session_id',
                allowNull: false
            }
        });
        UserSession.belongsTo(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'user_id',
                allowNull: false
            }
        });
    };

    return UserSession;
};
