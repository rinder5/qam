"use strict";

module.exports = function(sequelize, DataTypes) {
    const Question = sequelize.define("Question", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        user_id: DataTypes.INTEGER,
        session_id: DataTypes.INTEGER,
        question_text: DataTypes.STRING,
        question_type: DataTypes.STRING,
        state: DataTypes.ENUM('NEW', 'ANSWERED'),
        question_mode: DataTypes.STRING,
    },{
        freezeTableName: true,
        tableName: 'question'
    });

    //Associations
    Question.associate = function(models) {
        Question.belongsTo(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'user_id',
                allowNull: false
            }
        });

        Question.belongsTo(models.Session, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'session_id',
                allowNull: false
            }
        });

        Question.hasMany(models.Vote, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'question_id',
                allowNull: false
            }
        });
    };

    return Question;
};
