"use strict";

module.exports = function(sequelize, DataTypes) {
    const QuizOption = sequelize.define("QuizOption", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        quiz_id: DataTypes.INTEGER,
        text: DataTypes.STRING,
        option_num: DataTypes.INTEGER,
        leading_question: DataTypes.INTEGER
    },{
        freezeTableName: true,
        tableName: 'quiz_option'
    });

    //Associations
    QuizOption.associate = function(models) {
        QuizOption.belongsTo(models.Quiz, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'quiz_id',
                allowNull: false
            }
        });
    };
    return QuizOption;
};
