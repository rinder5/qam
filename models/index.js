"use strict";

/*  ====================================================================
 *  This file organizes all models inside the models folder
 *      and initializes them into a database object.
 *
 *  It also settles all the associations between each model,
 *      so we don't have to implement association linking within models.
 *  ==================================================================== */

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const env = process.env.NODE_ENV || "development";
const config = require('../config/config.js');

const sequelize = new Sequelize(
    config.database, config.db_username, config.db_password, {
        host: config.db_host,
        port: config.db_port,
        dialect: 'mysql',
        logging: config.sequelizeLoggingEnabled,
        typeValidation: true
    }
);

const db = {};

fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function(file) {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
    // console.log('Model initialized - ' + modelName);
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

sequelize.authenticate().then(function (err) {
    if (err) {
        console.log('There is connection in ERROR');
    } else if (config.logging) {
        console.log('Connection has been established successfully');
    }
});

module.exports = db;