"use strict";

module.exports = function(sequelize, DataTypes) {
    const Quiz = sequelize.define("Quiz", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        session_id: DataTypes.INTEGER,
        text: DataTypes.STRING,
        type: DataTypes.ENUM('MCQ', 'MULTI_MCQ', 'OPEN_ENDED'),
        question_number: DataTypes.INTEGER,
        answer_by: DataTypes.DATE,
    },{
        freezeTableName: true,
        tableName: 'quiz'
    });

    //Associations
    Quiz.associate = function(models) {
        Quiz.belongsTo(models.Session, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'session_id',
                allowNull: false
            }
        });
        Quiz.hasMany(models.QuizOption)
        Quiz.hasMany(models.QuizResponse)
    };
    return Quiz;
};
