"use strict";

module.exports = function(sequelize, DataTypes) {
    const Vote = sequelize.define("Vote", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        user_id: DataTypes.INTEGER,
        question_id: DataTypes.INTEGER,
        is_upvote: DataTypes.BOOLEAN
    },{
        freezeTableName: true,
        tableName: 'vote'
    });

    Vote.associate = function(models) {
        Vote.belongsTo(models.Question, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'question_id',
                allowNull: false
            }
        });

        Vote.belongsTo(models.User, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'user_id',
                allowNull: false
            }
        });
    };

    return Vote;
};
