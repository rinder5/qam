"use strict";

module.exports = function(sequelize, DataTypes) {
    const GroupSession = sequelize.define("GroupSession", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        group_number: DataTypes.INTEGER,
        session_id: DataTypes.INTEGER,
        next_question: DataTypes.INTEGER
    },{
        freezeTableName: true,
        tableName: 'group_session'
    });

    GroupSession.associate = function(models) {
        GroupSession.belongsTo(models.Session, {
            onDelete: "CASCADE",
            foreignKey: {
                name: 'session_id',
                allowNull: false
            }
        });
    };

    return GroupSession;
};
