const socketManager = require('./socketManager.js');
const questionController = require('./question.js');
const quizController = require('./quiz.js');
const sessionController = require('./session.js');

exports.injectDependencies = async function () {
    questionController.injectSocketManager(socketManager);
    quizController.injectSocketManager(socketManager);
    sessionController.injectSocketManager(socketManager);
    console.log('Dependencies injected');
};