'use strict';
const nodemailer = require('nodemailer');
let transporter;
const config = require('../config/config.js').mailService;
const shouldSendMail = config.shouldSendMail;
const apiKey = config.sendgrid_api_key;
const sender = config.senderAddress;

/*
// Generate test SMTP service account from ethereal.email
// Only needed if you don't have a real mail account for testing
nodemailer.createTestAccount((err, account) => {
    // create reusable transporter object using the default SMTP transport
    transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'qam.nus@gmail.com',
            pass: 'nusqam99'
        }
    });

});
*/

exports.sendVerificationEmail = async function (user) {
    // console.log('sendVerificationEmail ' + JSON.stringify(user));
    const userId = user.id;
    const email = user.email;
    const code = user.code;
    // console.log('sendVerificationEmail code = ' + code);
    if (!userId || !email) {
      return;
    }
    const textMessage = 'Please click this link to verify your email at QAM\n ' + code;
    const htmlMessage = '<p>Dear ' + user.username + ',</p>' +
        '<p>Thank you for registering with QAM. Please click the following link to verify your account.</p>' +
        '<p><a href=\"' + code + '\">' + code + '</a></p>';

    const subject = '[QAM] Verify Email Account';
    sendMail(email, subject, textMessage, htmlMessage);
};

exports.sendUpdateEmail = async function (user) {
    // console.log('sendVerificationEmail ' + JSON.stringify(user));
    const userId = user.id;
    const email = user.email;
    const code = user.code;
    // console.log('sendVerificationEmail code = ' + code);
    if (!userId || !email) {
      return;
    }
    const textMessage = 'Please click this link to verify your email at QAM\n ' + code;
    const htmlMessage = '<p>Dear ' + user.username + ',</p>' +
        '<p>You have recently changed your linked email address to this one. Please click the following link to verify your account.</p>' +
        '<p><a href=\"' + code + '\">' + code + '</a></p>';

    const subject = '[QAM] Verify Email Account';
    sendMail(email, subject, textMessage, htmlMessage);
};

exports.sendNewAccountEmail = async function (email, password) {
    const textMessage = 'Your QAM account has been automatically generated. \n' +
        'Here are your login details for your generated QAM account - \n' +
        'Email: ' + email + '\n' +
        'Password: ' + password;

    const htmlMessage =
        '<p>Your QAM account has been automatically generated.</p>' +
        '<p>Here are your login details for your generated QAM account - </p>' +
        '<p>Email: ' + email + '</p>'+
        '<p>Password: ' + password + '</p>';

    const subject = '[QAM] Login credentials';
    sendMail(email, subject, textMessage, htmlMessage);
};

exports.sendSessionInvitationEmail = async function (email, sessionName, sessionDescription, link) {
    const textMessage =
        'You have been invited to a Question & Answer session! \n' +
        'Session: ' + sessionName + '\n' +
        'Description: ' + sessionDescription + '\n' +
        'Link: ' + link;

    const htmlMessage =
        '<p>You have been invited to a Question & Answer session!</p>' +
        '<p>' + 'Session: ' + sessionName + '</p>' +
        '<p>' + 'Description: ' + sessionDescription + '</p>' +
        '<p>' + 'Link: ' + link + '</p>';

    const subject = '[QAM] Q&A Session Invitation (' + sessionName + ')';
    sendMail(email, subject, textMessage, htmlMessage);
};

exports.sendRestPasswordEmail = async function (email, password) {
    const textMessage = 'Your QAM Password has been reset. \n' +
        'Here are your login details for your generated QAM account - \n' +
        'Email: ' + email + '\n' +
        'Password: ' + password;

    const htmlMessage =
        '<p>Your QAM Password has been reset. </p>' +
        '<p>Here are your login details for your generated QAM account - </p>' +
        '<p>Email: ' + email + '</p>'+
        '<p>Password: ' + password + '</p>';

    const subject = '[QAM] Login credentials';
    sendMail(email, subject, textMessage, htmlMessage);
};

exports.sendSessionReportEmail = async function (email, sessionName, sessionDescription, users, questions, quizzes) {
    const textMessage = 'Report for QAM Session ' + sessionName;

    let htmlMessage = '<p><b>Your Session Report</b></p>';
    htmlMessage += '<p><b>Title</b>: ' + sessionName + '<br />';
    htmlMessage += '<b>Description</b>: ' + sessionDescription + '</p>';

    htmlMessage += "<p></p>";

    //Questions
    htmlMessage += '<p><b>Questions from Audience</b><br />';
    for (let i = 0; i < questions.length; i++) {
        htmlMessage += JSON.stringify(questions[i]) + '<br />';
    }
    htmlMessage += "</p>";
    htmlMessage += "<p></p>";

    //Quizzes
    htmlMessage += '<p><b>Quizzes</b><br />';
    for (let i = 0; i < quizzes.length; i++) {
        htmlMessage += JSON.stringify(quizzes[i]) + '<br />';
    }
    htmlMessage += "</p>";
    htmlMessage += "<p></p>";

    //Users
    htmlMessage += '<p><b>Users in Session</b><br />';
    htmlMessage += '<b>Total</b>: ' + (users.registered.length + users.anonymous.length) + '<br />';
    htmlMessage += '<b>Anonymous Users</b>: ' + (users.anonymous.length) + '<br />';
    htmlMessage += '<b>Registered Users</b>: ' + (users.registered.length) + '<br />';
    for (let i = 0; i < users.registered.length; i++) {
        htmlMessage += JSON.stringify(users.registered[i]) + '<br />';
    }
    htmlMessage += "</p>";

    const subject = '[QAM] Report for QAM Session ' + sessionName;
    sendMail(email, subject, textMessage, htmlMessage);
};

const sendMail = async function (email, subject, plainText, htmlText) {
    if (!shouldSendMail) {
        return;
    }
    // using SendGrid's v3 Node.js Library
    // https://github.com/sendgrid/sendgrid-nodejs
    const sendgridMail = require('@sendgrid/mail');
    sendgridMail.setApiKey(apiKey);
    const mailContent = {
        to: email,
        from: sender,
        subject: subject,
        text: plainText,
        html: htmlText,
    };
    sendgridMail.send(mailContent);

    // previous email setup, using gmail
    /*
    // setup email data with unicode symbols
    let mailOptions = {
        from: '"QAM" <qam.nus@gmail.com>', // sender address
        to: email, // list of receivers
        subject: subject,
        text: plainText, // plain text body
        html: htmlText // html body
    };
    if (!transporter) {
        console.log('ERROR - Email Service failed to initialize');
        return;
    }
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
    });
    */
};