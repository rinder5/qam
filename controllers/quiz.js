const utils = require('./utils');
const User = require('../models').User;
const Session = require('../models').Session;
const UserSession = require('../models').UserSession;
const GroupSession = require('../models').GroupSession;
const Quiz = require('../models').Quiz;
const QuizOption = require('../models').QuizOption;
const QuizResponse = require('../models').QuizResponse;
const GroupResponse = require('../models').GroupResponse;
const Sequelize = require('sequelize');
const config = require('../config/config.js');
const sequelize = require('sequelize');

let socketManager;

//To trigger SocketManager to update all clients
exports.injectSocketManager = function (controller) {
    socketManager = controller;
};
const updateSocketQuizzesChanged = async function (sessionId) {
    if (!sessionId) return;
    socketManager.updateQuizzes(sessionId);
};
const socketPromptNewQuiz = async function (sessionId, quiz) {
    if (!sessionId) return;
    socketManager.promptNewQuiz(sessionId, quiz);
};
const socketPromptNewQuizTemplate = async function (sessionId, quiz) {
    if (!sessionId) return;
    socketManager.promptNewQuizTemplate(sessionId, quiz);
};
const updateAdminSocketQuizResponses = async function (quizId, quizType, sessionId) {
    if (!sessionId || !quizId) return;
    socketManager.updateAdminSocketQuizResponses(quizId, quizType, sessionId);
};
const updateGroupSocketQuiz = async function (sessionId, errorMessage, groupNumber) {
    if (!sessionId) return;
    socketManager.updateGroupSocketQuiz(sessionId, errorMessage, groupNumber);
};

exports.postQuiz = async function(req, res) {
    const sessionId = req.body.session_id;
    const user = req.headers.user;
    const userId = user.id;
    const text = req.body.text;
    const answerBy = req.body.answer_by;
    let options = req.body.options;
    const type = req.body.type;
    const isMcq = (type === 'MCQ' || type === 'MULTI_MCQ');

    console.log('Type = ' + type);
    if (isMcq && (!options || options.length < 2)) {
        res.status(400).send('Invalid number of options for MCQ (' + options.length + ')');
        return;
    }

    const createdQuiz = await Quiz.create({
        session_id: sessionId,
        text: text,
        type: type,
        answer_by: answerBy
    })
    .catch(e => res.status(500).send(e.message));

    if (isMcq) {
        for (let i = 0; i < options.length; i++) {
            await QuizOption.create({
                quiz_id: createdQuiz.id,
                text: options[i],
                option_num: i
            })
        }
    }

    //Append options before sending to clients
    const socketPromptQuiz = createdQuiz.toJSON();
    socketPromptQuiz.options = options;

    // updateSocketQuizzesChanged(sessionId);
    socketPromptNewQuiz(sessionId, socketPromptQuiz);
    res.send(createdQuiz);
};

exports.createQuiz = async function(req, res) {
    const sessionId = req.body.session_id;
    const user = req.headers.user;
    const userId = user.id;
    const text = req.body.text;
    const answerBy = req.body.answer_by;
    let options = req.body.options;
    const type = req.body.type;
    const isMcq = (type === 'MCQ' || type === 'MULTI_MCQ');
    const leadingTo = req.body.leading_to;
    const questionNumber= req.body.question_number;

    console.log('Type = ' + type);
    if (isMcq && (!options || options.length < 2)) {
        res.status(400).send('Invalid number of options for MCQ (' + options.length + ')');
        return;
    }

    let createdQuiz;

    if (leadingTo.length > 0) {
        createdQuiz = await Quiz.create({
            session_id: sessionId,
            text: text,
            type: type,
            answer_by: answerBy,
            question_number: questionNumber
        })
        .catch(e => res.status(500).send(e.message));
    } else {
        createdQuiz = await Quiz.create({
            session_id: sessionId,
            text: text,
            type: type,
            answer_by: answerBy
        })
        .catch(e => res.status(500).send(e.message));
    }

    if (isMcq) {
        if (leadingTo.length > 0)
            for (let i = 0; i < options.length; i++) {
                await QuizOption.create({
                    quiz_id: createdQuiz.id,
                    text: options[i],
                    option_num: i,
                    leading_question: leadingTo[i]
                })
            }
        else
            for (let i = 0; i < options.length; i++) {
                await QuizOption.create({
                    quiz_id: createdQuiz.id,
                    text: options[i],
                    option_num: i
                })
            }   
    }

    //Append options before sending to clients
    const socketPromptQuiz = createdQuiz.toJSON();
    socketPromptQuiz.options = options;

    // updateSocketQuizzesChanged(sessionId);
    socketPromptNewQuizTemplate(sessionId, socketPromptQuiz);
    res.send(createdQuiz);
};

exports.updateQuiz = async function(req, res) {
    const sessionId = req.body.session_id;
    const user = req.headers.user;
    const userId = user.id;
    const text = req.body.text;
    const answerBy = req.body.answer_by;
    let options = req.body.options;
    const type = req.body.type;
    const isMcq = (type === 'MCQ' || type === 'MULTI_MCQ');
    const leadingTo = req.body.leading_to;
    const questionNumber= req.body.question_number;
    const quiz_id = req.body.quiz_id;

    console.log('Type = ' + type);
    if (isMcq && (!options || options.length < 2)) {
        res.status(400).send('Invalid number of options for MCQ (' + options.length + ')');
        return;
    }

    let createdQuiz;

    createdQuiz = await Quiz.update(
        { text: text },
        { where: {session_id: sessionId, question_number: questionNumber}}
    )
    .catch(e => res.status(500).send(e.message));
    
    await QuizOption.destroy(
        { where: {quiz_id: quiz_id}}
    )

    for (let i = 0; i < options.length; i++) {
        await QuizOption.create({
            quiz_id: quiz_id,
            text: options[i],
            option_num: i,
            leading_question: leadingTo[i]
        })
    }

    updateSocketQuizzesChanged(sessionId);
    //socketPromptNewQuizTemplate(sessionId, socketPromptQuiz);
    res.send(createdQuiz);
};

exports.postRequiz = async function(req, res) {
    const sessionId = req.body.session_id;
    let options = req.body.options;
    const quizId = req.body.quiz_id;

    const foundQuiz = await Quiz.findOne({
        where: {session_id: sessionId,
        id: quizId}
    });

    if (!foundQuiz) {
        res.status(400).send('Could not find quiz');
        return;
    }

    const isMcq = (foundQuiz.type === 'MCQ' || foundQuiz.type === 'MULTI_MCQ');
    
    if (isMcq) {
        const foundOptions = await QuizOption.findAll({
            attributes: ['text'],
            where: {quiz_id: quizId}
        });
        options = [];
        for (let i=0; i<foundOptions.length; i++) {
            options[i] = foundOptions[i].text;
        }
    }
    //Append options before sending to clients
    const socketPromptQuiz = foundQuiz.toJSON();
    socketPromptQuiz.options = options;

    // updateSocketQuizzesChanged(sessionId);
    socketPromptNewQuiz(sessionId, socketPromptQuiz);
    res.send(foundQuiz);
};

exports.postQuizResponse = async function(req, res) {
    const quizId = req.body.quiz_id;
    const quizResponse = req.body.quiz_response;
    const user = req.headers.user;
    const userId = user.id;
    const sessionId = req.headers.session.id;
    const quiz = await Quiz.find({where:{id:quizId}});
    if (!quiz) {
        res.status(400).send('Quiz ID is invalid');
        return;
    }
    const quizType = quiz.type;
    if (quizType === 'MCQ' && isNaN(quizResponse)) {
        res.status(400).send('Response to MCQ must be integer');
        return;
    }

    const createdQuizResponse = await QuizResponse.create({
        quiz_id: quizId,
        user_id: userId,
        text: quizResponse
    })
    .catch(e => res.status(500).send(e.message));
    updateAdminSocketQuizResponses(quizId, quizType, sessionId)
    res.send(createdQuizResponse);
};

exports.postGroupResponse = async function(req, res) {
    const quizId = req.body.quiz_id;
    const quizResponse = req.body.quiz_response;
    const groupNumber = req.body.group_number;
    const sessionId = req.headers.session.id;
    const user = req.headers.user;
    const userId = user.id;
    const errorMessage = req.body.error_message;
    const quiz = await Quiz.find({where:{id:quizId}});
    if (!quiz) {
        res.status(400).send('Quiz ID is invalid');
        return;
    }
    const userSession = await UserSession.find({where:{user_id:userId, session_id:sessionId}});
    if (!quiz) {
        res.status(400).send('Quiz ID is invalid');
        return;
    }
    const quizType = quiz.type;
    if (quizType === 'MCQ' && isNaN(quizResponse)) {
        res.status(400).send('Response to MCQ must be integer');
        return;
    }

    const createdQuizResponse = await GroupResponse.create({
        quiz_id: quizId,
        group_number: userSession.user_group,
        text: quizResponse
    }).catch(e => res.status(500).send(e.message))
    .then(updateGroupSocketQuiz(sessionId, errorMessage, userSession.user_group));
    res.send(createdQuizResponse);
};

exports.getQuizzesFromSession = async function (req, res) {
    const sessionId = req.body.sessionId;
    const quizzes = await Quiz.findAll({
        where: { session_id: sessionId }
    });
    //Quiz options are ordered in quiz_id, option_num
    const quizOptions = await QuizOption.findAll({
        attributes: ['quiz_id', 'text', 'leading_question'],
        order: [['quiz_id', 'ASC'],['option_num', 'ASC']],
        include: [{
            model: Quiz,
            attributes: [],
            where: { session_id: sessionId }
        }]
    });

    //Populate quizzes with MCQ options
    let quizOptionsMapping = {};
    for (let i = 0; i < quizOptions.length; i++) {
        let option = quizOptions[i];
        let quizId = option.quiz_id;
        if (!quizOptionsMapping[quizId]) {
            quizOptionsMapping[quizId] = [];
        }
        quizOptionsMapping[quizId].push(option.text);
    }

    //get quiz responses
    const findClause = {
        include: [{
            model: Quiz,
            attributes: [],
            where: { 
                session_id: sessionId
            }
        }]
    };
    findClause.attributes =  ['quiz_id', 'text', [Sequelize.fn('COUNT', 'text'), 'Count']];
    findClause.group = ['quiz_id', 'text'];
    const quizResponses = await QuizResponse.findAll(findClause);

    const response = {
        quizzes: quizzes,
        quizOptions: quizOptionsMapping,
        quizResponses: quizResponses
    };

    res.send(response);
};

exports.joinGroup = async function(req, res) {
    const sessionId = req.body.session_id;
    const user = req.headers.user;
    const userId = user.id;
    let userGrouping = await UserSession.update(
        {group_started: 1},
        {where: { session_id: sessionId, user_id: userId}}
    );
    if (userGrouping == 0) { res.status(404).send("No user/session exists!"); return; }
    
    userGrouping = await UserSession.findOne({
        where: { session_id: sessionId, user_id: userId}
    });

    let groupSession = await GroupSession.findOne({
        where: { session_id: sessionId, group_number: userGrouping.user_group}
    });
    if (groupSession == null){
        GroupSession.create({
            next_question: 1,
            session_id: sessionId,
            group_number: userGrouping.user_group,
        });
    }
    res.send(true);
}

exports.getSpecificQuiz = async function (req, res) {
    const sessionId = req.body.session_id;
    const user = req.headers.user;
    const userId = user.id;
    const dbConfigs = {
        host: config.db_host,
        port: config.db_port,
        dialect: 'mysql',
        dialectOptions: {
            multipleStatements: true
        },
        logging: false
    };
    let seq = new sequelize(
        config.database, config.db_username, config.db_password, dbConfigs
    );
    
    let quizzes;

    let userGrouping = await UserSession.findAll({
        attributes: ['user_group', 'group_started'],
        where: { session_id: sessionId,
            user_id: userId},
    });

    let quizResponses;
    userGrouping = userGrouping[0];
    if (userGrouping.group_started == '0') {
        let query = "SELECT quiz.id, quiz.text, quiz.question_number ";
        query += "FROM quiz ";
        query += "INNER JOIN user_session ON user_session.session_id = quiz.session_id AND quiz.question_number = user_session.next_question ";
        query += "WHERE user_id = $user AND quiz.session_id = $session";
        await seq.query(query, {bind: {session: sessionId, user: userId} , type:sequelize.QueryTypes.SELECT})
        .then(function(result) {
            quizzes = result;
        });

        if (quizzes.length == 0) {
            const response = {
                quizzes: '',
                userGrouping: userGrouping
            };
            res.send(response); return;
        }
        quizzes = quizzes[0];

        quizResponses = await QuizResponse.findAll({
            attributes: ['text'],
            order: [['text', 'ASC']],
            where: { quiz_id: quizzes.id,
                user_id: userId},
        });
    } else if (userGrouping.group_started == '1') {
        let query = "SELECT quiz.id, quiz.text, quiz.question_number ";
        query += "FROM quiz ";
        query += "INNER JOIN group_session ON group_session.session_id = quiz.session_id AND quiz.question_number = group_session.next_question ";
        query += "WHERE group_number = $group AND quiz.session_id = $session";
        await seq.query(query, {bind: {session: sessionId, group: userGrouping.user_group} , type:sequelize.QueryTypes.SELECT})
        .then(function(result) {
            quizzes = result;
        });

        if (quizzes.length == 0) {
            const response = {
                quizzes: '',
                userGrouping: userGrouping
            };
            res.send(response); return;
        }
        quizzes = quizzes[0];

        quizResponses = await GroupResponse.findAll({
            attributes: ['text'],
            order: [['text', 'ASC']],
            where: { quiz_id: quizzes.id,
                group_number: userGrouping.user_group},
        });
    }

    //Quiz options are ordered in quiz_id, option_num
    const quizOptions = await QuizOption.findAll({
        attributes: ['quiz_id', 'text', 'option_num', 'leading_question'],
        order: [['option_num', 'ASC']],
        where: { quiz_id: quizzes.id},
    });

    //Populate quizzes with MCQ options
    let quizOptionsMapping = [];
    for (let i = 0; i < quizOptions.length; i++) {
        quizOptionsMapping.push(quizOptions[i]);
    }

    //Populate quizzes with MCQ options
    let quizResponsesMapping = [1, 1, 1, 1];
    for (let i = 0; i < quizResponses.length; i++) {
        quizResponsesMapping[quizResponses[i].text] = 0;
    }

    const response2 = {
        quizzes: quizzes,
        quizOptions: quizOptionsMapping,
        quizResponses: quizResponsesMapping,
        userGrouping: userGrouping
    };
    res.send(response2);
};

exports.getQuizzesFromSessionByGroup = async function (req, res) {
    const sessionId = req.body.sessionId;

    //get quiz responses
    
    let query = "SELECT quiz_response.quiz_id, quiz_response.text, user_group, COUNT(quiz_response.text) AS Count ";
    query += "FROM quiz_response ";
    query += "INNER JOIN user_session ON user_session.user_id = quiz_response.user_id AND user_session.session_id = $1 ";
    query += "INNER JOIN quiz ON quiz.id = quiz_response.quiz_id ";
    query += "WHERE quiz.session_id = $1 ";
    query += "GROUP BY quiz_response.quiz_id, quiz_response.text, user_group";

    const dbConfigs = {
        host: config.db_host,
        port: config.db_port,
        dialect: 'mysql',
        dialectOptions: {
            multipleStatements: true
        },
        logging: false
    };
    let sequelize = new Sequelize(
        config.database, config.db_username, config.db_password, dbConfigs
    );
    sequelize.query(query, {bind: [sessionId] , type:Sequelize.QueryTypes.SELECT})
        .then(function(result) {
            const response = {
                quizResponses: result
            };
            res.send(response)
    });
};