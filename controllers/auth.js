const passport = require('passport');
// const BasicStrategy = require('passport-http').BasicStrategy;
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models').User;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const config = require('../config/config.js');

passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with username
        usernameField : 'username',
        passwordField : 'password',
        session:false,
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, usernameOrEmail, password, callback) { // callback with username and password from our form
       let whereClause = {};
       if (usernameOrEmail.indexOf('@') > 0) {
           whereClause.email = usernameOrEmail;
       } else {
           whereClause.username = usernameOrEmail;
       }
        User.findOne({ where: whereClause})
            .then(function (user, err) {
                if (!user) {
                    return callback(null, false);
                }
                user.verifyPassword(password, function(err, isMatch) {
                    if (err) { return callback(err, false); }

                    if (!isMatch) { return callback(null, false); }

                    req.headers.user = user;
                    // Success
                    return callback(null, user);
                });
        });
    }
));

const jwtStrategyOptions = {
    jwtFromRequest : ExtractJwt.fromBodyField('sessionToken'),
    secretOrKey : config.jwtSecret,
    passReqToCallback : true
};
passport.use('jwt-login', new JwtStrategy(jwtStrategyOptions,
    async function(req, jwt_payload, done_callback) {
        if (!jwt_payload) {
            return done_callback (null, false);
        }
        const user = await User.findById(jwt_payload.userId);
        if (user) {
            req.headers.decodedToken = jwt_payload;
            req.headers.user = user;
            return done_callback (null, user);
        }
        return done_callback (null, false);
    }
));

passport.serializeUser(function(user, done) {
    // console.log('serializeUser ' + JSON.stringify(user));
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    // console.log('deserializeUser ' + JSON.stringify(user));
    done(null, user);
});

exports.localAuthenticated = passport.authenticate('local-login');
exports.jwtAuthenticated = passport.authenticate('jwt-login');