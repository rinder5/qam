const User = require('../models').User;
const Session = require('../models').Session;
const Question = require('../models').Question;
const UserSession = require('../models').UserSession;
const Vote = require('../models').Vote;
const utils = require('./utils');
let socketManager;

//To trigger SocketManager to update all clients
exports.injectSocketManager = function (controller) {
  socketManager = controller;
};
const updateSocketQuestionsChanged = async function (sessionId) {
    if (!sessionId) return;
    const allQuestionsInSession = await getQuestionsInSession(sessionId);
    socketManager.updateQuestions(sessionId, allQuestionsInSession);
};

exports.validateQuestionId = async function(req, res, next) {
    const questionId = req.body.questionId;
    if (!questionId) {
        res.status(400).send('Validation Fail: Invalid questionId ' + questionId);
        return;
    }
    let question = await Question.findOne(
        { where: { id: questionId } }
    );
    if (!question) {
        res.status(400).send('Validation Fail (DB): Invalid questionId ' + questionId);
        return;
    }
    req.headers.question = question;
    next();
};

// Create endpoint /api/users for POST
exports.postQuestion = async function(req, res) {
    const sessionId = req.body.session_id;
    const question_mode = req.body.question_mode;
    const question_type = req.body.question_type;
    const question_text = req.body.question_text;
    const userId = req.headers.user.id;

    if (!question_text) {
        res.status(400).send('Invalid question text');
        return;
    }
    const session = await Session.findOne({
        where: { id: sessionId }
    });
    if (!session) {
        res.status(400).send('Invalid session_id');
        return;
    }

    const createdQuestion = await Question.create({
        user_id: userId,
        session_id: sessionId,
        question_text: question_text,
        question_mode: question_mode,
        question_type: question_type
    })
    .catch((err) => {
        res.status(500).send('Failed to create Question - ' + err);
    });
    await updateSocketQuestionsChanged(sessionId);
    res.send({status: 'success', question: createdQuestion});
};

// Create endpoint /api/users for GET
exports.getQuestion = async function(req, res) {
    const questionId = req.params.questionId;

    //Query again to get related Users, Sessions and Vote count
    let question = await Question.findOne(
        {
            where: { id: questionId },
            include: [
                { model: User, attributes: ['id', 'username'] },
                { model: Session },
                { model: Vote, attributes: [] }
            ]
        }
    );

    //Count number of votes and add to question JSON
    const votes = await Vote.count({
        where: {
            question_id: questionId
        }
    });

    question = question.toJSON();
    question.upvotes = votes;
    res.send(question);
};

// Create endpoint /api/users for GET
exports.getQuestionsOwned = async function(req, res) {
    let username = "";
    if (req.headers.authorization) {
        username = utils.decodeAuthUsername(req.headers.authorization);
    } else {
        res.status(400).send('Unauthorized');
        return;
    }

    const owner = await User.findOne({
        attributes: ['id'],
        where: { username: username }
    });
    const ownerId = owner.id;
    const ownedSessionRelations = await UserSession.findAll({
        attributes: ['id', 'relation_type'],
        where: {
            user_id: ownerId,
            relation_type: ["SPEAKER", "ADMIN"]
        }
    });
    const ownedSessionIds = ownedSessionRelations.map(function (ownedSessionRelation) {
        return ownedSessionRelation.id;
    });

    const ownedSessions = await Session.findAll(
        {
            where: {
                id: ownedSessionIds
            }
        }
    );
    res.send(ownedSessions);
};

exports.unvote = async function(req, res) {
    const questionId = req.params.questionId;

    //Check if user is valid
    const username = utils.decodeAuthUsername(req.headers.authorization);
    if (!username) {
        res.status(400).send('Unauthorized');
        return;
    }
    const user = await User.findOne({
        attributes: ['id'],
        where: { username: username }
    });
    const userId = user.id;

    const destroyCall = await Vote.destroy({
        where: {
            user_id: userId,
            question_id: questionId
        }
    })
    .catch((err) => {
        res.status(500).send('Failed to destroy Vote - ' + err);
    });
    await updateSocketQuestionsChanged(sessionId);

    res.send('Unvote request was ' + (destroyCall ? 'successful' : 'unsuccessful'));
};
exports.upvote = async function(req, res) {
    const questionId = req.body.questionId;
    const userId = req.headers.user.id;
    
    const question = await Question.findOne({
        where: { id: questionId }
    });

    if (!question) {
        res.status(400).send('Question does not exist');
        return;
    }

    const sessionId = question.session_id;

    if (question) {
        //Check if user has already upvoted question before
        const previousUpvote = await Vote.findOne({
            where: { user_id: userId, question_id: questionId }
        });
        if (previousUpvote) {
            res.status(400).send('User has already upvoted this question');
            return;
        }

        const createdVote = await Vote.create({
            user_id: userId,
            question_id: questionId,
            is_upvote: true
        })
        .catch((err) => {
            res.status(500).send('Failed to create Vote - ' + err);
        });
        await updateSocketQuestionsChanged(sessionId);
        res.send('Upvote success ' + JSON.stringify(createdVote));
    }

};
exports.markAnswered = async function(req, res) {
    const questionId = req.body.questionId;
    const userId = req.headers.user.id;

    const question = await Question.findOne({
        where: { id: questionId }
    });

    if (!question) {
        res.status(400).send('Question does not exist');
        return;
    }

    const sessionId = question.session_id;
    //Check if user is part of session and ADMIN
    const userSession = await UserSession.findOne({
        where: {
            session_id: sessionId,
            user_id: userId,
            relation_type: 'ADMIN'
        }
    });
    if (!userSession) {
        res.status(400).send('User is not admin of session');
        return;
    }

    if (question) {
        const test = await question.updateAttributes({
            state: 'ANSWERED'
        })
        .catch((err) => {
            res.status(500).send('Failed to update Question - ' + err);
        });
        await updateSocketQuestionsChanged(sessionId);
        res.send('Question marked as answered');
    }

};

const getQuestionsInSession = async function (sessionId) {
    return await Question.findAll ({
        where: {session_id: sessionId},
        include: [
            {
                model: User,
                attributes: ['username']
            },
            {
                model: Vote,
                attributes: ['user_id']
            }
        ]
    });
};

const getQuestionsFromSession = async function (req, res) {
    const sessionId = req.body.sessionId;
    const questions = await Question.findAll ({
        where: {session_id: sessionId},
        include: [
            {
                model: User,
                attributes: ['username']
            },
            {
                model: Vote,
                attributes: ['user_id']
            }
        ]
    });
    res.send(questions);
    return questions;
};

exports.getQuestionsInSession = getQuestionsInSession;
exports.getQuestionsFromSession = getQuestionsFromSession;