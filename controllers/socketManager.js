/**
 * This class manages all sockets to clients
 *
 * It maps users to respective sessions
 *
 * Each active user <-> session will have one socket
 */

let io;
const sessions = {};
const sockets = {};
const config = require('../config/config.js');
const jwt = require('jsonwebtoken');
const questionController = require('./question');
const sessionController = require('./session');
const UserSession = require('../models').UserSession;
const GroupSession = require('../models').GroupSession;
const Session = require('../models').Session;
const User = require('../models').User;
const Quiz = require('../models').Quiz;
const QuizResponse = require('../models').QuizResponse;
const QuizOption = require('../models').QuizOption;
const sequelize = require('sequelize');

exports.initialize = async function (_io) {
    io = _io;
    io.sockets.on('connection', async function (socket) {
        socket.on('enterSession', async function (data) {
            onSocketEnterSession(socket, data);
        });

        socket.on('disconnect', async function (data) {
            onSocketDisconnect(socket, data);
        });

        socket.on('message', async function (data) {
            console.log('ServerSideMessage = ' + JSON.stringify(data));
        });
    });
};

async function onSocketEnterSession(socket, data) {
    if (socket.userId && socket.sessionId && socket.role) {
        //When user logs into another account
        removeUserFromSession(socket.userId, socket.sessionId);
    }
    const decodedUser = authenticateConnection (socket, data);
    const user = await User.findOne({where: {id: decodedUser.userId}});

    if (!user || user.username !== decodedUser.username) {
        socket.emit('forceReAuthAndRefresh', 'UserId does not exist');
        return undefined;
    }

    if (decodedUser) {
        const sessionId = data.sessionId;
        socket.sessionId = data.sessionId;
        socket.userId = decodedUser.userId;

        let userSession = await UserSession.findOne({
            where: {
                user_id: decodedUser.userId,
                session_id: sessionId
            }
        });

        const session = await sessionController.getSession(sessionId);

        if (!session) {
            socket.emit('enterSessionFailure', 'Session does not exist');
            return;
        }

        if (session.invite_only) {
            if (!userSession ||
                (userSession.relation_type !== "ADMIN" && userSession.relation_type !== "INVITED_PARTICIPANT")) {
                socket.emit('enterSessionFailure', 'This is an invite-only session. Please use an invited account!');
                return;
            }
        }

        if (session.require_verification) {
            const user = await User.findOne({
                where: {id: decodedUser.userId}
            });
            if (!user.is_verified) {
                let errorMsg;
                if (user.is_anonymous) {
                    errorMsg = 'You need a verified account to access this session. Please register by clicking the LOGIN button.';
                } else {
                    errorMsg = 'Please verify your account by clicking on the link in the email sent to you, then refresh this page.';
                }
                socket.emit('enterSessionFailure', errorMsg);
                return;
            }
        }

        if (!userSession) {
            userSession = await UserSession.create({
                relation_type: 'PARTICIPANT',
                user_id: decodedUser.userId,
                session_id: sessionId
            });
        }

        let role = userSession.relation_type;

        socket.user = decodedUser;
        socket.role = role;
        socket.join(sessionId);

        await addUserToSession (decodedUser, sessionId, socket.id);
        updateUsersListInSession (sessionId);
        updateOneSocketWithSessionData (socket, sessionId);
        await sessionController.socketAddUserToSession(decodedUser.id, sessionId);
        if (userSession && userSession.relation_type === 'ADMIN') {
            sessions[sessionId].adminSocket = socket;
            socket.emit('setAdmin', { isAdmin: true });
            const adminData = {
                invite_only: session.invite_only,
                require_verification: session.require_verification
            };
            socket.emit('updateAdminData', adminData);
            await updateQuizzes(sessionId);
            await pushAdminSocketAllQuizResponses(sessionId);
        }
        socket.emit('message', 'Successfully connected');
        socket.emit('enterSessionSuccess', 'Successfully connected');
    } else {
        socket.emit ('message', 'Session expired - please login again');
        socket.emit('enterSessionFailure', 'Session expired - please login again');
    }
}

function onSocketDisconnect (socket, data) {
    if (socket.userId && socket.sessionId) {
        if (removeUserFromSession(socket.userId, socket.sessionId)) {
            updateUsersListInSession(socket.sessionId);
        }
        if (sockets[socket.id]) {
            delete sockets[socket.id];
        }
    }
}

async function createSession(sessionId) {
    const session = await Session.findOne({
        where: { id: sessionId }
    });
    if (session.has_ended) {
        console.log('session has ended');
        return;
    }

    //Find all questions pertaining to sessionId
    const existingQuestions = await questionController.getQuestionsInSession(sessionId);
    sessions[sessionId] = {
        id: sessionId,
        users: {},
        participant: {},
        questions: existingQuestions,
        adminSocket: undefined
    }
}

function kickAllUsersAndCloseSession (sessionId) {
    io.sockets.in(sessionId).emit('notifySessionEnd', {});
    var clients = io.of(sessionId).clients();
    sessions[sessionId] = undefined;
    for (let i = 0; i < clients.length; i++) {
        clients[i].disconnect();
    }
}

function removeUserFromSession (userId, sessionId) {
    if (sessions[sessionId] && sessions[sessionId]['users']) {
        if (sessions[sessionId]['users'][userId]) {
            //Count is to allow users to have multiple tabs
            sessions[sessionId]['users'][userId].count -= 1;
            if (!sessions[sessionId]['users'][userId].count) {
                delete sessions[sessionId]['users'][userId];
                return true;
            }
        }
        return false;
    }
}

async function addUserToSession (user, sessionId, socketId) {
    if (!sessionId) {
        console.log('ERROR! SessionId UNDEFINED! User = ' + JSON.stringify(user));
    }
    const userId = user.userId;

    if (!sessions[sessionId]) {
        await createSession(sessionId);
    }
    if (sessions[sessionId]['users'][userId] &&
        sessions[sessionId]['users'][userId].socketId === socketId) {
        return false;
    }

    if (sessions[sessionId]['users'][userId]) {
        sessions[sessionId]['users'][userId].count++;
        return true;
    } else {

        const userDetails = await UserSession.findOne({
            where: { user_id : userId, session_id : sessionId} 
        });
        let userGroup = 0;
        if (userDetails)
            userGroup = userDetails.user_group;

        sessions[sessionId]['users'][user.userId] = {
            username: user.username,
            socketId: socketId,
            count: 1,
            isAnonymous: user.is_anonymous,
            is_verified: user.is_verified,
            user_group: userGroup
        };
        sockets[socketId] = {
            userId: userId,
            sessionId: sessionId,
            user_group: userGroup
        };
        return true;
    }
}

function authenticateConnection (socket, data) {
    let decoded;
    try {
        //Authenticate user via token
        const token = data.sessionToken;
        decoded = jwt.verify(token, config.jwtSecret);
        return decoded;
    } catch(err) {
        const errObj = {
          message: 'Failed to authenticate socket - ' + err
        };
        // disconnectSocket(socket, errObj);
        return undefined;
    }
}

const updateOneSocketWithSessionData = function (socket, sessionId) {
    const users = sessions[sessionId].users;
    socket.emit ('updateUsersList', users);
    const questions = sessions[sessionId].questions;
    socket.emit('updateQuestions', questions);
}
const updateUsersListInSession = function (sessionId) {
    if (sessions[sessionId] && sessions[sessionId].adminSocket) {
        const users = sessions[sessionId].users;
        sessions[sessionId].adminSocket.emit('updateUsersList', users);
    }
};
const updateQuestions = function(sessionId, questions) {
    if (sessions[sessionId]) {
        sessions[sessionId].questions = questions;
    }
    io.sockets.in(sessionId).emit('updateQuestions', questions);
};
const updateQuizzes = async function(sessionId) {
    const quizzes = await Quiz.findAll({
        where: { session_id: sessionId }
    });
    //Quiz options are ordered in quiz_id, option_num
    const quizOptions = await QuizOption.findAll({
        attributes: ['quiz_id', 'text', 'leading_question'],
        order: [['quiz_id', 'ASC'],['option_num', 'ASC']],
        include: [{
            model: Quiz,
            attributes: [],
            where: { session_id: sessionId }
        }]
    });

    //Populate quizzes with MCQ options
    let quizOptionsMapping = {};
    let quizLeadingMapping = {};
    for (let i = 0; i < quizOptions.length; i++) {
        let option = quizOptions[i];
        let quizId = option.quiz_id;
        if (!quizOptionsMapping[quizId]) {
            quizOptionsMapping[quizId] = [];
            quizLeadingMapping[quizId] = [];
        }
        quizOptionsMapping[quizId].push(option.text);
        quizLeadingMapping[quizId].push({text:option.text, lead:option.leading_question});
    }
    const response = {
        quizzes: quizzes,
        quizOptions: quizOptionsMapping,
        quizLeading: quizLeadingMapping
    };

    io.sockets.in(sessionId).emit('updateAdminQuizzes', response);
};
const promptNewQuiz = async function(sessionId, quiz) {
    io.sockets.in(sessionId).emit('promptQuiz', quiz);
    //Update quizzes list for admin
    updateQuizzes(sessionId);
};
const promptNewQuizTemplate = async function(sessionId, quiz) {
    //Update quizzes list for admin
    updateQuizzes(sessionId);
};
const pushAdminSocketAllQuizResponses = async function(sessionId) {
    if (!sessions[sessionId] || !sessions[sessionId].adminSocket) {
        //Session does not exist yet, or no admin in session
        return;
    }
    let query = "SELECT quiz_response.quiz_id, quiz_response.text, user_group, COUNT(quiz_response.text) AS Count ";
    query += "FROM quiz_response ";
    query += "INNER JOIN user_session ON user_session.user_id = quiz_response.user_id AND user_session.session_id = $1 ";
    query += "INNER JOIN quiz ON quiz.id = quiz_response.quiz_id ";
    query += "WHERE quiz.session_id = $1 ";
    query += "GROUP BY quiz_response.quiz_id, quiz_response.text, user_group";

    const dbConfigs = {
        host: config.db_host,
        port: config.db_port,
        dialect: 'mysql',
        dialectOptions: {
            multipleStatements: true
        },
        logging: false
    };
    let seq = new sequelize(
        config.database, config.db_username, config.db_password, dbConfigs
    );
    seq.query(query, {bind: [sessionId] , type:sequelize.QueryTypes.SELECT})
        .then(function(result) {
            const response = {
                quizGroupResponses: result
            };
            sessions[sessionId].adminSocket.emit('updateQuizGroupResponse', response);
    });

    const findClause = {
        include: [{
            model: Quiz,
            attributes: [],
            where: { session_id: sessionId }
        }]
    };
    findClause.attributes =  ['quiz_id', 'text', [sequelize.fn('COUNT', 'text'), 'Count']];
    findClause.group = ['quiz_id', 'text'];
    const quizResponses = await QuizResponse.findAll(findClause);
    sessions[sessionId].adminSocket.emit('updateQuizResponse', quizResponses);
};
const updateAdminSocketQuizResponses = async function(quizId, quizType, sessionId) {
    if (!sessions[sessionId] || !sessions[sessionId].adminSocket) {
        //Session does not exist yet, or no admin in session
        return;
    }
    let query = "SELECT quiz_response.quiz_id, quiz_response.text, user_group, COUNT(quiz_response.text) AS Count ";
    query += "FROM quiz_response ";
    query += "INNER JOIN user_session ON user_session.user_id = quiz_response.user_id AND user_session.session_id = $1 ";
    query += "INNER JOIN quiz ON quiz.id = quiz_response.quiz_id ";
    query += "WHERE quiz.session_id = $1 ";
    query += "GROUP BY quiz_response.quiz_id, quiz_response.text, user_group";

    const dbConfigs = {
        host: config.db_host,
        port: config.db_port,
        dialect: 'mysql',
        dialectOptions: {
            multipleStatements: true
        },
        logging: false
    };
    let seq = new sequelize(
        config.database, config.db_username, config.db_password, dbConfigs
    );
    seq.query(query, {bind: [sessionId] , type:sequelize.QueryTypes.SELECT})
        .then(function(result) {
            const response = {
                quizGroupResponses: result
            };
            sessions[sessionId].adminSocket.emit('updateQuizGroupResponse', response);
    });

    const findClause = {
        where:{quiz_id : quizId}
    };
    findClause.attributes =  ['quiz_id', 'text', [sequelize.fn('COUNT', 'text'), 'Count']];
    findClause.group = ['text'];
    const quizResponses = await QuizResponse.findAll(findClause);
    sessions[sessionId].adminSocket.emit('updateQuizResponse', quizResponses);
};
const updateGroupSocketQuiz = async function(sessionId, errorMessage, groupNumber) {
    if (!sessions[sessionId]) {
        //Session does not exist yet
        return;
    }
    const response = {errorMessage: errorMessage, groupNumber: groupNumber};
    io.sockets.in(sessionId).emit('updateGroupResponses', response);
};
const forceAllSocketsRefresh = function (sessionId) {
    if (!sessions[sessionId] || !sessions[sessionId].adminSocket) {
        //Session does not exist yet, or no admin in session
        return;
    }
    io.sockets.in(sessionId).emit('forceRefresh', {});
};
const kickVoiceConnection = function (sessionId, userId) {
    if (!sessions[sessionId] || !sessions[sessionId].adminSocket) {
        return;
    }
    io.sockets.in(sessionId).emit('kickVoiceConnection', userId);
};
const requestVoiceConnection = function (sessionId, userId) {
    if (!sessions[sessionId] || !sessions[sessionId].adminSocket) {
        return;
    }
    sessions[sessionId].adminSocket.emit('requestVoiceConnection', userId);
};
const acceptVoiceConnection = function (sessionId, userId) {
    if (!sessions[sessionId] || !sessions[sessionId].adminSocket) {
        return;
    }
    io.sockets.in(sessionId).emit('acceptVoiceConnection', userId);
};
const closeVoiceConnection = function (sessionId, userId) {
    if (!sessions[sessionId] || !sessions[sessionId].adminSocket) {
        return;
    }
    sessions[sessionId].adminSocket.emit('closeVoiceConnection', userId);
};

exports.updateQuestions = updateQuestions;
exports.updateQuizzes = updateQuizzes;
exports.promptNewQuiz = promptNewQuiz;
exports.promptNewQuizTemplate = promptNewQuizTemplate;
exports.updateAdminSocketQuizResponses = updateAdminSocketQuizResponses;
exports.updateGroupSocketQuiz = updateGroupSocketQuiz;
exports.forceAllSocketsRefresh = forceAllSocketsRefresh;
exports.kickAllUsersAndCloseSession = kickAllUsersAndCloseSession;
exports.kickVoiceConnection = kickVoiceConnection;
exports.requestVoiceConnection = requestVoiceConnection;
exports.acceptVoiceConnection = acceptVoiceConnection;
exports.closeVoiceConnection = closeVoiceConnection;














function forceDisconnectSocket (socket, err) {
    socket.emit ('disconnect', err);
    socket.disconnect();
}

exports.addAuthedUser = function (userId, sessionToken) {
    console.log('addAuthedUser ID[' + userId + '] TOKEN[' + sessionToken + ']');
    authedUsers[userId] = sessionToken;
};