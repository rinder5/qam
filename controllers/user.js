const utils = require('./utils');
const mailer = require('./mailer');
const config = require('../config/config.js');
const User = require('../models').User;
const UserSession = require('../models').UserSession;
const Session = require('../models').Session;
const Sequelize = require('sequelize');

const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);
const REGEX_USERNAME = /^[a-zA-Z0-9]+$/;

// const socketManager = require('./socketManager');
const jwt = require('jsonwebtoken');

exports.login = async function(req, res, next) {
    //Injected user object by auth
    const user = req.headers.user;

    // //Requires sessionId as one JWT token is signed for each session
    // const sessionId = req.body.sessionId;
    // const strippedUser = {
    //     userId: user.id,
    //     sessionId: sessionId,
    //     username: user.username,
    //     is_anonymous: false
    // };
    // const token = jwt.sign(strippedUser, config.jwtSecret, {expiresIn : config.jwtExpiresInSeconds});
    const token = await signJwtToken(user);
    res.send({
        status: 'success',
        userId: user.id,
        username: user.username,
        sessionId: -1,  //TODO: Remove this useless data
        isAnonymous: false,
        email: user.email,
        sessionToken: token
    });
};
const signJwtToken = async function (user) {
    const strippedUser = {
        userId: user.id,
        sessionId: -1,  //TODO: Remove this useless data
        username: user.username,
        email: user.email,
        is_anonymous: false,
        is_verified: user.is_verified
    };
    const token = jwt.sign(strippedUser, config.jwtSecret, {expiresIn : config.jwtExpiresInSeconds});
    return token;
};

exports.verifyIsUserVerified = async function(req, res, next) {
    const user = req.headers.user;
    if (!user.is_verified) {
        res.status(403).send('User is not verified - this action requires a verified account');
        return;
    }
    next();
};

exports.verifyJwtSession = async function(req, res, next) {
    //Injected user object by passport
    const decodedToken = req.headers.decodedToken;
    const sessionId = decodedToken.sessionId;
    const user = req.headers.user;
    res.send({
        status: 'success',
        userId: decodedToken.userId,
        username: user.username,
        sessionId: sessionId,
        email: user.email,
        isAnonymous: false,
        isVerified: user.is_verified
    });
};

exports.generateAnonUserAndToken = async function (req, res, next){
    let anonUser;
    const sessionId = req.body.sessionId;
    //First check if anonymous userId is passed in body,
    //re-use it by generating a new token with specified session
    const anonToken = req.body.anonSessionToken;
    if (anonToken) {
        try {
            const decoded = jwt.verify(anonToken, config.jwtSecret);
            if (decoded.userId) {
                const userId = decoded.userId;
                if (userId) {
                    const existingAnonUser = await User.findOne({where:{id: userId}});
                    if (existingAnonUser && existingAnonUser.is_anonymous) {
                        anonUser = existingAnonUser;
                    }
                }
            }
        } catch(err) {
            // JWT invalid
        }
    }

    //If an existing anon user wasn't found earlier, create a new one
    if (!anonUser) {
        anonUser = await createAnonUser();
    }
    const strippedUser = {
        userId: anonUser.id,
        sessionId: sessionId,
        username: anonUser.username,
        is_anonymous: true
    };

    const expiresIn = 60*60*24*365; //Anon users token never expire
    const token = jwt.sign(strippedUser, config.jwtSecret, {expiresIn : expiresIn});
    res.send({
        status: 'success',
        userId: strippedUser.userId,
        username: strippedUser.username,
        sessionId: sessionId,
        isAnonymous: true,
        sessionToken: token
    });
};
// exports.generateAnonTokenForNewSession = async function (req, res, next){
//     const userId = req.body.userId;
//     if (!userId) {
//
//     }
// };
exports.getUsersFromSession = async function (req, res) {
    const sessionId = req.body.sessionId;

    const dbConfigs = {
        host: config.db_host,
        port: config.db_port,
        dialect: 'mysql',
        dialectOptions: {
            multipleStatements: true
        },
        logging: false
    };
    let sequelize = new Sequelize(
        config.database, config.db_username, config.db_password, dbConfigs
    );

    let users;
    let query = "SELECT user.id, user.username, user.email, user.is_anonymous, user_session.user_group ";
    query += "FROM user ";
    query += "INNER JOIN user_session ON user.id = user_session.user_id ";
    query += "WHERE user_session.session_id = $session AND user_session.relation_type = 'PARTICIPANT' ";
    await sequelize.query(query, {bind: {session: sessionId} , type:sequelize.QueryTypes.SELECT})
    .then(function(result) {
        users = result;
    });     

    //# of quiz questions answered (if any)
    let quizzesAnswered
    query = "SELECT user.id AS 'id', COUNT(DISTINCT quiz_response.quiz_id) AS 'quizzesAnswered' ";
    query += "FROM user ";
    query += "INNER JOIN quiz_response ON user.id = quiz_response.user_id ";
    query += "INNER JOIN quiz ON quiz_response.quiz_id = quiz.id ";
    query += "WHERE quiz.session_id = $session ";
    query += "GROUP BY user.id";
    await sequelize.query(query, {bind: {session: sessionId} , type:sequelize.QueryTypes.SELECT})
    .then(function(result) {
        quizzesAnswered = result;
    });

    //# of questions asked and # of upvotes gained (if any)
    let questionsAndVotesGained;
    query = "SELECT question.user_id AS 'id', COUNT(DISTINCT question.id) AS 'questionsAsked', COUNT(vote.is_upvote) AS 'upvotesGained' ";
    query += "FROM question ";
    query += "LEFT JOIN vote ON question.id = vote.question_id "
    query += "WHERE session_id = $session ";
    query += "GROUP BY question.user_id";
    await sequelize.query(query, {bind: {session: sessionId} , type:sequelize.QueryTypes.SELECT})
    .then(function(result) {
        questionsAndVotesGained = result;
    });

    //# of upvotes given (if any)
    let votesGiven;
    query = "SELECT vote.user_id AS 'id', COUNT(*) AS 'votesGiven' ";
    query += "FROM vote ";
    query += "INNER JOIN question ON question.id = vote.question_id "
    query += "WHERE session_id = $session ";
    query += "GROUP BY vote.user_id";
    await sequelize.query(query, {bind: {session: sessionId} , type:sequelize.QueryTypes.SELECT})
    .then(function(result) {
        votesGiven = result;
    });
    let response = joinObjects(users, votesGiven, questionsAndVotesGained, quizzesAnswered);
    res.send(response);
    return response;
};
function joinObjects() {
    var idMap = {};
    // Iterate over arguments
    for(var i = 0; i < arguments.length; i++) { 
        console.log("argument " + i + ": " + JSON.stringify(arguments[i]));
        // Iterate over individual argument arrays (aka json1, json2)
        for(var j = 0; j < arguments[i].length; j++) {
            var currentID = arguments[i][j]['id'];
            if(!idMap[currentID]) {
                idMap[currentID] = {};
            }
            // Iterate over properties of objects in arrays (aka id, name, etc.)
            for(key in arguments[i][j]) {
                idMap[currentID][key] = arguments[i][j][key];
            }
        }
    }

    // push properties of idMap into an array
    var newArray = [];
    for(property in idMap) {
        newArray.push(idMap[property]);
    }
    return newArray;
}
exports.validateAuthUserId = async function(req, res, next) {
    if (!req.headers.authorization) {
        res.status(400).send('Unauthorized');
        return;
    }
    const username = utils.decodeAuthUsername(req.headers.authorization);
    const user = await User.findOne({
        where: { username: username }
    });
    if (!user) {
        res.status(400).send('Unauthorized');
        return;
    }
    req.headers.user = user;
    next();
};

exports.verifyEmail = async function(req, res, next) {
    const beforeDecrypt = req.params.code;
    let afterDecrypt = utils.decrypt(beforeDecrypt);

    if (!afterDecrypt) {
        res.status(400).send('Email verification failed - invalid verification code');
    }

    const splitStr = afterDecrypt.split('`');
    if (splitStr.length !== 2) {
        res.status(400).send('Email verification failed - invalid verification code');
        return;
    }

    if (isNaN(splitStr[0])) {
        res.status(400).send('Email verification failed - invalid verification code');
        return;
    }
    User.findOne({
        where: {
            id: splitStr[0],
            username: splitStr[1]
        }
    }).then(user => {
        if (!user) {
            res.status(400).send('Email verification failed - invalid verification code combination');
        } else {
            if (user.is_verified) {
                res.status(400).send('Email has already been verified');
            } else {
                user.updateAttributes({
                    is_verified: true
                }).then(function() {
                    console.log('Successfully is_verified');
                    res.send('Email has been successfully verified. Please login');
                }).catch(function(e) {
                    res.status(400).send('Email verification failed ' + e);
                })
            }
        }
    });
};

// Create endpoint /api/users for POST
exports.postUsers = function(req, res) {
    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    const userType = req.body.user_type;

    //Check username validity
    if (!REGEX_USERNAME.test(username)) {
        res.status(400).send('Invalid username');
        return;
    }
    //Check password validity
    if (!password || password.length < 8) {
        res.status(400).send('Invalid password (Ensure password is at least 8 characters)');
        return;
    }

    if (!email || !utils.validateEmail(email)) {
        res.status(400).send('Invalid email address');
        return;
    }

    //Check if username/email already in use
    User.findOne({
        where: {
            $or: [{username: username}, {email: email}]
        }
    }).then(user => {
        if (!user) {
            //Hash password, save user
            const hashedPassword = bcrypt.hashSync(password, salt);
            User.create({
                username: username,
                password: hashedPassword,
                email: email,
                user_type: userType
            }).then(createdUser => {
                const verifyUrl = getVerificationUrl(req.headers.host, createdUser);
                createdUser.code = verifyUrl;
                mailer.sendVerificationEmail(createdUser);

                const strippedUser = {
                    userId: createdUser.id,
                    sessionId: -1,
                    username: createdUser.username,
                    is_anonymous: false
                };
                const token = jwt.sign(strippedUser, config.jwtSecret, {expiresIn : config.jwtExpiresInSeconds});
                res.send({
                    status: 'success',
                    userId: createdUser.id,
                    username: createdUser.username,
                    sessionId: -1,
                    isAnonymous: false,
                    sessionToken: token
                });
            });
        } else {
            if (user.email === email) {
                res.status(400).send('Email (' + email + ') is already in use');
            } else {
                res.status(400).send('Username (' + username + ') is already in use');
            }

        }
    });
};

// Create endpoint /api/editUser for POST
exports.editUser = function(req, res) {
    const currentUsername = req.body.currentUsername;
    const newUsername = req.body.newUsername;
    const newPassword = req.body.newPassword;
    const currentEmail = req.body.currentEmail;
    const newEmail = req.body.newEmail;
    const userType = req.body.user_type;
    const userId = req.body.userId;
    let duplicateCredentials = false;

    //Check changes were made
    if (currentUsername == newUsername && currentEmail == newEmail && newPassword == "") {
        res.status(400).send('Nothing was changed!');
        return;
    }
    //Check username validity
    if (currentUsername != newUsername) {
        if (!REGEX_USERNAME.test(newUsername)) {
            res.status(400).send('Invalid username');
            return;
        }
    }
    //Check password validity
    if (newPassword != "") {
        if (!newPassword || newPassword.length < 8) {
            res.status(400).send('Invalid password (Ensure password is at least 8 characters)');
            return;
        }
    }
    //Check email validity
    if (currentEmail != newEmail) {
        if (!newEmail || !utils.validateEmail(newEmail)) {
            res.status(400).send('Invalid email address');
            return;
        }
    }

    //Check if username/email already in use
    User.findAndCountAll({
        where: {
            $or: [{username: newUsername}, {email: newEmail}]
        }
    }).then(user => {
        //user already exists
        for (var i=0; i<user.count; i++) {
            if (user.rows[i].email == newEmail && newEmail != currentEmail) {
                res.status(400).send('Email (' + newEmail + ') is already in use');
                duplicateCredentials = true;
                return;
            } else if (user.rows[i].username == newUsername && newUsername != currentUsername) {
                res.status(400).send('Username (' + newUsername + ') is already in use');
                duplicateCredentials = true;
                return;
            }
        }
     
        if (!duplicateCredentials) {
            //update user if no new password
            if (newPassword == "") {
                //update user if no new email, no new password
                if (newEmail == currentEmail) {
                    User.findOne({
                        where: 
                            {id: userId}
                    }).then(user => {
                        user.updateAttributes({
                            username: newUsername
                        }).then(editedUser => {
                            const strippedUser = {
                                userId: editedUser.id,
                                sessionId: -1,
                                username: editedUser.username,
                                is_anonymous: false
                            };
                            const token = jwt.sign(strippedUser, config.jwtSecret, {expiresIn : config.jwtExpiresInSeconds});
                            res.send({
                                status: 'success',
                                userId: editedUser.id,
                                username: editedUser.username,
                                sessionId: -1,
                                isAnonymous: false,
                                sessionToken: token
                            });
                        });        
                    });
                } else { //update user if new email, no password
                    User.findOne({
                        where: 
                            {id: userId}
                    }).then(user => {
                        user.updateAttributes({
                            username: newUsername,
                            email: newEmail,
                            is_verified: false
                        }).then(editedUser => {
                            const verifyUrl = getVerificationUrl(req.headers.host, editedUser);
                            editedUser.code = verifyUrl;
                            mailer.sendUpdateEmail(editedUser);

                            const strippedUser = {
                                userId: editedUser.id,
                                sessionId: -1,
                                username: editedUser.username,
                                is_anonymous: false
                            };
                            const token = jwt.sign(strippedUser, config.jwtSecret, {expiresIn : config.jwtExpiresInSeconds});
                            res.send({
                                status: 'success',
                                userId: editedUser.id,
                                username: editedUser.username,
                                sessionId: -1,
                                isAnonymous: false,
                                sessionToken: token
                            });
                        });        
                    });
                }
            } else { //update user if new password
                //update user if no new email, new password
                if (newEmail == currentEmail) {
                    User.findOne({
                        where: 
                            {id: userId}
                    }).then(user => {
                        //Hash password, save user
                        const hashedPassword = bcrypt.hashSync(newPassword, salt);
                        user.updateAttributes({
                            username: newUsername,
                            password: hashedPassword
                        }).then(editedUser => {

                            const strippedUser = {
                                userId: editedUser.id,
                                sessionId: -1,
                                username: editedUser.username,
                                is_anonymous: false
                            };
                            const token = jwt.sign(strippedUser, config.jwtSecret, {expiresIn : config.jwtExpiresInSeconds});
                            res.send({
                                status: 'success',
                                userId: editedUser.id,
                                username: editedUser.username,
                                sessionId: -1,
                                isAnonymous: false,
                                sessionToken: token
                            });
                        });        
                    });
                } else { //update user if new email, new password
                    User.findOne({
                        where: 
                            {id: userId}
                    }).then(user => {
                        //Hash password, save user
                        const hashedPassword = bcrypt.hashSync(newPassword, salt);
                        user.updateAttributes({
                            username: newUsername,
                            password: hashedPassword,
                            email: newEmail,
                            is_verified: false
                        }).then(editedUser => {
                            const verifyUrl = getVerificationUrl(req.headers.host, editedUser);
                            editedUser.code = verifyUrl;
                            mailer.sendUpdateEmail(editedUser);

                            const strippedUser = {
                                userId: editedUser.id,
                                sessionId: -1,
                                username: editedUser.username,
                                is_anonymous: false
                            };
                            const token = jwt.sign(strippedUser, config.jwtSecret, {expiresIn : config.jwtExpiresInSeconds});
                            res.send({
                                status: 'success',
                                userId: editedUser.id,
                                username: editedUser.username,
                                sessionId: -1,
                                isAnonymous: false,
                                sessionToken: token
                            });
                        });        
                    });
                }
            }
        }
    });
};

getVerificationUrl = function (hostLink, user) {
    const code = "" + user.id + '`' + user.username;
    const encrypt = utils.encrypt(code);
    const verifyUrl = hostLink + '/verify/' + encrypt;
    return verifyUrl;
};
getSessionUrl = function (hostLink, sessionCode) {
    return hostLink + '/session/' + sessionCode;
};

exports.getUser = function(req, res) {
    let username = "";
    if (req.headers.authorization) {
        username = utils.decodeAuthUsername(req.headers.authorization);
    } else {
        res.status(400).send('Unauthorized');
        return;
    }

    User.findOne({
        attributes: ['id', 'username'],
        where: {
            username: username
        }
    }).then(function(user) {
        res.send(user);
    });
};

exports.resetPassword = async function (req, res) {
    const email = req.body.email;
    if (!email){
        res.status(400).send('Invalid Email');
        return;
    }

    const user = await User.findOne({
        where: {email: email}
    });
    if (!user) {
        //Failure here, but don't let user know in case its spam attempt
        res.send(200);
        return;
    }
    const randomPassword = utils.generateRandomPassword();
    const hashedPassword = bcrypt.hashSync(randomPassword, salt);

    await user.updateAttributes({
        password: hashedPassword
    });
    mailer.sendRestPasswordEmail(email, randomPassword);
    res.send(200);
};

exports.generateAccountsFromEmails = async function (req, res) {
    const sessionId = req.body.session_id;
    let emailsGroups = req.body.emailsGroups;
    emailsGroups = emailsGroups.split(',');

    let generatedAccounts = [];
    let alreadyExistingEmails = [];
    let invalidEmails = [];

    //split values into emails and groups
    let emails = [];
    let groups = [];
    let validGroup = false;
    for (let i=0; i<emailsGroups.length; i++) {
        let emailOrGroup = emailsGroups[i];
        emailOrGroup = emailOrGroup.trim();
        if (emails.length > groups.length) {
            if (!isNaN(emailOrGroup) && emailOrGroup != "") {
                groups.push(emailOrGroup);
                validGroup = true;
            } else {
                groups.push(0);
            }
        }
        if (emails.length == groups.length && !validGroup) {
            if (!utils.validateEmail(emailOrGroup)) {
                invalidEmails.push(emailOrGroup);
                continue;
            }
            emails.push(emailOrGroup);
        }
        validGroup = false;
    }

    console.log('generateAccountsFromEmail emails= ' + emails);
    const session = req.headers.session;

    for (let i = 0; i < emails.length; i++) {
        let email = emails[i];

        let emailCheck = await User.findOne({
            where: {email: email}
        });
        if (emailCheck) {
            const existingUserSession = await UserSession.findOne({
                where: {
                    user_id: emailCheck.id,
                    session_id: sessionId
                }
            });
            if (existingUserSession) {
                if (existingUserSession.relation_type !== 'ADMIN'
                    && existingUserSession.relation_type !== 'INVITED_PARTICIPANT') {
                    //Prevent ADMINs from demoting themselves
                    existingUserSession.updateAttributes({
                        relation_type: 'INVITED_PARTICIPANT'
                    });
                    mailer.sendSessionInvitationEmail(email, session.session_name,
                        session.session_description, getSessionUrl(req.headers.host, session.access_token));
                } else {
                    alreadyExistingEmails.push(email);
                }
            } else {
                await UserSession.create({
                    user_id: emailCheck.id,
                    session_id: sessionId,
                    user_group: groups[i],
                    relation_type: 'INVITED_PARTICIPANT'
                });
                generatedAccounts.push(email);
                mailer.sendSessionInvitationEmail(email, session.session_name,
                    session.session_description, getSessionUrl(req.headers.host, session.access_token));
            }
        } else {
            const randomPassword = utils.generateRandomPassword();
            const generatedUser = await createUserFromEmailInvite(email, randomPassword);
            if (generatedUser) {
                generatedAccounts.push(email);
                await UserSession.create({
                    user_id: generatedUser.id,
                    session_id: sessionId,
                    user_group: groups[i],
                    relation_type: 'INVITED_PARTICIPANT'
                });

                const verifyUrl = getVerificationUrl(req.headers.host, generatedUser);
                generatedUser.code = verifyUrl;
                mailer.sendVerificationEmail(generatedUser);
                mailer.sendSessionInvitationEmail(email, session.session_name,
                    session.session_description, getSessionUrl(req.headers.host, session.access_token));
            } else {
                alreadyExistingEmails.push(email);
            }
        }
    }
    
    const response = {
        createdAccountEmails: generatedAccounts,
        alreadyExistsEmails: alreadyExistingEmails,
        invalidEmails: invalidEmails
    };
    res.send(response);
};

async function createUserFromEmailInvite (email, generatedPassword) {
    console.log('createUserFromEmailInvite Email[' + email + ']  Password[' + generatedPassword + ']')
    //Check if user with email already exists
    let userCheck = await User.findOne({
        where: {email: email}
    });
    console.log('UserCheck = ' + JSON.stringify(userCheck));

    if (userCheck) {
        return userCheck;
    }

    const splitEmail = email.split('@');
    let username = (splitEmail.length == 2) ? splitEmail[0] : generateAnonUsername();


    //Check if username is already in use, and append random digits to end if so to minimize collision
    userCheck = await User.findOne({
        where: {username: username}
    });

    if (userCheck) {
        // const randomNumber = Math.floor(100000 + Math.random() * 900000).toString().substring(0, 4);
        let subSplit = splitEmail[1].split('.');
        username += '_' + subSplit[0];
    }

    const hashedPassword = bcrypt.hashSync(generatedPassword, salt);
    const user = await User.create({
        username: username,
        password: hashedPassword,
        email: email,
        user_type: 'PARTICIPANT',
        is_anonymous: false
    });

    mailer.sendNewAccountEmail(email, generatedPassword);
    return user;
}

async function createAnonUser () {
    let randomUsername = await generateAnonUsername();
    const hashedPassword = bcrypt.hashSync('password', salt);
    const user = await User.create({
        username: randomUsername,
        password: hashedPassword,
        user_type: 'PARTICIPANT',
        is_anonymous: true
    });
    return user;
}

async function generateAnonUsername () {
    const prefixes = [
        'Jumping',
        'Running',
        'Swimming',
        'Climbing',
        'Laughing',
        'Flying',
        'Crawling',
        'Diving',
        'Kicking',
        'Lifting',
        'Skating',
        'Punching',
        'Kicking'
    ];
    const suffixes = [
        'Kangaroo',
        'Penguin',
        'Otter',
        'Lion',
        'Alpaca',
        'Narwhal',
        'Lynx',
        'Wolf'
    ];
    const randomPrefix = prefixes[Math.floor(Math.random() * prefixes.length)];
    const randomSuffix = suffixes[Math.floor(Math.random() * suffixes.length)];
    const randomNumber = Math.floor(100000 + Math.random() * 900000).toString().substring(0, 4);
    const randomUsername = randomPrefix + randomSuffix + randomNumber;
    const user = await User.findOne({
        where: {username: randomUsername}
    });
    if (user) {
        return await generateAnonUsername();
    }
    return randomPrefix + randomSuffix + randomNumber;
}

exports.signJwtToken = signJwtToken;