const User = require('../models').User;
const Session = require('../models').Session;
const UserSession = require('../models').UserSession;
const GroupSession = require('../models').GroupSession;
const Question = require('../models').Question;
const Quiz = require('../models').Quiz;
const utils = require('./utils');
const mailer = require('./mailer');
const moment = require('moment');
let socketManager;

//To trigger SocketManager to update all clients
exports.injectSocketManager = function (controller) {
    socketManager = controller;
};

exports.updateUserSession = async function (req, res) {
    const sessionId = req.body.session_id;
    const user = req.headers.user;
    const userId = user.id;
    const nextQuiz = req.body.next_quiz;

    const createdQuiz = await UserSession.update(
        { next_question: nextQuiz },
        { where: {session_id: sessionId, user_id: userId}}
    )
    .catch(e => res.status(500).send(e.message));
    res.send(createdQuiz);    
}

exports.updateGroupSession = async function (req, res) {
    const sessionId = req.body.session_id;
    const groupNumber = req.body.group_number;
    const nextQuiz = req.body.next_quiz;

    const createdQuiz = await GroupSession.update(
        { next_question: nextQuiz },
        { where: {session_id: sessionId, group_number: groupNumber}}
    ).catch(e => res.status(500).send(e.message));    
    res.send(createdQuiz);
}

// Create endpoint /api/users for POST
exports.postSession = async function(req, res) {
    const user = req.headers.user;
    const username = user.username;
    if (!username) {
        res.status(400).send('Unauthorized');
        return;
    }

    const session_name = req.body.session_name;
    const session_description = req.body.session_description;
    const session_code = req.body.session_code;
    const session_type = req.body.session_type;
    let access_token = req.body.session_code;

    if (!session_name) {
        res.status(400).send('Please enter a session name!');
        return;
    }
    if (!session_description) {
        res.status(400).send('Please enter a session description!');
        return;
    }

    if (!access_token) {
       access_token = getRandomSessionAccessToken();
    }

    if (access_token.length < 3 || access_token.length > 8) {
        res.status(400).send('Session Code must be between 3 - 8 characters!');
        return;
    }

    if (!user.is_verified) {
        res.status(400).send('User is not verified, and cannot create session');
        return;
    }
    const priorSession = await Session.findOne({
        where: { access_token : access_token }
    });

    if (priorSession) {
        res.status(400).send('Session Code ' + access_token + ' is already in use - please choose another!');
        return;
    }

    const createdSession = await Session.create({
        session_name: session_name,
        session_description: session_description,
        session_code: session_code,
        session_type: session_type,
        access_token: access_token
    });
    if (!createdSession) {
        res.status(400).send('An unexpected error occurred while creating the session! Please refresh the page and try again.');
        return;
    }

    const userSession = await UserSession.create({
        relation_type: 'ADMIN',
        user_id: user.id,
        session_id: createdSession.id
    });

    res.send({
        sessionId: createdSession.id,
        sessionAccessToken: access_token
    });

    // User.findOne({
    //     where: { username: username }
    // }).then(user => {
    //     if (user) {
    //         if (user.is_verified) {
    //             const sessionAccessToken = getRandomSessionAccessToken();
    //             Session.create({
    //                 session_name: session_name,
    //                 session_description: session_description,
    //                 session_code: session_code,
    //                 access_token: sessionAccessToken
    //             }).then(createdSession => {
    //                 UserSession.create({
    //                     relation_type: 'ADMIN',
    //                     user_id: user.id,
    //                     session_id: createdSession.id
    //                 }).then(createdUserSession => {
    //                     res.send({
    //                         status: 'success',
    //                         sessionId: createdSession.id,
    //                         sessionAccessToken: sessionAccessToken
    //                     });
    //                 });
    //             });
    //         } else {
    //             res.status(400).send('Owner not verified');
    //         }
    //     } else {
    //         res.status(400).send('Owner does not exist');
    //     }
    // });
};

//Middleware Validators
exports.validateUserInSession = async function(req, res, next) {
    const userId = req.headers.user.id;

    const session = req.headers.session;
    if (!session) {
        console.log('Session not in req headers - ensure validateSessionExistence middleware is called');
        res.sendStatus(400);
    }

    if (!userId) {
        res.status(400).send('User authentication error occured');
        return;
    }

    const whereClause = {
        session_id: session.id,
        user_id: userId
    };
    if (session.invite_only) {
        whereClause.relation_type = 'INVITED_PARTICIPANT'
    }
    const userSession = await UserSession.findOne( { where: whereClause } );

    if (!userSession) {
        res.status(400).send('User is not in session!');
        res.end();
    }
    next();
};
exports.validateUserIsAdmin = async function(req, res, next) {
    const userId = req.headers.user.id;
    const sessionId = req.body.session_id;

    if (!sessionId) {
        res.status(400).send('session_id not specified in body');
        return;
    }
    if (!userId) {
        res.status(400).send('User authentication error occured');
        return;
    }
    const adminUserSession = await UserSession.findOne(
        {
            where: {
                relation_type: 'ADMIN',
                session_id: sessionId,
                user_id: userId
            }
        }
    );

    if (!adminUserSession) {
        res.status(400).send('User is not admin of session!');
        res.end();
    }
    next();
};

//Important middleware to populate headers with session object
exports.validateSessionExistence = async function(req, res, next) {
    const sessionId = req.body.session_id;

    if (!sessionId) {
        res.status(400).send('session_id not specified in body');
        return;
    }
    const session = await Session.findOne(
        {
            where: {
                id: sessionId
            }
        }
    );

    if (!session) {
        res.status(400).send('Session does not exist!');
        res.end();
    }
    req.headers.session = session;
    next();
};

// Create endpoint /api/users for GET
exports.getDoesSessionExist = function(req, res) {
    const sessionAccessCode = req.params.sessionAccessCode;
    Session.findOne(
        {
            where: {
                access_token: sessionAccessCode
            }
        }
    ).then(function(session) {
        if (session) {
            res.send(session);
        } else {
            res.sendStatus(400);
        }
    });
};

exports.findAdminUserSession = function(req, res) {
    const userId = req.body.userId;
    const sessionId = req.body.sessionId;
    UserSession.findOne(
        {
            where: {
                user_id: userId,
                session_id: sessionId,
                relation_type: 'ADMIN'
            }
        }
    ).then(function(session) {
        if (session) {
            res.send(session);
        } else {
            res.sendStatus(400);
        }
    });
};

// Create endpoint /api/users for GET
exports.getSession = function(req, res) {
    const sessionId = req.params.sessionId;
    Session.findOne(
        {
            where: {
                id: sessionId
            },
            include: [
                { model: User, attributes: ['id', 'username', 'relation_type', 'is_anonymous'] }
            ]
        }
    ).then(function(a) {
        res.send(a);
    });
};
exports.getSession = async function (sessionId) {
    const session = await Session.findOne(
        {
            where: {
                id: sessionId
            }
        }
    );
    return session;
};

exports.isSessionInviteOnly = async function (sessionId) {
    const session = await Session.findOne(
        {
            where: {
                id: sessionId,
                invite_only: true
            }
        }
    );
    return (session !== undefined);
};

exports.setSessionIsInviteOnly = async function(req, res) {
    const session = req.headers.session;
    const isInviteOnly = req.body.invite_only;

    if (isInviteOnly === undefined) {
        res.status(400).send('invite_only not defined in request body');
        return;
    }
    if (session.invite_only === isInviteOnly) {
        res.send('Session\'s invite_only is already ' + isInviteOnly);
        return;
    }
    await session.updateAttributes({invite_only: isInviteOnly})
        .catch(e => {
            res.status(500).send(e.message);
        });

    if (socketManager) {
        //Force all clients in session to refresh to kick unwanted users
        socketManager.forceAllSocketsRefresh(session.id);
    }

    res.send('Session\'s invite_only updated to ' + isInviteOnly);
};
exports.setSessionIsRequireVerification = async function(req, res) {
    const session = req.headers.session;
    const isRequireVerification = req.body.require_verification;

    if (isRequireVerification === undefined) {
        res.status(400).send('require_verification not defined in request body');
        return;
    }
    if (session.require_verification === isRequireVerification) {
        res.send('Session\'s require_verification is already ' + isRequireVerification);
        return;
    }
    await session.updateAttributes({require_verification: isRequireVerification})
        .catch(e => {
            res.status(500).send(e.message);
        });

    if (socketManager) {
        //Force all clients in session to refresh to kick unwanted users
        socketManager.forceAllSocketsRefresh(session.id);
    }
    
    res.send('Session\'s require_verification updated to ' + isRequireVerification);
};
exports.setSessionIsVoicechatEnabled = async function(req, res) {
    const session = req.headers.session;
    const isVoicechatEnabled = req.body.voicechat_enabled;

    if (isVoicechatEnabled === undefined) {
        res.status(400).send('voicechat_enabled not defined in request body');
        return;
    }
    if (session.voicechat_enabled === isVoicechatEnabled) {
        res.send('Session\'s voicechat_enabled is already ' + isVoicechatEnabled);
        return;
    }
    await session.updateAttributes({voicechat_enabled: isVoicechatEnabled})
        .catch(e => {
            res.status(500).send(e.message);
        });

    if (socketManager) {
        //Force all clients in session to refresh to kick unwanted users
        //socketManager.forceAllSocketsRefresh(session.id);
    }
    
    res.send('Session\'s voice_ updated to ' + isVoicechatEnabled);
};
exports.requestVoiceConnection = async function(req, res) {
    if (!socketManager) {
        res.status(400).send("Nothing running here!");
    }
    socketManager.requestVoiceConnection(req.headers.session.id, req.headers.user.username);
    res.send("Success");
};
exports.closeVoiceConnection = async function(req, res) {
    if (!socketManager) {
        res.status(400).send("Nothing running here!");
    }
    socketManager.closeVoiceConnection(req.headers.session.id, req.headers.user.username);
    res.send("Success");
};
exports.kickVoiceConnection = async function(req, res) {
    if (!socketManager) {
        res.status(400).send("Nothing running here!");
    }
    socketManager.kickVoiceConnection(req.headers.session.id, req.body.username);
    res.send("Success");
};
exports.acceptVoiceConnection = async function(req, res) {
    if (!socketManager) {
        res.status(400).send("Nothing running here!");
    }
    socketManager.acceptVoiceConnection(req.headers.session.id, req.body.username);
    res.send("Success");
};
exports.setSessionIsRequireVoiceApproval = async function(req, res) {
    const session = req.headers.session;
    const isRequireVoiceApproval = req.body.require_voice_approval;

    if (isRequireVoiceApproval === undefined) {
        res.status(400).send('require_voice_approval not defined in request body');
        return;
    }
    if (session.require_voice_approval === isRequireVoiceApproval) {
        res.send('Session\'s require_voice_approval is already ' + isRequireVoiceApproval);
        return;
    }
    await session.updateAttributes({require_voice_approval: isRequireVoiceApproval})
        .catch(e => {
            res.status(500).send(e.message);
        });

    if (socketManager) {
        //Force all clients in session to refresh to kick unwanted users
        //socketManager.forceAllSocketsRefresh(session.id);
    }
    
    res.send('Session\'s require_voice_approval to ' + isRequireVoiceApproval);

};
exports.setSessionDates = async function(req, res) {
    const session = req.headers.session;
    let start = req.body.start;
    let end = req.body.end;

    start = start.split('"').join("");
    end = end.split('"').join("");

    if (start === undefined && end === undefined ) {
        //At least one of them must be specified
        res.status(400).send('Start & End dates not defined in request body');
        return;
    }
    await session.updateAttributes({
        start_time: start,
        end_time: end
    }).catch(e => {
        res.status(500).send(e.message);
    });

    res.send('Session\'s Start,End Dates updated to ' + start + ' ' + end);
};

exports.triggerSessionReport = async function (req, res) {
    console.log('Trigger Session Report');
    const session = req.headers.session;
    const sendSuccess = await sendSessionReport(session);
    if (sendSuccess) {
        res.sendStatus(200);
    } else {
        res.sendStatus(400);
    }
};

//Trigger session report to only admin
const sendSessionReport = async function(session) {
    console.log('sendSessionReport');
    const sessionId = session.id;

    //Find admin email
    const userSession = await UserSession.findOne({
       where: {
           session_id: sessionId,
           relation_type: ["SPEAKER", "ADMIN"]
       }
    });
    if (!userSession) {
        console.log('ERROR: UserSession not found');
        return false;
    }
    const admin = await User.findOne({
        where: { id: userSession.user_id }
    });
    if (!admin || !admin.email) {
        console.log('ERROR: Admin not found');
        return false;
    }
    const adminEmail = admin.email;

    //Find all questions in session
    const questions = await Question.findAll({
        where: { session_id: sessionId }
    });
    console.log('questions ' + JSON.stringify(questions));
    const quizzes = await Quiz.findAll({
        where: { session_id: sessionId }
    });
    console.log('quizzes ' + JSON.stringify(quizzes));
    const registeredUsers = await User.findAll({
        where: { is_anonymous: false },
        attributes: ['id', 'username', 'email', 'is_anonymous', 'is_verified'],
        include: [{
            model: UserSession,
            attributes: [],
            where: { session_id: sessionId, relation_type: ['PARTICIPANT', 'INVITED_PARTICIPANT'] }
        }]
    });
    const anonymousUsers = await User.findAll({
        where: { is_anonymous: true },
        attributes: ['id', 'username', 'email', 'is_anonymous', 'is_verified'],
        include: [{
            model: UserSession,
            attributes: [],
            where: { session_id: sessionId, relation_type: ['PARTICIPANT', 'INVITED_PARTICIPANT'] }
        }]
    });

    const users = {
        registered: registeredUsers,
        anonymous: anonymousUsers
    };
    console.log('registeredUsers\n' + JSON.stringify(registeredUsers));
    console.log('anonymousUsers\n' + JSON.stringify(anonymousUsers));
    await mailer.sendSessionReportEmail(adminEmail, session.session_name, session.session_description,
        users, questions, quizzes);
    return true;
};

//Trigger session report to only admin
exports.endSession = async function(req, res) {
    const session = req.headers.session;
    if (session.has_ended) {
        res.status(400).send('Session has already ended');
        return;
    }
    await endSessionAndSendReport(session);
    res.sendStatus(200);
};
const endSessionAndSendReport = async function (session) {
    session.updateAttributes({has_ended: true});
    socketManager.kickAllUsersAndCloseSession(session.id);
    await sendSessionReport(session);
};

// Create endpoint /api/users for GET
exports.getSessionsOwned = async function(req, res) {
    const username = utils.decodeAuthUsername(req.headers.authorization);
    if (!username) {
        res.status(400).send('Unauthorized');
        return;
    }

    const owner = await User.findOne({
        attributes: ['id'],
        where: {
            username: username
        }
    });
    const ownerId = owner.id;
    const ownedSessionRelations = await UserSession.findAll({
        attributes: ['id', 'relation_type'],
        where: {
            user_id: ownerId,
            relation_type: ["SPEAKER", "ADMIN"]
        }
    });
    const ownedSessionIds = ownedSessionRelations.map(function (ownedSessionRelation) {
        return ownedSessionRelation.id;
    });

    const ownedSessions = await Session.findAll(
        {
            where: {
                id: ownedSessionIds
            }
        }
    );
    res.send(ownedSessions);
};

exports.findSessionsOwned = async function(req, res) {
    const userId = req.headers.user.id;
    const ownedSessionRelations = await UserSession.findAll({
        attributes: ['session_id', 'relation_type'],
        where: {
            user_id: userId,
            relation_type: ["SPEAKER", "ADMIN"]
        }
    });
    const ownedSessionIds = ownedSessionRelations.map(function (ownedSessionRelation) {
        return ownedSessionRelation.session_id;
    });

    const ownedSessions = await Session.findAll(
        {
            where: {
                id: ownedSessionIds
            }
        }
    );
    res.send(ownedSessions);
};

exports.addUserToSession = async function(req, res) {
    //Check if user is valid
    const username = utils.decodeAuthUsername(req.headers.authorization);
    if (!username) {
        res.status(400).send('Unauthorized');
        return;
    }
    const user = await User.findOne({
        attributes: ['id'],
        where: {
            username: username
        }
    });
    const userId = user.id;

    //Check if specified session is valid
    const sessionId = req.params.sessionId;
    const accessToken = req.params.access_token;
    if (!sessionId && !accessToken) {
        res.status(400).send('sessionId not specified');
        return;
    }
    let session;
    if (sessionId) {
        session = await Session.findOne({
            where: {id: sessionId}
        });
    } else {
        session = await Session.findOne({
            where: {access_token: access_token}
        });
    }
    if (!session) {
        res.status(400).send('sessionId ' + sessionId + ' is invalid');
        return;
    }

    //Check if user already has previous session
    const previousUserSession = await UserSession.findOne({
        where: {
            user_id: userId,
            session_id: sessionId
        }
    });
    if (previousUserSession) {
        //Check if user has admin rights to session
        if (previousUserSession.relation_type === 'ADMIN') {

        } else {
            res.status(400).send('User ' + username + ' is already in specified session');
            return;
        }
    }

    const userSession = await UserSession.create({
        user_id: userId,
        session_id: sessionId,
        relation_type: 'PARTICIPANT'
    });
    res.send('UserSession created - ' + JSON.stringify(userSession));
};

exports.socketAddUserToSession = async function(userId, sessionId) {
    if (!sessionId || !userId) {
        return false;
    }
    //Check if user is valid
    const user = await User.findOne({
        where: { id: userId }
    });
    if (!user) {
        return false;
    }
    const session = await Session.findOne({
        where: { id: sessionId }
    });
    if (!session) {
        return false;
    }

    //Check if user already has previous session
    const previousUserSession = await UserSession.findOne({
        where: {
            user_id: userId,
            session_id: sessionId
        }
    });
    if (previousUserSession) {
        return false;
    }

    const userSession = await UserSession.create({
        user_id: userId,
        session_id: sessionId,
        relation_type: 'PARTICIPANT'
    });
    return true;
};

exports.endPendingExpireSessions = async function() {
    console.log('endPendingExpireSessions');
    const currTime = Date.now();
    const toBeExpiredSessions = await Session.findAll({
        where: {
            has_ended: false,
            end_time: {
                $lt: moment().toDate()
            }
        }
    });
    console.log('toBeExpiredSessions = ' + JSON.stringify(toBeExpiredSessions));
    for (let i = 0; i < toBeExpiredSessions.length; i++) {
        const session = toBeExpiredSessions[i];
        await endSessionAndSendReport(session);
        console.log('Ended session ' + session.id);
    }
};

const getRandomSessionAccessToken = function(length) {
    const tokenLength = length ? length : 6;
    let token = '';
    const possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    for (let i = 0; i < tokenLength; i++) {
        token += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length));
    }
    return token;
};