const crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';
const hash = crypto.createHash('md5', 'mySecret');

exports.decodeAuthUsername = function (auth) {
    if (!auth) return '';
    const tmp = auth.split(' ');   // Split on a space, the original auth looks like  "Basic Y2hhcmxlczoxMjM0NQ==" and we need the 2nd part
    const buf = new Buffer(tmp[1], 'base64'); // create a buffer and tell it the data coming in is base64
    const plain_auth = buf.toString();        // read it back out as a string
    // At this point plain_auth = "username:password"
    const creds = plain_auth.split(':');      // split on a ':'
    return creds[0];
};

exports.encrypt = function (text){
    // return hash.update(buffer).digest('hex');
    // console.log('beforeEncrypt = ' + text);

    const cipher = crypto.createCipher(algorithm,password);
    let crypted = cipher.update(text,'utf8','hex');
    // console.log('crypted = ' + JSON.stringify(crypted));

    crypted += cipher.final('hex');
    return crypted;
};

exports.decrypt = function (text) {
    try {
        const decipher = crypto.createDecipher(algorithm, password);
        let dec = decipher.update(text, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    } catch (e) {
        return '';
    }
};

exports.validateEmail = function(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

exports.generateRandomPassword = function(length) {
    const passwordLength = length ? (length+1) : 9;
    const randomPassword = Math.floor(100000 + Math.random() * 900000).toString().substring(0, passwordLength);
    return randomPassword;
};
exports.generateRandomCode = function(length) {
    const codeLen = length ? length : 6;
    let code = '';
    const possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    for (let i = 0; i < codeLen; i++) {
        code += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length));
    }
    return code;
};
