const sessionController = require('./session.js');
const CronJob = require('cron').CronJob;

const start = function() {
    //Check for expired sessions every minute
    new CronJob('0 * * * * *', function() {
        // console.log('You will see this message every second');
        sessionController.endPendingExpireSessions();
    }, null, true);
};

exports.start = start;