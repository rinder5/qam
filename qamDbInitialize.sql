CREATE TABLE user (
	id int NOT NULL AUTO_INCREMENT,
	user_type varchar(50) DEFAULT NULL,
	username varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	email varchar(100),
	is_anonymous BOOLEAN DEFAULT FALSE,
	is_verified BOOLEAN DEFAULT FALSE,
	createdAt DATE,
	updatedAt DATE,
	PRIMARY KEY (id)
);

CREATE TABLE session (
	id int NOT NULL AUTO_INCREMENT,
	session_name varchar(100) DEFAULT NULL,
	session_description varchar(100) DEFAULT NULL,
	session_code  varchar(10) DEFAULT NULL,
    session_type ENUM('QAM', 'BRANCHING_QUIZ', 'GROUP_QUIZ') DEFAULT 'QAM' NOT NULL,
	access_token varchar(100) DEFAULT NULL,
	invite_only BOOLEAN DEFAULT FALSE,
	require_verification BOOLEAN DEFAULT FALSE,
	voicechat_enabled BOOLEAN DEFAULT FALSE,
	require_voice_approval BOOLEAN DEFAULT TRUE,
	has_ended BOOLEAN DEFAULT FALSE,
	start_time DATETIME,
	end_time DATETIME,
	createdAt DATE,
	updatedAt DATE,
	PRIMARY KEY (id)
);

CREATE TABLE user_session (
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	session_id int NOT NULL,
	user_group int DEFAULT 0 NOT NULL,
	next_question int DEFAULT 1 NOT NULL,
	relation_type ENUM('ADMIN', 'SPEAKER', 'PARTICIPANT', 'INVITED_PARTICIPANT'),
	group_started bool DEFAULT false,
	createdAt DATE,
	updatedAt DATE,
	PRIMARY KEY (id),
	FOREIGN KEY (user_id)
		REFERENCES user(id)
		ON DELETE CASCADE,
	FOREIGN KEY (session_id)
		REFERENCES session(id)
		ON DELETE CASCADE,
	UNIQUE KEY(user_id, session_id)
);

CREATE TABLE group_session (
	id int NOT NULL AUTO_INCREMENT,
	group_number int NOT NULL,
	session_id int NOT NULL,
	next_question int DEFAULT 1 NOT NULL,
	createdAt DATE,
	updatedAt DATE,
	PRIMARY KEY (id),
	FOREIGN KEY (session_id)
		REFERENCES session(id)
		ON DELETE CASCADE,
	UNIQUE KEY(group_number, session_id)
);

CREATE TABLE question (
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	session_id int NOT NULL,
	question_text varchar(1000),
	question_mode varchar(12),
	question_type varchar(12),
	state ENUM('NEW', 'ANSWERED') DEFAULT 'NEW',
	createdAt DATE,
	updatedAt DATE,
	PRIMARY KEY (id),
	FOREIGN KEY (user_id)
		REFERENCES user(id)
		ON DELETE CASCADE,
	FOREIGN KEY (session_id)
		REFERENCES session(id)
		ON DELETE CASCADE
);

CREATE TABLE vote (
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	question_id int NOT NULL,
	is_upvote BOOLEAN,
	createdAt DATE,
	updatedAt DATE,
	PRIMARY KEY (id),
	FOREIGN KEY (user_id)
		REFERENCES user(id)
		ON DELETE CASCADE,
	FOREIGN KEY (question_id)
		REFERENCES question(id)
		ON DELETE CASCADE
);

CREATE TABLE quiz (
  id int NOT NULL AUTO_INCREMENT,
  session_id int NOT NULL,
  text varchar(1000) NOT NULL,
  type ENUM('MCQ', 'MULTI_MCQ', 'OPEN_ENDED') DEFAULT 'MCQ' NOT NULL,
  question_number int,
  answer_by DATE,

  createdAt DATE,
  updatedAt DATE,
  PRIMARY KEY (id),
  FOREIGN KEY (session_id)
  REFERENCES session(id)
    ON DELETE CASCADE
);

CREATE TABLE quiz_option (
  id int NOT NULL AUTO_INCREMENT,
  quiz_id int NOT NULL,
  text varchar(280),
  option_num int NOT NULL,
  leading_question int DEFAULT 0,

  createdAt DATE,
  updatedAt DATE,
  PRIMARY KEY (id),
  FOREIGN KEY (quiz_id)
  REFERENCES quiz(id)
    ON DELETE CASCADE
);

CREATE TABLE quiz_response (
  id int NOT NULL AUTO_INCREMENT,
  quiz_id int NOT NULL,
  user_id int NOT NULL,
  text varchar(280),

  createdAt DATE,
  updatedAt DATE,
  PRIMARY KEY (id),
  FOREIGN KEY (quiz_id)
  REFERENCES quiz(id)
    ON DELETE CASCADE,
  FOREIGN KEY (user_id)
  REFERENCES user(id)
    ON DELETE CASCADE
);

CREATE TABLE group_response (
  id int NOT NULL AUTO_INCREMENT,
  quiz_id int NOT NULL,
  group_number int NOT NULL,
  text varchar(280),

  createdAt DATE,
  updatedAt DATE,
  PRIMARY KEY (id),
  FOREIGN KEY (quiz_id)
  REFERENCES quiz(id)
    ON DELETE CASCADE
);