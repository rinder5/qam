const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const app = require('../../app.js');
const testHooks = require('../testHelper.js');
const models = require('../../models/index');
const User = models.User;

chai.use(chaiHttp);

describe('Users', async () => {
    before(async () => {
        await testHooks.runBeforeHooks();
    });

    it('Should be able to create new user', async function() {
        const brandNewUsername = 'newUsername';
        const beforeCount = await User.count();
        const response = await chai.request(app)
            .post('/api/users')
            .set('content-type', 'application/json')
            .send({ username: brandNewUsername, password: 'password', email: 'newUsername@gmail.com', user_type: 'ADMIN' });
        expect(response).to.have.status(200);
        const afterCount = await User.count();
        expect(beforeCount).to.be.equal(afterCount - 1);
        const user = await User.findOne({where:{username:brandNewUsername}});
        expect(user.username).to.be.equal(brandNewUsername);
    });
    it('Should be not able to create new user with invalid password', async function() {
        const existingUsername = 'aNewUsername';
        const beforeCount = await User.count();
        try {
            const response = await chai.request(app)
                .post('/api/users')
                .set('content-type', 'application/json')
                .send({
                    username: existingUsername,
                    password: 'pass',
                    email: 'brandnewemail@gmail.com',
                    user_type: 'ADMIN'
                });
        } catch ({ response }) {
            expect(response).to.have.status(400);
            const afterCount = await User.count();
            expect(beforeCount).to.be.equal(afterCount);
            expect(response.text).to.be.equal('Invalid password (Ensure password is at least 8 characters)');
        }
    });
    it('Should be not able to create new user with existing username', async function() {
        const existingUsername = 'baron';
        const beforeCount = await User.count();
        try {
            const response = await chai.request(app)
                .post('/api/users')
                .set('content-type', 'application/json')
                .send({
                    username: existingUsername,
                    password: 'password',
                    email: 'brandnewemail@gmail.com',
                    user_type: 'ADMIN'
                });
        } catch ({ response }) {
            expect(response).to.have.status(400);
            const afterCount = await User.count();
            expect(beforeCount).to.be.equal(afterCount);
            expect(response.text).to.be.equal('Username (' + existingUsername + ') is already in use');
        }
    });
    it('Should be not able to create new user with existing email', async function() {
        const existingEmail = 'baron@gmail.com';
        const beforeCount = await User.count();
        try {
            const response = await chai.request(app)
                .post('/api/users')
                .set('content-type', 'application/json')
                .send({
                    username: 'brandNewUsername',
                    password: 'password',
                    email: existingEmail,
                    user_type: 'ADMIN'
                });
        } catch ({ response }) {
            expect(response).to.have.status(400);
            const afterCount = await User.count();
            expect(beforeCount).to.be.equal(afterCount);
            expect(response.text).to.be.equal('Email (' + existingEmail + ') is already in use');
        }
    });
});
