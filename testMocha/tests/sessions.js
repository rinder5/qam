const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const app = require('../../app.js');
const testHelper = require('../testHelper.js');
const models = require('../../models/index');
const Session = models.Session;
let numCreatedSessions = 0;
const SocketRocket = require('socket-rocket');
const socketAddress = 'http://localhost:' + testHelper.port;

chai.use(chaiHttp);

describe('Sessions', async () => {
    before(async () => {
        await testHelper.runBeforeHooks();
    });

    it('Should be able to create new session as verified user', async function() {
        const newSessionName = 'Created Session #' + numCreatedSessions;
        const newSessionDesc = 'Session Description #' + numCreatedSessions;
        const newSessionCode = 'NSESH' + numCreatedSessions;

        const beforeCount = await Session.count();
        const response = await chai.request(app)
            .post('/api/sessions')
            .set('content-type', 'application/json')
            .send({
                session_name: newSessionName,
                session_description: newSessionDesc,
                session_code: newSessionCode,
                sessionToken: testHelper.userTokens.verified1
            });
        expect(response).to.have.status(200);
        const afterCount = await Session.count();
        expect(beforeCount).to.be.equal(afterCount - 1);
        const session = await Session.findOne({where:{access_token:newSessionCode}});
        expect(session.session_name).to.be.equal(newSessionName);
        expect(session.session_description).to.be.equal(newSessionDesc);
        numCreatedSessions++;
    });

    it('Should not be able to create new session without session name', async function() {
        const newSessionName = undefined;
        const newSessionDesc = 'Session Description #' + numCreatedSessions;
        const newSessionCode = 'NSESH' +  + numCreatedSessions;
        const beforeCount = await Session.count();
        try {
            const response = await chai.request(app)
                .post('/api/sessions')
                .set('content-type', 'application/json')
                .send({
                    session_name: newSessionName,
                    session_description: newSessionDesc,
                    session_code: newSessionCode,
                    sessionToken: testHelper.userTokens.verified1
                });
        } catch ({ response }) {
            expect(response).to.have.status(400);
            const afterCount = await Session.count();
            expect(afterCount).to.be.equal(beforeCount);
            expect(response.text).to.be.equal('Please enter a session name!');
        }
    });

    it('Should not be able to create new session without session description', async function() {
        const newSessionName = 'Created Session #' + numCreatedSessions;
        const newSessionDesc = undefined;
        const newSessionCode = 'NSESH' +  + numCreatedSessions;
        const beforeCount = await Session.count();
        try {
            const response = await chai.request(app)
                .post('/api/sessions')
                .set('content-type', 'application/json')
                .send({
                    session_name: newSessionName,
                    session_description: newSessionDesc,
                    session_code: newSessionCode,
                    sessionToken: testHelper.userTokens.verified1
                });
        } catch ({ response }) {
            expect(response).to.have.status(400);
            const afterCount = await Session.count();
            expect(afterCount).to.be.equal(beforeCount);
            expect(response.text).to.be.equal('Please enter a session description!');
        }
    });

    it('Should not be able to create new session as unverified user', async function() {
        const newSessionName = 'Created Session #' + numCreatedSessions;
        const newSessionDesc = 'Session Description #' + numCreatedSessions;
        const newSessionCode = 'NSESH' + numCreatedSessions;
        const beforeCount = await Session.count();
        try {
            const response = await chai.request(app)
                .post('/api/sessions')
                .set('content-type', 'application/json')
                .send({
                    session_name: newSessionName,
                    session_description: newSessionDesc,
                    session_code: newSessionCode,
                    sessionToken: testHelper.userTokens.unverified1
                });
        } catch ({ response }) {
            expect(response).to.have.status(403);
            const afterCount = await Session.count();
            expect(afterCount).to.be.equal(beforeCount);
            expect(response.text).to.be.equal('User is not verified - this action requires a verified account');
        }
    });

    it('Should be able to enter session as ADMIN and get alerted with new user', async function() {
        const adminSocket = new SocketRocket(socketAddress);
        let response = await adminSocket.executeEventsAndWait('setAdmin', [{
            event:'enterSession',
                data: {
                    sessionToken: testHelper.userTokens.verified1,
                    sessionId: 601
                }
        }]);
        expect(response.isAdmin).to.be.equal(true);

        let unverifiedClientSocket;

        //Connect unverified user and wait for updateUsersList
        response = await adminSocket.executeFunctionAndWait('updateUsersList',
            ()=> {
                unverifiedClientSocket = new SocketRocket(socketAddress);
                unverifiedClientSocket.executeEventsAndWait(undefined, [{
                    event:'enterSession',
                    data: {
                        sessionToken: testHelper.userTokens.unverified1,
                        sessionId: 601
                    }
                }]);
            }
        );
        expect(response['501'].username).to.be.equal('baron');
        expect(response['502'].username).to.be.equal('rebekah');

        //Disconnect unverified user and wait for updateUsersList
        response = await adminSocket.executeFunctionAndWait('updateUsersList',
            ()=> {
                unverifiedClientSocket.kill();
            }
        );
        expect(response['501'].username).to.be.equal('baron');
    });

    it('Should not allow unverified user to enter session with enforced verification', async function() {
        const newSessionName = 'Created Session #' + numCreatedSessions;
        const newSessionDesc = 'Session Description #' + numCreatedSessions;
        const newSessionCode = 'NSESH' + numCreatedSessions;
        const response = await chai.request(app)
            .post('/api/sessions')
            .set('content-type', 'application/json')
            .send({
                session_name: newSessionName,
                session_description: newSessionDesc,
                session_code: newSessionCode,
                sessionToken: testHelper.userTokens.verified1
            });
        // console.log('response = ' + JSON.stringify(response))
        expect(response.status).to.be.equal(200);
        const createdSession = JSON.parse(response.text);
        // console.log('createdSession = ' + JSON.stringify(createdSession))
        let enforceVerificationRespones = await chai.request(app)
            .post('/api/sessions/set-require-verification')
            .set('content-type', 'application/json')
            .send({
                session_id: createdSession.sessionId,
                sessionToken: testHelper.userTokens.verified1,
                require_verification: true
            });


        expect(enforceVerificationRespones.text).to.be.equal('Session\'s require_verification updated to true');

        const unverifiedClientSocket = new SocketRocket(socketAddress);
        const attempt = await unverifiedClientSocket.executeEventsAndWait('enterSessionFailure', [{
            event:'enterSession',
            data: {
                sessionToken: testHelper.userTokens.unverified1,
                sessionId: createdSession.sessionId
            }
        }]);
        expect(attempt).to.be
            .equal('Please verify your account by clicking on the link in the email sent to you, then refresh this page.');
    });
});
