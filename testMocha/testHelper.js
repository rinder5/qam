const sequelize_fixtures = require('sequelize-fixtures');

const dbInitializer = require('../initializers/dbInitializer.js');
const models = require('../models/index');

const chaiHttp = require('chai-http');
const userController = require('../controllers/user.js');

const userTokens = {};

const config = require('../config/config.js');

//Make sure paths are listed in dependency order
//i.e if A references B, then B must be loaded before A
const fixturesPaths = [
    'testMocha/test-fixtures/users.json',
    'testMocha/test-fixtures/sessions.json',
    'testMocha/test-fixtures/user_sessions.json',
    'testMocha/test-fixtures/questions.json'
];

exports.runBeforeHooks = async function () {
    //Prepare database and dummy data (fixtures)
    await dbInitializer.execute();
    await sequelize_fixtures.loadFiles(fixturesPaths, models, {log: function(){/*Disable Logging*/}});
    await setUserTokens();
};

setUserTokens = async function () {
    //Make sure users here all exist in fixtures
    let token;
    let user;

    //Create verified user tokens
    user = { id: 501, username: 'baron' };
    token = await userController.signJwtToken(user);
    userTokens['verified1'] = token;

    //Create unverified user
    user = { id: 502, username: 'rebekah' };
    token = await userController.signJwtToken(user);
    userTokens['unverified1'] = token;
};

exports.userTokens = userTokens;
exports.port = config.port;