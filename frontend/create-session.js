let notLoggedIn = true;

function redirect (url) {
    window.location.href = url;
}

//Create Session form submission
$(function() {
    $('#createSessionForm').submit(function() {
        if (notLoggedIn) {
            showModal('loginModal');
            return false;
        }

        const sessionNameText = document.getElementById("sessionName").value;
        const sessionDescriptionText = document.getElementById("sessionDescription").value;
        const sessionCodeText = document.getElementById("sessionCode").value;
        const sessionTypeValue = document.getElementById("sessionType").value;

        if (!sessionNameText) {
            setErrorMessage('Invalid Session Name');
            return false;
        }

        addMessage('Creating Session... please wait');

        let storedToken = localStorage.getItem("qamToken");
        if (storedToken) {
            storedToken = JSON.parse(storedToken);

            $.ajax({
                type: 'POST',
                url: '/api/sessions',
                data:{
                    session_name: sessionNameText,
                    session_description: sessionDescriptionText,
                    sessionToken: storedToken.sessionToken,
                    session_code: sessionCodeText,
                    session_type: sessionTypeValue
                },
                success:function(data){
                    console.log('POST Session success! - ' + JSON.stringify(data));
                    redirect('/session/' + data.sessionAccessToken);
                },
                error:function(data){
                    setText('createSessionWarningMessage', data.responseText);
                    setElementVisibility('createSessionWarning', true);
                    addMessage('POST Session failed - ' + JSON.stringify(data));
                }
            });
        }
        else {
            console.log('Error: No token');
            setErrorMessage('Not Logged In');
        }
        return false;
    });
});

function addMessage(message) {
    console.log(message);
}

function disableForm() {
    document.getElementById("sessionName").disabled = true;
    document.getElementById("sessionDescription").disabled = true;
    if (!coreAnonymous && !coreVerified) {
        document.getElementById("createSessionSubmitBtn").disabled = true;
    }
}

//Auth-Related (user-bar.js)
addOnSuccessfulAuthLoginCallback(function () {
    if (authJwtResult && authJwtResult.isVerified) {
        notLoggedIn = false;
        setElementVisibility('createSessionWarning', false);
    } else {
        setElementVisibility('createSessionWarning', true);
        setText('createSessionWarningMessage', 'Wait! Verify your email, then refresh this page!');
        disableForm();
    }
});
addOnFailureAuthLoginCallback(function () {
    console.log('addOnFailureAuthLoginCallback');
    setElementVisibility('createSessionWarning', true);
    setText('createSessionWarningMessage', 'Hold up a second! Please login to continue.');
    setText('createSessionSubmitBtn', 'Login');
    disableForm();
});
authenticate();