//Create Session Form
$(function() {
    $('#sessionForm').submit(function() {
        const sessionAccessCode = document.getElementById("sessionCode").value;
        console.log('sessionAccessCode = ' + sessionAccessCode);
        if (sessionAccessCode) {
            $.ajax({
                type: 'GET',
                url: '/api/sessions/exists/' + sessionAccessCode,
                data: {},
                success: function (data) {
                    redirect('/session/' + sessionAccessCode);
                },
                error: function (data) {
                    setElementVisibility ('sessionWarning', true);
                    console.log('[Session Doesnt Exist! ' + sessionAccessCode + '] ');
                    setText('sessionWarningMessage', "Woops!? Session Code " + sessionAccessCode + " is invalid!");
                }
            });
        }
        return false;
    });
});

authenticate();