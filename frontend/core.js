function redirect (url) {
    console.log('Redirecting... ' + url);
    window.location.href = url;
}

function addMessage(message) {
    // var text = document.createTextNode(message),
    //     el = document.createElement('li'),
    //     messages = document.getElementById('messages');
    //
    // el.appendChild(text);
    // messages.appendChild(el);
    // console.log(message);
}


function showWarningMessage (warningMessageId, message) {
    const warningMessage = document.getElementById(warningMessageId);
    if (warningMessage) {
        console.log('warningMessage = ' + warningMessage);
        let className = warningMessage.className;
        console.log('className = ' + className);
        if (className.indexOf('hidden') > 0) {
            className = className.replace(' hidden', '');
            warningMessage.className = className;
            console.log('className = ' + className);

        }
    } else {
        console.log('Warning Message not found. ID = ' + warningMessageId);
    }
}

function setElementVisibility (elementId, isNewVisible) {
    const el = document.getElementById(elementId);
    if (el) {
        let className = el.className;
        let hasHiddenInClass = className.indexOf('hidden') > -1;

        if (!hasHiddenInClass && !isNewVisible) {
            className = className + ' hidden';
            el.className = className;
        } else if (hasHiddenInClass && isNewVisible) {
            className = className.replace('hidden', '');
            el.className = className;
        }
    } else {
        console.log('setElementVisibility fail, elementId = ' + elementId);
    }
}

function setText (elementId, newText) {
    const el = document.getElementById(elementId);
    if (el) {
        el.innerHTML = newText;
    } else {
        console.log('setText fail, elementId = ' + elementId);
    }
}

function showModal(modalId) {
    $('#' + modalId).modal();
}
function setModalVisibility(modalId, isVisible) {
    if (isVisible) {
        $('#' + modalId).modal();
    } else {
        $('#' + modalId).modal('hide');
    }
}
