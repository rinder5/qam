//Admin Variables

//Other Variables
let sessionQuestions = [];
let sessionQuizzes = [];
let sessionGroups = [];
let sessionQuizResponses = [];
let sessionQuizResponsesByGroup = [];
let sessionQuizOptions = [];
let currentQuizIndex = 0;
let currentGroupIndex = 0;
let sessionUsers = [];
let sessionId;
let sessionType = 'QAM';

function promptUserRelogin() {
    setLoginBarVisible(true);
}
function setLoginBarVisible (isVisible) {
    setModalVisibility('login-modal', isVisible);
}
function getSessionAccessCodeFromUrl() {
    const currentUrl = window.location.href;
    const indexOfLastSlash = currentUrl.lastIndexOf('/')
    const sessionAccessCode = currentUrl.substr(indexOfLastSlash+1, currentUrl.length);
    return sessionAccessCode;
}
         
async function populateData() {
    authenticate();
    await checkIfAuthorized();
    await getSessionDetails();
    await getQuestions();
    await getQuizzes();
    await getUsers();
    await getQuizzesByGroup();
    showData();
}
async function checkIfAuthorized() {
    const sessionAccessCode = getSessionAccessCodeFromUrl();
    $.ajax({
        type: 'GET',
        url: '/api/sessions/exists/' + sessionAccessCode,
        data: {},
        success: function (data) {
            sessionId = data.id;
            let storedToken = localStorage.getItem("qamToken");
            if (storedToken) {        
                storedToken = JSON.parse(storedToken);
                const sessionToken = storedToken.sessionToken;
                $.ajax({
                    type: 'POST',
                    url: '/api/authenticateJwt',
                    data: {
                        sessionToken: sessionToken
                    },
                    success: function (data2) {
                        userId = data2.userId;

                        $.ajax({
                            type: 'POST',
                            url: '/api/sessions/findAdminUserSession',
                            data: {
                                userId: userId,
                                sessionId: sessionId
                            },
                            error: function () {
                                window.location.href = '../';
                            }
                        })
                    },
                    error: function () {
                        window.location.href = '../';
                    }
                });
            } else {
                window.location.href = '../';
            }
        },
        error: function (data) {
            window.location.href = '../';
        }
    });
}
async function getSessionDetails() {
    const sessionAccessCode = getSessionAccessCodeFromUrl();
    const result = await $.ajax({
        type: 'GET',
        url: '/api/sessions/exists/' + sessionAccessCode,
        data: {
        },
        success: function (data) {
            document.getElementById("session-title").innerHTML = data.session_name;
            document.getElementById("session-description").innerHTML = data.session_description;
            sessionId = data.id;
            sessionType = data.session_type;
        }
    });
    return result;
}
async function getQuestions() {
    const result = await $.ajax({
        type: 'POST',
        url: '/api/questions/getQuestionsFromSession',
        data: {
            sessionId: sessionId
        },
        success:function(data){
            sessionQuestions = data;
        }
    });
    return result;
}
async function getQuizzes() {
    const result = await $.ajax({
        type: 'POST',
        url: '/api/quizzes/getQuizzesFromSession',
        data: {
            sessionId: sessionId
        },
        success:function(data){
            console.log("quiz data: " + JSON.stringify(data));
            sessionQuizzes = data.quizzes;
            sessionQuizOptions = data.quizOptions;
            sessionQuizResponses = data.quizResponses;
        }
    });
    return result;
}
async function getQuizzesByGroup() {
    const result = await $.ajax({
        type: 'POST',
        url: '/api/quizzes/getQuizzesFromSessionByGroup',
        data: {
            sessionId: sessionId
        },
        success:function(data){
            sessionQuizResponsesByGroup = data.quizResponses;
            console.log("sessionquizresponsesbygroup:");
            console.log(JSON.stringify(sessionQuizResponsesByGroup));
            sessionGroups.push(-1); //placeholder for 'everyone'
            for (let i=0; i<sessionQuizResponsesByGroup.length; i++) {
                if (!sessionGroups.includes(sessionQuizResponsesByGroup[i].user_group))
                    sessionGroups.push(sessionQuizResponsesByGroup[i].user_group);
            }
            sessionGroups = sessionGroups.sort();
        }
    });
    return result;
}
async function getUsers() {
    const result = await $.ajax({
        type: 'POST',
        url: '/api/users/getUsersFromSession',
        data: {
            sessionId: sessionId
        },
        success:function(data){
            sessionUsers = data;
        }
    });
    return result;
}
function htmlEscape(str) {
    return str
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}
function sortTable(n, tableId) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc"; 
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir == "asc") {
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
            } else if (dir == "desc") {
            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount ++; 
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
            dir = "desc";
            switching = true;
            }
        }
    }
}
function sortNumericalTable(n, tableId) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc"; 
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir == "asc") {
                if (Number(x.innerHTML) > Number(y.innerHTML)) {
                    shouldSwitch = true;
                    break;
                  }
            } else if (dir == "desc") {
                if (Number(x.innerHTML) < Number(y.innerHTML)) {
                    shouldSwitch = true;
                    break;
                  }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount ++; 
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
            dir = "desc";
            switching = true;
            }
        }
    }
}
function showData() {
    
    let htmlMessage = '<div class="masonry-brick">' + 
               '<div class="card text-center">' +
    '<div class="card-body">';
    //Questions
    if (sessionType == 'QAM') {
        htmlMessage += '<p><button class="accordion"><b>Questions from Audience ('
        + sessionQuestions.length + ')</b></button><div class="panel">';
        let closedQuestions = [];
        let openQuestions = [];
        for (let i = 0; i < sessionQuestions.length; i++) {
            if (sessionQuestions[i].state == "NEW")
                openQuestions[openQuestions.length] = sessionQuestions[i];
            else
                closedQuestions[closedQuestions.length] = sessionQuestions[i];
            }
        htmlMessage += '<p><button class="accordion"><b>Answered Questions ('
        + closedQuestions.length + ')</b></button><div class="panel">'
        + '<center><table id="table1"><tr><th onclick="sortTable(0, \'table1\')">Question &#8645</th><th onclick="sortTable(1, \'table1\')">User &#8645</th><th onclick="sortNumericalTable(2, \'table1\')">Votes &#8645</th></tr>';
        for (let i = 0; i < closedQuestions.length; i++) {
            htmlMessage += '<tr><td>' + htmlEscape(closedQuestions[i].question_text) + '</td><td>' + closedQuestions[i].User.username + '</td><td>' + closedQuestions[i].Votes.length + '</td></tr>';
        }
        htmlMessage += "</table></center></div><br>";
        htmlMessage += '<p><button class="accordion"><b>Unanswered Questions ('
        + openQuestions.length + ')</b></button><div class="panel">'
        + '<center><table id="table2"><tr><th onclick="sortTable(0, \'table2\')">Question &#8645</th><th onclick="sortTable(1, \'table2\')">User &#8645</th><th onclick="sortNumericalTable(2, \'table2\')">Votes &#8645</th></tr>';
        for (let i = 0; i < openQuestions.length; i++) {
            htmlMessage += '<tr><td>' +  htmlEscape(openQuestions[i].question_text) + '</td><td>' + openQuestions[i].User.username + '</td><td>' + openQuestions[i].Votes.length + '</td></tr>';
        }
        htmlMessage += "</table></center></div>";
        htmlMessage += "</div>";
        htmlMessage += "</p>";
        htmlMessage += "<p></p>";
    }
    //Quizzes
    htmlMessage += '<p><button class="accordion"><b>Quizzes ('
     + sessionQuizzes.length + ')</b></button><div class="panel" id="quizAccordion">';
    let mcqQuizzes = [];
    let multipleMcqQuizzes = [];
    for (let i = 0; i < sessionQuizzes.length; i++) {
        if (sessionQuizzes[i].type == "MCQ")
         mcqQuizzes[mcqQuizzes.length] = sessionQuizOptions[sessionQuizzes[i].id];
        else if (sessionQuizzes[i].type == "MULTI_MCQ")
            multipleMcqQuizzes[multipleMcqQuizzes.length] = sessionQuizOptions[sessionQuizzes[i].id];
    }
    htmlMessage += '<p>';
    //buttons for navigating questions
    htmlMessage += '<div class="session-questions-filter"><div class="btn-group btn-group-justified" role="group"><div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleQuiz(false)">'
     + '<i class="fas fa-caret-left"></i></button></div>'
     + '<div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleQuiz(true)">Question '
     + (currentQuizIndex + 1) + '</button></div>'
     + '<div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleQuiz(true)">'
     + '<i class="fas fa-caret-right"></i></button></div></div></div><br>';
    //buttons for navigating groups
    htmlMessage += '<div class="session-questions-filter"><div class="btn-group btn-group-justified" role="group"><div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleGroup(false)">'
    + '<i class="fas fa-caret-left"></i></button></div>'
    + '<div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleGroup(true)">Group '
    + (currentGroupIndex + 1) + '</button></div>'
    + '<div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleGroup(true)">'
    + '<i class="fas fa-caret-right"></i></button></div></div></div><br>';

    for (let i = 0; i < sessionQuizzes.length; i++) {
        htmlMessage += JSON.stringify(sessionQuizzes[i]) + '<br />';
    }
    htmlMessage += "</div>";
    htmlMessage += "</p>";
    htmlMessage += "<p></p>";

    //Users
    if (sessionType == 'QAM') {
        htmlMessage += '<p><button class="accordion"><b>Users in Session ('
        + sessionUsers.length + ')</b></button><div class="panel">';
        htmlMessage += '<p><button class="accordion"><b>User Participation</b></button><div class="panel">'
        + '<center><table id="table3"><tr><th onclick="sortTable(0, \'table3\')">Username &#8645</th><th onclick="sortTable(1, \'table3\')">Questions Asked &#8645</th><th onclick="sortNumericalTable(2, \'table3\')">Votes Given &#8645</th><th onclick="sortNumericalTable(3, \'table3\')">Quizzes Answered &#8645</th></tr>';
        for (let i = 0; i < sessionUsers.length; i++) {
            htmlMessage += '<tr><td>' + sessionUsers[i].username + '</td><td>';
            if (sessionUsers[i].questionsAsked === undefined)
                htmlMessage += '0</td><td>';
            else
                htmlMessage += sessionUsers[i].questionsAsked + '</td><td>';
            if (sessionUsers[i].upvotesGiven === undefined)
                htmlMessage += '0</td><td>';
            else
                htmlMessage += sessionUsers[i].upvotesGiven + '</td><td>';
            if (sessionUsers[i].quizzesAnswered === undefined)
                htmlMessage += '0</td></tr>';
            else
                htmlMessage += sessionUsers[i].quizzesAnswered + '</td></tr>';
        }
        htmlMessage += "</table></center></div></p>";
        htmlMessage += '<p><button class="accordion"><b>User Quality</b></button><div class="panel">'
        + '<center><table id="table4"><tr><th onclick="sortTable(0, \'table4\')">Username &#8645</th><th onclick="sortNumericalTable(1, \'table4\')">Upvotes Gained &#8645</th></tr>';
        for (let i = 0; i < sessionUsers.length; i++) {
            htmlMessage += '<tr><td>' + sessionUsers[i].username + '</td><td>';
            if (sessionUsers[i].upvotesGained === undefined)
                htmlMessage += '0</td></tr>';
            else
                htmlMessage += sessionUsers[i].upvotesGained + '</td></tr>';
        }
        htmlMessage += "</table></center></div></p>";
        htmlMessage += '<p><button class="accordion"><b>User Details</b></button><div class="panel">'
        + '<center><table id="table5"><tr><th onclick="sortTable(0, \'table5\')">Username &#8645</th><th onclick="sortTable(1, \'table5\')">Email &#8645</th><th onclick="sortNumericalTable(2, \'table5\')">Group &#8645</th><th onclick="sortTable(3, \'table5\')">Status &#8645</th></tr>';
        for (let i = 0; i < sessionUsers.length; i++) {
            htmlMessage += '<tr><td>' + sessionUsers[i].username + '</td><td>' + sessionUsers[i].email + '</td><td>' + sessionUsers[i].user_group + '</td><td>';
            if (sessionUsers[i].is_anonymous)
                htmlMessage += 'Anonymous</td></tr>';
            else
                htmlMessage += 'Registered</td></tr>';
        }
        htmlMessage += "</table></center></div></p>";
        htmlMessage += "</div></p>";
    } else {//session is quiz type
        htmlMessage += '<p><button class="accordion"><b>Users in Session ('
        + sessionUsers.length + ')</b></button><div class="panel">';
        htmlMessage += '<p><button class="accordion"><b>User Participation</b></button><div class="panel">'
        + '<center><table id="table6"><tr><th onclick="sortTable(0, \'table6\')">Username &#8645</th><th onclick="sortNumericalTable(1, \'table6\')">Quizzes Answered &#8645</th></tr>';
        for (let i = 0; i < sessionUsers.length; i++) {
            htmlMessage += '<tr><td>' + sessionUsers[i].username + '</td><td>';
            if (sessionUsers[i].quizzesAnswered === undefined)
                htmlMessage += '0</td></tr>';
            else
                htmlMessage += sessionUsers[i].quizzesAnswered + '</td></tr>';
        }
        htmlMessage += "</table></center></div></p>";
        htmlMessage += '<p><button class="accordion"><b>User Details</b></button><div class="panel">'
        + '<center><table id="table7"><tr><th onclick="sortTable(0, \'table7\')">Username &#8645</th><th onclick="sortTable(1, \'table7\')">Email &#8645</th><th onclick="sortNumericalTable(2, \'table7\')">Group &#8645</th><th onclick="sortTable(3, \'table7\')">Status &#8645</th></tr>';
        for (let i = 0; i < sessionUsers.length; i++) {
            htmlMessage += '<tr><td>' + sessionUsers[i].username + '</td><td>' + sessionUsers[i].email + '</td><td>' + sessionUsers[i].user_group + '</td><td>';
            if (sessionUsers[i].is_anonymous)
                htmlMessage += 'Anonymous</td></tr>';
            else
                htmlMessage += 'Registered</td></tr>';
        }
        htmlMessage += "</table></center></div></p>";
        htmlMessage += "</div></p>";
    }
    htmlMessage += "</div></div></div>";

    document.getElementById('container-data-cards').innerHTML = htmlMessage;

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            /* Toggle between adding and removing the "active" class,
            to highlight the button that controls the panel */
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            var panel = this.parentElement.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }

    currentQuizIndex--;
    cycleQuiz(true);
}

function cycleGroup (isRight) {
    if (isRight) {
        currentGroupIndex++;
        if (currentGroupIndex >= sessionGroups.length) {
            currentGroupIndex = 0;
        }
    } else {
        currentGroupIndex--;
        if (currentGroupIndex < 0) {
            currentGroupIndex = sessionGroups.length - 1;
        }
    }
    currentQuizIndex--;
    cycleQuiz(true);
}

function cycleQuiz (isRight) {
    if (isRight) {
        currentQuizIndex++;
        if (currentQuizIndex >= sessionQuizzes.length) {
            currentQuizIndex = 0;
        }
    } else {
        currentQuizIndex--;
        if (currentQuizIndex < 0) {
            currentQuizIndex = sessionQuizzes.length - 1;
        }
    }
    let htmlMessage = '<p>';
    //displaying data
    let quizResponses = [];
    let quizResponseCount = [];
    let indexCount = [];
    let quizOptions;
    let words = [];
    if (currentGroupIndex == 0) {
        for (let i=0; i< sessionQuizResponses.length; i++) {
            if (sessionQuizResponses[i].quiz_id ==  sessionQuizzes[currentQuizIndex].id) {
                quizResponses[quizResponses.length] = sessionQuizResponses[i].text;
                quizResponseCount[quizResponseCount.length] = sessionQuizResponses[i].Count;
            }
        }
        if (sessionQuizzes[currentQuizIndex].type == "OPEN_ENDED") {
            htmlMessage += "<h3>" + sessionQuizzes[currentQuizIndex].text + "</h3>"
            + '<center><table id="table8"><tr><th onclick="sortTable(0, \'table8\')">Answers &#8645</th><th onclick="sortNumericalTable(1, \'table8\')">Count &#8645</th></tr>';
            for (let i = 0; i < quizResponses.length; i++) {
                htmlMessage += '<tr><td>' + quizResponses[i] + '</td><td>' + quizResponseCount[i] + '</td></tr>';
                let wordArray = quizResponses[i].split(/[ '!"?;.\(,\)]+/);
                for (let j=0; j<wordArray.length; j++) {
                    if (words[wordArray[j]] == undefined)
                        words[wordArray[j]] = 1;
                    else
                        words[wordArray[j]]++;
                }
            };
            htmlMessage += "</table>"
            + '<div id="responseChart" width="200" height="100" class="jqcloud"></center></div>';
        } else {
            quizOptions = sessionQuizOptions[sessionQuizzes[currentQuizIndex].id];
            for (let i = 0; i < quizOptions.length; i++) {
                if (quizResponses.includes(i + "")) {
                    indexCount[i] = quizResponseCount[quizResponses.indexOf(i + "")]
                } else {
                    indexCount[i] = 0;
                }
            }
            htmlMessage += "<h3>" + sessionQuizzes[currentQuizIndex].text + "</h3>"
            + '<canvas id="responseChart" width="200" height="100"></canvas>';
        }
    } else { //group data
        for (let i=0; i< sessionQuizResponsesByGroup.length; i++) {
            if (sessionQuizResponsesByGroup[i].quiz_id ==  sessionQuizzes[currentQuizIndex].id && sessionQuizResponsesByGroup[i].user_group == sessionGroups[currentGroupIndex]) {
                quizResponses[quizResponses.length] = sessionQuizResponsesByGroup[i].text;
                quizResponseCount[quizResponseCount.length] = sessionQuizResponsesByGroup[i].Count;
            }
        }
        if (sessionQuizzes[currentQuizIndex].type == "OPEN_ENDED") {
            htmlMessage += "<h3>" + sessionQuizzes[currentQuizIndex].text + "</h3>"
            + '<center><table id="table9"><tr><th onclick="sortTable(0, \'table9\')">Answers &#8645</th><th onclick="sortNumericalTable(1, \'table9\')">Count &#8645</th></tr>';
            for (let i = 0; i < quizResponses.length; i++) {
                htmlMessage += '<tr><td>' + htmlEscape(quizResponses[i]) + '</td><td>' + quizResponseCount[i] + '</td></tr>';
                let wordArray = quizResponses[i].split(/[ '!"?;.\(,\)]+/);
                for (let j=0; j<wordArray.length; j++) {
                    if (words[wordArray[j]] == undefined)
                        words[wordArray[j]] = 1;
                    else
                        words[wordArray[j]]++;
                }
            };
            htmlMessage += "</table>"
            + '<div id="responseChart" width="200" height="100" class="jqcloud"></center></div>';
        } else {
            quizOptions = sessionQuizOptions[sessionQuizzes[currentQuizIndex].id];
            for (let i = 0; i < quizOptions.length; i++) {
                if (quizResponses.includes(i + "")) {
                    indexCount[i] = quizResponseCount[quizResponses.indexOf(i + "")]
                } else {
                    indexCount[i] = 0;
                }
            }
            htmlMessage += "<h3>" + sessionQuizzes[currentQuizIndex].text + "</h3>"
            + '<canvas id="responseChart" width="200" height="100"></canvas>';
        }
    }
    //buttons for navigating questions
    htmlMessage += '<div class="session-questions-filter"><div class="btn-group btn-group-justified" role="group"><div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleQuiz(false)">'
    + '<i class="fas fa-caret-left"></i></button></div>'
    + '<div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleQuiz(true)">Question '
    + (currentQuizIndex + 1) + '</button></div>'
    + '<div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleQuiz(true)">'
    + '<i class="fas fa-caret-right"></i></button></div></div></div>';
    //buttons for navigating groups
    htmlMessage += '<div class="session-questions-filter"><div class="btn-group btn-group-justified" role="group"><div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleGroup(false)">'
    + '<i class="fas fa-caret-left"></i></button></div>'
    + '<div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleGroup(true)">';
    
    if (currentGroupIndex == 0)
        htmlMessage += 'All Users';
    else
        htmlMessage += 'Group ' + sessionGroups[currentGroupIndex];
    htmlMessage += '</button></div>'
    + '<div class="btn-group" role="group"><button class="btn btn primary btn-block btn-lg" type="button" onclick="cycleGroup(true)">'
    + '<i class="fas fa-caret-right"></i></button></div></div></div><br>';

    document.getElementById("quizAccordion").innerHTML = htmlMessage;

    if (sessionQuizzes[currentQuizIndex].type == "OPEN_ENDED") {
        let keys = Object.keys(words);
        let wordsWeights = [];
        for (let i=0; i<keys.length; i++) {
            wordsWeights.push({text: keys[i], weight: words[keys[i]]});
        }
        $('#responseChart').jQCloud(wordsWeights, {
            width: 500,
            height: 200
          });
    }
    if (sessionQuizzes[currentQuizIndex].type != "OPEN_ENDED") {
        var ctx = document.getElementById("responseChart");
        if (quizOptions.length == 2) {
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [quizOptions[0], quizOptions[1]],
                    datasets: [{
                        label: '# of Votes',
                        data: [indexCount[0], indexCount[1]],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        } else if (quizOptions.length == 3) {
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [quizOptions[0], quizOptions[1], quizOptions[2]],
                    datasets: [{
                        label: '# of Votes',
                        data: [indexCount[0], indexCount[1], indexCount[2]],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        } else {
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [quizOptions[0], quizOptions[1], quizOptions[2], quizOptions[3]],
                    datasets: [{
                        label: '# of Votes',
                        data: [indexCount[0], indexCount[1], indexCount[2], indexCount[3]],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        }
    }
}

//View Generators
function generateNoSessionsCard(text) {
    let card = '' +
        '<div class="masonry-brick">' +
            '<div class="card text-center">' +
                '<div class="card-body">' +
                    '<h4 class="card-title">' + text + '</h4>' +
                '</div>' +
            '</div>' +
        '</div>';
    return card;
}
function generateSessionCard (user_id, question_text) {
    let card = '' +
        '<div class="masonry-brick">' +
        '<div class="card text-center">';

    card +=
        '<div class="card-header">' +
        '<span>' + user_id + '</span>' +
        '</div>';

    card +=
        '<div class="card-body">' +
        '<h4 class="card-title">' + question_text + '</h4>' +
        '</div>';

    card +=
        '<div class="card-footer text-muted">' +
        '<div class="container-fluid">';

    card +=
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return card;
}

populateData();