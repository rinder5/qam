

$(function() {
    $('.message a').click(function(){
        $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    });

    $('#registerForm').submit(function(event) {
        event.preventDefault();

        addMessage('Creating account... please wait');
        document.getElementById("regMessage").innerHTML  = 'Creating...';

        const username = document.getElementById("regUsername").value;
        const password = document.getElementById("regPassword").value;
        const email = document.getElementById("regEmail").value;
        document.getElementById("regUsername").value = '';
        document.getElementById("regPassword").value = '';
        document.getElementById("regEmail").value = '';

        if (!username || !password) {
            addMessage('Please enter a valid username/password/email');
            document.getElementById("regMessage").innerHTML  = 'Invalid Username/Password';
            return false;
        }
        if (!email) {
            document.getElementById("regMessage").innerHTML  = 'Invalid Email Address';
        }

        $.ajax({
            type: 'POST',
            url: '/api/users',
            data:{
                username: username,
                password: password,
                email: email,
                user_type: "ADMIN"
            },
            success:function(data){
                addMessage('POST User success! - ' + JSON.stringify(data));
                redirect('/login');
            },
            error:function(data){
                addMessage('POST User failed - ' + JSON.stringify(data.responseText));
                document.getElementById("regMessage").innerHTML  = data.responseText;
            }
        });
    });

    $('#loginForm').submit(function() {
        loginButtonPressed();
    });
});


function loginButtonPressed() {
    console.log('loginButtonPressed');
    $.ajax({
        type: 'POST',
        url: '/api/login',
        data:$('#loginForm').serialize(),
        success:function(data){
            const qamToken = {
                username: data.username,
                userId: data.userId,
                isAnonymous: data.isAnonymous,
                email: data.email,
                sessionId: 1,
                sessionToken: data.sessionToken
            };
            localStorage.setItem("qamToken", JSON.stringify(qamToken));
            addMessage('Token = ' + JSON.stringify(data.sessionToken));
            redirect('/');
        },
        error:function(data){
            addMessage('Login failed - ' + JSON.stringify(data));
            document.getElementById("loginMessage").innerHTML  = "Invalid Username/Password";
        }
    });
    return false;
}

function addMessage(message) {
    console.log(message);
}

function redirect (url) {
    window.location.href = url;
}