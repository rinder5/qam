//Admin Variables
let isAdmin = false;
let adminQuizData = {};

//Other Variables
const viewingOptions = ['OPEN', 'CLOSED'];
let allSessions = [];
let openSessions = [];
let closedSessions = [];
let currentViewingIndex = 0;
let sessionId;
let userId;

function promptUserRelogin() {
    setLoginBarVisible(true);
}
function setLoginBarVisible (isVisible) {
    setModalVisibility('login-modal', isVisible);
}
/*
function compareSession (a,b) {
    if (a.voteCount > b.voteCount)
        return -1;
    if (a.voteCount < b.voteCount)
        return 1;
    return 0;
}
function updateSessions (sessions) {
    allsessions = sessions;
    showCurrViewingsessions();
}
*/
function populateSessions() {
    authenticate();
    let storedToken = localStorage.getItem("qamToken");
    if (storedToken) {
        storedToken = JSON.parse(storedToken);
        const sessionToken = storedToken.sessionToken;
        $.ajax({
            type: 'POST',
            url: '/api/viewSessions',
            data: {
                sessionToken: sessionToken,
                userId: storedToken.userId
            },
            success:function(data){
                console.log("success: " + data.length + " sessions found.");
                //if (data) {
                    allSessions = data;
                    for (let i = 0; i < allSessions.length; i++) {
                        if (allSessions[i].has_ended)
                            closedSessions[closedSessions.length] = allSessions[i];
                        else
                            openSessions[openSessions.length] = allSessions[i];
                    }
                    showCurrViewingSessions();
                //}
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                console.log("Status: " + textStatus);
                console.log("Error: " + errorThrown); 
            }
        });
    }
}
function showCurrViewingSessions() {
    let sessions = allSessions;
    if (currentViewingIndex == 0)
        sessions = openSessions;
    else
        sessions = closedSessions;
    let sessionCount = 0;
    let str = '';

    //sessions.sort(comparesession);

    for (let i = 0; i < sessions.length; i++) {
        const q = sessions[i];
        let sessionCard = generateSessionCard(q.session_name, q.session_description, q.session_code, currentViewingIndex);
        str += sessionCard;
        sessionCount++;
    }
    if (sessionCount === 0) {
        const text = 'No sessions have been made... yet!';
        str += generateNoSessionsCard(text);
    }
    document.getElementById('container-session-cards').innerHTML = str;
}
//View Generators
function generateNoSessionsCard(text) {
    let card = '' +
        '<div class="masonry-brick">' +
            '<div class="card text-center">' +
                '<div class="card-body">' +
                    '<h4 class="card-title">' + text + '</h4>' +
                '</div>' +
            '</div>' +
        '</div>';
    return card;
}
function generateSessionCard (session_name, session_description, session_code, isClosed) {
    let card = '' +
        '<div class="masonry-brick">' +
        '<div class="card text-center">';

    card +=
        '<div class="card-header">' +
        '<span>' + session_name + '</span>' +
        '</div>';

    card +=
        '<div class="card-body">' +
        '<h4 class="card-title">' + session_description + '</h4>' +
        '</div>';

    card +=
        '<div class="card-footer text-muted">' +
        '<div class="container-fluid">';

    if (isClosed) {
        card += '<div class="row">' +
                '<button class="btn btn-lg btn-block btn-primary" ' +
                'onclick="otherRedirect(\'' +
                session_code +
                '\')">View</button>' +
                '</div>'
    } else {
        card += '<div class="row">' +
                '<button class="btn btn-lg btn-block btn-primary" ' +
                'onclick="redirect(\'' +
                session_code +
                '\')">View</button>' +
                '</div>'
    }

    card +=
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return card;
}
function redirect(session_code) {
    window.location.href = "session/" + session_code;
}
function otherRedirect(session_code) {
    window.location.href = "closedsession/" + session_code;
}
function cycleViewingOption (isRight) {
    if (isRight) {
        currentViewingIndex++;
        if (currentViewingIndex >= viewingOptions.length) {
            currentViewingIndex = 0;
        }
    } else {
        currentViewingIndex--;
        if (currentViewingIndex < 0) {
            currentViewingIndex = viewingOptions.length - 1;
        }
    }
    showCurrViewingSessions();
    document.getElementById('viewingOptionText').innerHTML = viewingOptions[currentViewingIndex];
}

populateSessions();