//Admin Variables
let isAdmin = false;
let adminQuizData = {};
let quizGroupResponses = [];
let groupIds = [];
let questionNumbers = [];
let currViewingQuizResponseIndex = 0;
let currViewingQuizGroupIndex = 0;
let currAskingQuestionModalIndex = 0;
const quizTypes = ['MCQ', 'MULTI_MCQ', 'OPEN_ENDED'];
let adminCurrSelectedQuizType = quizTypes[0];
let allowUnverifiedUsers = true;
let allowUninvitedUsers = true;
let requireVoiceApproval = true;
let allowVoicechat = false;
let voiceConnections = [];

//Other Variables
const viewingOptions = ['NEW', 'ANSWERED'];
let allQuestions = [];
let allowedAnswers = [1, 1, 1, 1];
let errorMessage = '';
let currentViewingIndex = 0;
let sessionEnded = false;
let groupRound = false;
let groupNumber = 0;
let voiceConnecting = false;
let sessionId;
let sessionType;
let userId;
let currQuiz;

const socket = io.connect('', {});
socket.on('error', console.error.bind(console));
socket.on('message', function(data){
    addMessage(data);
});
socket.on('updateUsersList', function(data) {
    updateUsers(data);
});
socket.on('invalidEnterSession', function(data) {
    addMessage('invalidEnterSession = ' + JSON.stringify(data));
});
socket.on('updateQuestions', function(data) {
    updateQuestions(data);
});
socket.on('updateAdminQuizzes', function(data) {
    updateAdminQuizzes(data);
    if (sessionType != "QAM")
        updateQuizFormat(sessionType);
});
socket.on('updateQuizResponse', function(data) {
    updateAdminQuizResponse(data);
});
socket.on('updateQuizGroupResponse', function(data) {
    updateAdminGroupDetails(data);
});
socket.on('updateGroupResponses', function(data) {
    updateGroupResponses(data);
});
socket.on('requestVoiceConnection', function(data) {
    requestVoiceConnection(data);
});
socket.on('closeVoiceConnection', function(data) {
    closeVoiceConnection(data);
});
socket.on('kickVoiceConnection', function(data) {
    let storedToken = localStorage.getItem("qamToken");
    let username;
    if (storedToken) {        
        storedToken = JSON.parse(storedToken);
        username = storedToken.username;
    } else {
        let anonQamToken = localStorage.getItem("anonQamToken");
        anonQamToken = JSON.parse(anonQamToken);
        username = anonQamToken.username;
    }
    if (data == username)
        kickVoiceConnection();
});
socket.on('acceptVoiceConnection', function(data) {
    let storedToken = localStorage.getItem("qamToken");
    let username;
    if (storedToken) {        
        storedToken = JSON.parse(storedToken);
        username = storedToken.username;
    } else {
        let anonQamToken = localStorage.getItem("anonQamToken");
        anonQamToken = JSON.parse(anonQamToken);
        username = anonQamToken.username;
    }
    if (data == username)
        acceptVoiceConnection();
});
socket.on('promptQuiz', function(data) {
    promptQuiz(data);
});
socket.on('setAdmin', function(data) {
    isAdmin = data.isAdmin;
    allowUnverifiedUsers = !data.require_verification;
    allowUninvitedUsers = !data.invite_only;
    showCurrViewingQuestions();
    selectQuizType();   //Force Ask Quiz modal to update DOM, even if page refreshed
});
socket.on('updateAdminData', function(data) {
    allowUninvitedUsers = !data.invite_only;
    allowUnverifiedUsers = !data.require_verification;
    updateSessionOptionsModal();
});
socket.on('enterSessionSuccess', function(data) {
    setBottomBar();
    setElementVisibility('viewingOptionFilter', true);
});
socket.on('enterSessionFailure', function(data) {
    const failCard = generateNoQuestionsCard(data);
    document.getElementById('container-question-cards').innerHTML = failCard;
    promptUserRelogin();
});
socket.on('forceReAuthAndRefresh', function(data){
    revokeAuthAndRefresh();
    console.log('Reason - ' + data);
});
socket.on('redirect', function(data) {
  redirect(data);
});
socket.on('notifySessionEnd', function(data) {
    document.getElementById('session-description').innerHTML
        = 'The session has ended. A session report has been sent to the admin.';
    setModalVisibility('sessionOptionsModal', false);
});
socket.on('forceRefresh', function(data){
    window.location.reload();
});

//jQuery Form | Selector triggers
$(function() {
    $('#form-question').submit(function() {
        submitQuestion();
        return false;
    });
    //quizform
    $('#quizForm').submit(function() {
        submitQuiz();
        return false;
    });
    $('#createQuiz').click(function() {
        createQuiz();
        return false;
    });
    $('#quizResponseForm').submit(function() {
        submitQuizResponse(currQuiz.type);
        return false;
    });
    $('#inviteUsersByEmailForm').submit(function() {
        inviteUsersByEmail();
        return false;
    });
    $('#inviteUsersByCSVForm').submit(function() {
        inviteUsersByCSV();
        return false;
    });
    $('#postQuestionTypeSelect').change(function(){
        selectQuizType();
    });
    //Session Options Date Range
    $('#sessionOptionDateRange').daterangepicker({
        timePicker: true,
        timePickerIncrement: 15,
        locale: {
            format: 'DD/MM/YYYY h:mm A'
        }
    }, function(start, end, label) {
        changeSessionDates(start, end)
    });
    $('#allowUninvitedCheckbox').change(function(){
        onAllowUninvitedCheckboxChange();
    });
    $('#allowUnverifiedCheckbox').change(function(){
        onAllowUnverifiedCheckboxChange();
    });
    $('#allowVoicechatCheckbox').change(function(){
        onAllowVoicechatCheckboxChange();
    });
    $('#requireApprovalCheckbox').change(function(){
        onRequireApprovalCheckboxChange();
    });

});

function submitQuiz() {
    let keys = [];
    for (let key in adminQuizData) {
        keys.push(key);
    }
    if (currAskingQuestionModalIndex == keys.length) {
        const quizText = document.getElementById("quizText").value;
        const option1 = document.getElementById("quizOption1").value;
        const option2 = document.getElementById("quizOption2").value;
        const option3 = document.getElementById("quizOption3").value;
        const option4 = document.getElementById("quizOption4").value;

        const options = [];
        if (option1) options.push(option1);
        if (option2) options.push(option2);
        if (option3) options.push(option3);
        if (option4) options.push(option4);

        document.getElementById("quizText").value = '';
        document.getElementById("quizOption1").value = '';
        document.getElementById("quizOption2").value = '';
        document.getElementById("quizOption3").value = '';
        document.getElementById("quizOption4").value = '';

        let storedToken = getAccessToken();
        if (quizText) {
            $.ajax({
                type: 'POST',
                url: '/api/submitquiz',
                data:{
                    text: quizText,
                    type: adminCurrSelectedQuizType,
                    sessionToken: storedToken.sessionToken,
                    session_id: sessionId,
                    options: options
                },
                success:function(data){
                    console.log('POST Quiz success!');
                },
                error:function(data){
                    addMessage('POST Quiz failed - ' + JSON.stringify(data));
                }
            });
        }
    }
    else { //repeated quiz
        let storedToken = getAccessToken();
        const currQuizId = keys[currAskingQuestionModalIndex];

        $.ajax({
            type: 'POST',
            url: '/api/requiz',
            data:{
                sessionToken: storedToken.sessionToken,
                session_id: sessionId,
                quiz_id: currQuizId
            },
            success:function(data){
                console.log('POST Quiz success!');
            },
            error:function(data){
                addMessage('POST Quiz failed - ' + JSON.stringify(data));
            }
        });
    }
    setModalVisibility('askQuestionModal', false);
}

function requestVoiceConnection(username) {
    var newConnection = {};
    newConnection.username = username;
    if (requireVoiceApproval) {
        newConnection.status = "Pending";
        newConnection.button = "<button onclick=\'acceptVoiceConnectionRequest(\"" + username + "\")\'>Accept User</button>";
    } else {
        newConnection.status = "Active";
        newConnection.button = "<button onclick=\'kickVoiceConnectionRequest(\"" + username + "\")\'>Kick User</button>";
        acceptVoiceConnectionRequest(username);
    }
    voiceConnections.push(newConnection);
    setVoiceModal();
}
function acceptVoiceConnectionRequest(username) {
    let storedToken = getAccessToken();
    const url = '/api/session/acceptVoiceConnectionRequest';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
            session_id: sessionId,
            sessionToken: storedToken.sessionToken,
            username: username
        },
        success:function(data){
            closeVoiceConnection(username);
            var newConnection = {};
            newConnection.username = username;
            newConnection.status = "Active";
            newConnection.button = "<button onclick=\'kickVoiceConnectionRequest(\"" + username + "\")\'>Kick User</button>";
            voiceConnections.push(newConnection);
            setVoiceModal();    
        }
    });

}
function kickVoiceConnectionRequest(username) {
    let storedToken = getAccessToken();
    const url = '/api/session/kickVoiceConnectionRequest';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
            session_id: sessionId,
            sessionToken: storedToken.sessionToken,
            username: username
        },
        success:function(data){
            closeVoiceConnection(username);
        }
    });

}
function acceptVoiceConnection() {
    if (document.getElementById('voiceInfoMessage').classList.contains("error-background")) {
        document.getElementById('voiceInfoMessage').classList.remove("error-background");
        document.getElementById('voiceInfoMessage').classList.add("info");
    }
    document.getElementById('voiceInfoMessage').innerHTML = "Connection Active.";
}
function kickVoiceConnection() {
    if (document.getElementById('voiceInfoMessage').classList.contains("info")) {
        document.getElementById('voiceInfoMessage').classList.add("error-background");
        document.getElementById('voiceInfoMessage').classList.remove("info");
    }
    document.getElementById('voiceInfoMessage').innerHTML = "Connection Closed by Speaker.";
    document.getElementById('sendVoiceConnectionRequestButton').innerHTML = "Voicechat with Speaker";
    document.getElementById('sendVoiceConnectionRequestButton').setAttribute("onClick", "sendVoiceConnectionRequest()");
    voiceConnecting=false;
}
function closeVoiceConnection(username) {
    voiceConnections = voiceConnections.filter(function(value, index, arr)
    {
        return (value.username != username);
    });
    setVoiceModal();
}
function createQuiz() {
    if (sessionType == "QAM") {
        let keys = [];
        for (let key in adminQuizData) {
            keys.push(key);
        }
        if (currAskingQuestionModalIndex == keys.length) {
            const quizText = document.getElementById("quizText").value;
            const option1 = document.getElementById("quizOption1").value;
            const option2 = document.getElementById("quizOption2").value;
            const option3 = document.getElementById("quizOption3").value;
            const option4 = document.getElementById("quizOption4").value;

            const options = [];
            if (option1) options.push(option1);
            if (option2) options.push(option2);
            if (option3) options.push(option3);
            if (option4) options.push(option4);

            document.getElementById("quizText").value = '';
            document.getElementById("quizOption1").value = '';
            document.getElementById("quizOption2").value = '';
            document.getElementById("quizOption3").value = '';
            document.getElementById("quizOption4").value = '';

            let storedToken = getAccessToken();
            if (quizText) {
                $.ajax({
                    type: 'POST',
                    url: '/api/createquiz',
                    data:{
                        text: quizText,
                        type: adminCurrSelectedQuizType,
                        sessionToken: storedToken.sessionToken,
                        session_id: sessionId,
                        options: options
                    },
                    success:function(data){
                        console.log('POST Quiz success!');
                        setModalVisibility('askQuestionModal', false);
                    },
                    error:function(data){
                        addMessage('POST Quiz failed - ' + JSON.stringify(data));
                    }
                });
            }
        }
    }
}

function submitQuestion() {
    const questionText = document.getElementById("questionField").value;
    if (!questionText) return;
    document.getElementById("questionField").value = '';
    let storedToken = getAccessToken();
    if (storedToken) {
        $.ajax({
            type: 'POST',
            url: '/api/questions',
            data:{
                question_text: questionText,
                sessionToken: storedToken.sessionToken,
                session_id: sessionId
            },
            success:function(data){
                console.log('POST Question success! - ' + JSON.stringify(data));
            },
            error:function(data){
                addMessage('POST Question failed - ' + JSON.stringify(data));
            }
        });
    }
    else {
        authenticate();
    }
    return false;
}
function promptUserRelogin() {
    setLoginBarVisible(true);
}
function setLoginBarVisible (isVisible) {
    setModalVisibility('login-modal', isVisible);
}
function updateQuizFormat(sessionType) {//todo something something something ui only
    if (isAdmin) {
        let mcqOptions = '';
        if (questionNumbers.length == 0) {
            for (let j=1; j<2; j++) {
                mcqOptions += '<div id="answerQuestionModal">'
                + '<div class="modal-dialog modal-dialog-centered modal-custom modal-custom-wide">'
                + '<div class="modal-content"><div class="modal-header">'
                + '<h4  class="modal-title"  id="quizResponseText">'
                + 'Question ' + j + ': <input type="text" id="quizQuestion' + j + '" class="form-control" name="askQuestion" placeholder="Question Text" required="required">'
                + '</h4>'
                + '</div><div class="modal-body"><form id="quizResponseForm' + j + '"><span id="quizResponseAnswerPortion">';
                if (sessionType == "BRANCHING_QUIZ") {
                    for (let i=0; i<4; i++) {
                        mcqOptions += '<label class="custom-select-multi">'
                            + '<input type="text" class="borderless" placeholder="Answer ' + (i+1) + '" id="quizAnswerText' + j + "|" + i + '">'
                            + ' -> <input type="text" class="borderless" size="3" id=\'quizNextOption' + j + "|" + i + '\' placeholder="next Q">'
                            + '</label><br>';
                    }
                } else if (sessionType == "GROUP_QUIZ") {
                    for (let i=0; i<4; i++) {
                        mcqOptions += '<label class="custom-select-multi">'
                            + '<input type="radio" name="mcqChoice" id=\'quizNextOption' + j + "|" + i + '\' value="' + i + '"';
                        if (i==0)
                            mcqOptions += 'checked';
                        mcqOptions += '> <input type="text" class="borderless" placeholder="Answer ' + (i+1) + '" id="quizAnswerText' + j + "|" + i + '">'
                            + '</label><br>';
                    }
                }
                mcqOptions += '</span><div class="form-group"><div id="quizErrorText' + j + '"class="error"></div><div id="quizSuccessText' + j + '"class="success"></div>'
                + '<button type="submit" class="btn btn-primary btn-block btn-lg" onClick="event.preventDefault(); createQuiz2(' + j + ')">Submit</button>'
                + '</div></form></div></div></div></div>';
            }
        } else {
            for (let j=0; j<questionNumbers.length; j++) {
                let questionExists=false;
                let id;
                for (let key in adminQuizData) {
                    if (adminQuizData[key].question_number == questionNumbers[j]) {
                        questionExists=true;
                        id=key;
                    }
                }
                if (!questionExists) {
                    mcqOptions += '<div id="answerQuestionModal">'
                    + '<div class="modal-dialog modal-dialog-centered modal-custom modal-custom-wide">'
                    + '<div class="modal-content"><div class="modal-header">'
                    + '<h4  class="modal-title"  id="quizResponseText">'
                    + 'Question ' + questionNumbers[j] + ': <input type="text" id="quizQuestion' + questionNumbers[j] + '" class="form-control" name="askQuestion" placeholder="Question Text" required="required">'
                    + '</h4>'
                    + '</div><div class="modal-body"><form id="quizResponseForm' + questionNumbers[j] + '"><span id="quizResponseAnswerPortion">';
                    if (sessionType == "BRANCHING_QUIZ") {
                        for (let i=0; i<4; i++) {
                            mcqOptions += '<label class="custom-select-multi">'
                                + '<input type="text" class="borderless" placeholder="Answer ' + (i+1) + '" id="quizAnswerText' + questionNumbers[j] + "|" + i + '">'
                                + ' -> <input type="text" class="borderless" size="3" id=\'quizNextOption' +questionNumbers[j] + "|" + i + '\' placeholder="next Q">'
                                + '</label><br>';
                        }
                    } else if (sessionType == "GROUP_QUIZ") {
                        for (let i=0; i<4; i++) {
                            mcqOptions += '<label class="custom-select-multi">'
                                + '<input type="radio" name="mcqChoice" id=\'quizNextOption' + questionNumbers[j] + "|" + i + '\' value="' + i + '"';
                            if (i==0)
                                mcqOptions += 'checked';
                            mcqOptions += '> <input type="text" class="borderless" placeholder="Answer ' + (i+1) + '" id="quizAnswerText' + questionNumbers[j] + "|" + i + '">'
                                + '</label><br>';
                        }
                    }
                    mcqOptions += '</span><div class="form-group"><div id="quizErrorText' + questionNumbers[j] + '"class="error"></div><div id="quizSuccessText' + questionNumbers[j] + '"class="success"></div>'
                    + '<button type="submit" class="btn btn-primary btn-block btn-lg" onClick="event.preventDefault(); createQuiz2(' + questionNumbers[j] + ')">Submit</button>'
                    + '</div></form></div></div></div></div>';
                } else {
                    let options= adminQuizData[id].leading;
                    mcqOptions += '<div id="answerQuestionModal">'
                    + '<div class="modal-dialog modal-dialog-centered modal-custom modal-custom-wide">'
                    + '<div class="modal-content"><div class="modal-header">'
                    + '<h4  class="modal-title"  id="quizResponseText">'
                    + 'Question ' + questionNumbers[j] + ': <input type="text" id="quizQuestion' + questionNumbers[j] + '" class="form-control" name="askQuestion" value="' + adminQuizData[id].text + '" placeholder="Question Text" required="required">'
                    + '</h4>'
                    + '</div><div class="modal-body"><form id="quizResponseForm' + questionNumbers[j] + '"><span id="quizResponseAnswerPortion">';
                    if (sessionType == "BRANCHING_QUIZ") {
                        for (let i=0; i<options.length; i++) {
                            mcqOptions += '<label class="custom-select-multi">'
                                + '<input type="text" class="borderless" value="' + options[i].text + '" placeholder="Answer ' + (i+1) + '" id="quizAnswerText' + questionNumbers[j] + "|" + i + '">'
                                + ' -> <input type="text" class="borderless" size="3" id=\'quizNextOption' +questionNumbers[j] + "|" + i + '\' value="' + options[i].lead + '" placeholder="next Q">'
                                + '</label><br>';
                        }
                        for (let i=options.length; i<4; i++) {
                            mcqOptions += '<label class="custom-select-multi">'
                                + '<input type="text" class="borderless" placeholder="Answer ' + (i+1) + '" id="quizAnswerText' + questionNumbers[j] + "|" + i + '">'
                                + ' -> <input type="text" class="borderless" size="3" id=\'quizNextOption' + questionNumbers[j] + "|" + i + '\' placeholder="next Q">'
                                + '</label><br>';
                        }
                    } else if (sessionType == "GROUP_QUIZ") {
                        for (let i=0; i<options.length; i++) {
                            mcqOptions += '<label class="custom-select-multi">'
                                + '<input type="radio" name="mcqChoice" id=\'quizNextOption' + questionNumbers[j] + "|" + i + '\' value="' + i + '"';
                            if (options[i].lead > 0)
                                mcqOptions += 'checked';
                            mcqOptions += '> <input type="text" class="borderless" value="' + options[i].text + '" placeholder="Answer ' + (i+1) + '" id="quizAnswerText' + questionNumbers[j] + "|" + i + '">'
                                + '</label><br>';
                        }
                        for (let i=options.length; i<4; i++) {
                            mcqOptions += '<label class="custom-select-multi">'
                                + '<input type="radio" name="mcqChoice" id=\'quizNextOption' + questionNumbers[j] + "|" + i + '\' value="' + i + '"';
                            mcqOptions += '> <input type="text" class="borderless" placeholder="Answer ' + (i+1) + '" id="quizAnswerText' + questionNumbers[j] + "|" + i + '">'
                                + '</label><br>';
                        }
                    }
                    mcqOptions += '</span><div class="form-group"><div id="quizErrorText' + questionNumbers[j] + '"class="error"></div><div id="quizSuccessText' + questionNumbers[j] + '"class="success"></div>'
                    + '<button type="submit" class="btn btn-primary btn-block btn-lg" onClick="event.preventDefault(); updateQuiz2(' + questionNumbers[j] + ')">Update</button>'
                    + '</div></form></div></div></div></div>';
                }
            }
        }
        document.getElementById("viewingOptionFilter").innerHTML = mcqOptions;
        document.getElementById("askQuestionButton").hidden = true;
        document.getElementById("container-question-cards").hidden = true;
        document.getElementById("postQuestionTypeSelect").hidden = true;
        document.getElementById("voiceOptionsButton").hidden = true;
    } else if (!isAdmin) {
        let mcqOptions = '';
        allowedAnswers = currQuiz.quizResponses;
        groupRound = currQuiz.userGrouping.group_started;
        groupNumber = currQuiz.userGrouping.user_group;
        mcqOptions += '<div id="answerQuestionModal">'
        + '<div class="modal-dialog modal-dialog-centered modal-custom modal-custom-wide">'
        + '<div class="modal-content"><div class="modal-header">';
        if (currQuiz.quizzes != '') {
            mcqOptions += '<h4  class="modal-title"  id="quizResponseText">';
            if (groupRound && sessionType == "GROUP_QUIZ") {
                mcqOptions += "Group Section: (Group " + groupNumber + ")<br>";
            }
            else if (sessionType == "GROUP_QUIZ") {
                mcqOptions += "Individual Section:<br>";
            }
            mcqOptions += 'Question ' + currQuiz.quizzes.question_number + ": " + currQuiz.quizzes.text
            + '</h4>'
            + '</div><div class="modal-body"><form id="quizResponseForm' + currQuiz.quizzes.question_number + '"><span id="quizResponseAnswerPortion">';
            for (let i=0; i<currQuiz.quizOptions.length; i++ ){
                mcqOptions += '<label class="custom-select-multi"';
                if (!allowedAnswers[i])
                    mcqOptions += ' disabled';
                mcqOptions += '><input type="radio" name="mcqChoice" id=\'quizNextOption' + currQuiz.quizzes.question_number + "|" + i + '\' value="' + currQuiz.quizOptions[i].option_num + '"';
                if (i==0)
                    mcqOptions += 'checked';
                if (!allowedAnswers[i])
                    mcqOptions += ' disabled';
                mcqOptions += '>' + currQuiz.quizOptions[i].text
                    + '</label><br>';
            }
            mcqOptions += '</span><div class="form-group"><div id="quizErrorText' + currQuiz.quizzes.question_number + '"class="error">' + errorMessage + '</div><div id="quizSuccessText' + currQuiz.quizzes.question_number + '"class="success"></div>'
            + '<button type="submit" class="btn btn-primary btn-block btn-lg" onClick="event.preventDefault(); submitQuiz2(' + currQuiz.quizzes.question_number + ')">Submit</button>'
            + '</div></form>';
        } else if (currQuiz.quizzes == '') {
            mcqOptions += '<h4  class="modal-title"  id="quizResponseText">';
            if (groupRound) {
                mcqOptions += "Group Section: (Group " + groupNumber + ")<br>"
                + "No questions remaining!<br>Thank you for taking this quiz."
                + '</h4>';
            } else if (sessionType == "GROUP_QUIZ") {
                mcqOptions += "Individual Section:<br>"
                + "No questions remaining!<br>Find your teammates from group " + groupNumber + " to begin answering as a group!<br>"
                + '<button type="submit" class="btn btn-primary btn-block btn-lg" onClick="event.preventDefault(); joinGroup()">'
                + 'Begin group questions (with your teammates!)'
                + '</button>'
                + '</h4>';
            } else {
                mcqOptions += "No questions remaining!<br>Thank you for taking this quiz."
                + '</h4>';
            }
        }
        mcqOptions += '</div></div></div></div>';
        document.getElementById("viewingOptionFilter").innerHTML = mcqOptions;
        document.getElementById("container-question-cards").hidden = true;

    }
}
function joinGroup() {
    let storedToken = getAccessToken();
    const url = '/api/quizzes/joingroup';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
            session_id: sessionId,
            sessionToken: storedToken.sessionToken
        },
        success:function(data){
            console.log('POST JoinGroup success! - ' + JSON.stringify(data));
            setBottomBar(sessionType);
        },
        error:function(data){
            addMessage('POST JoinGroup failed - ' + JSON.stringify(data));
        }
    });

}
function submitQuiz2(id) {
    document.getElementById("quizSuccessText" + id).innerHTML = "";
    let responseText;
    for (var i=0; i<currQuiz.quizOptions.length; i++) {
        optionId = 'quizNextOption' + id + '|' + i;
        if ((option = document.getElementById(optionId)).checked == true) {
            responseText = option.value;
        }
    }
    errorMessage = '';
    let next_quiz = currQuiz.quizOptions[responseText].leading_question;
    if (next_quiz < 0 && sessionType == "GROUP_QUIZ" && groupRound == false) {
        next_quiz = id+1;
    } else if (next_quiz < 0 && sessionType == "GROUP_QUIZ" && groupRound == true) {
        next_quiz = id;
        allowedAnswers[responseText] = false;
        errorMessage = "Wrong answer! Please try again!";
    } else if (next_quiz < 0 && sessionType == "BRANCHING_QUIZ") {
        next_quiz = id;
        allowedAnswers[responseText] = false;
        errorMessage = "Wrong answer! Please try again!";
    }

    if (!groupRound) {
        let storedToken = getAccessToken();
        const url = '/api/quizzes/response';
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                quiz_id: currQuiz.quizzes.id,
                session_id: sessionId,
                quiz_response: responseText,
                sessionToken: storedToken.sessionToken
            },
            success:function(data){
                console.log('POST QuizResponse success! - ' + JSON.stringify(data));
                //document.getElementById("quizSuccessText" + id).innerHTML = "Success! Response Sent!";
                const url2 = '/api/sessions/updateUserSession';
                $.ajax({
                    type: 'POST',
                    url: url2,
                    data:{
                        session_id: sessionId,
                        next_quiz: next_quiz,
                        sessionToken: storedToken.sessionToken
                    },
                    success:function(data){
                        console.log('POST QuizResponse success! - ' + JSON.stringify(data));
                        const url3 = '/api/quizzes/getSpecificQuiz'
                        $.ajax({
                            type: 'POST',
                            url: url3,
                            data:{
                                session_id: sessionId,
                                sessionToken: storedToken.sessionToken
                            },
                            success:function(data){
                                console.log('POST QuizResponse success! - ' + JSON.stringify(data));
                                currQuiz = data;
                                updateQuizFormat(sessionType);
                            },
                            error:function(data){
                                addMessage('POST QuizResponse failed - ' + JSON.stringify(data));
                            }
                        });
                    },
                    error:function(data){
                        addMessage('POST QuizResponse failed - ' + JSON.stringify(data));
                    }
                });     
            },
            error:function(data){
                addMessage('POST QuizResponse failed - ' + JSON.stringify(data));
            }
        });
    } else if (groupRound) {
        let storedToken = getAccessToken();
        const url = '/api/sessions/updateGroupSession';
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                session_id: sessionId,
                next_quiz: next_quiz,
                group_number: groupNumber,
                sessionToken: storedToken.sessionToken
            },
            success:function(data){
                console.log('POST QuizSession Update success! - ' + JSON.stringify(data));
                //document.getElementById("quizSuccessText" + id).innerHTML = "Success! Response Sent!";
                const url2 = '/api/quizzes/groupResponse';
                $.ajax({
                    type: 'POST',
                    url: url2,
                    data:{
                        quiz_id: currQuiz.quizzes.id,
                        session_id: sessionId,
                        quiz_response: responseText,
                        group_number: groupNumber,
                        sessionToken: storedToken.sessionToken,
                        error_message: errorMessage
                    },
                    success:function(data){
                        console.log('POST QuizResponse success! - ' + JSON.stringify(data));
                        const url3 = '/api/quizzes/getSpecificQuiz'
                        $.ajax({
                            type: 'POST',
                            url: url3,
                            data:{
                                session_id: sessionId,
                                sessionToken: storedToken.sessionToken
                            },
                            success:function(data){
                                console.log('POST QuizResponse success! - ' + JSON.stringify(data));
                                currQuiz = data;
                                updateQuizFormat(sessionType);
                            },
                            error:function(data){
                                addMessage('POST QuizResponse failed - ' + JSON.stringify(data));
                            }
                        });
                    },
                    error:function(data){
                        addMessage('POST QuizResponse failed - ' + JSON.stringify(data));
                    }
                });     
            },
            error:function(data){
                addMessage('POST QuizResponse failed - ' + JSON.stringify(data));
            }
        });
    }
}
function createQuiz2(id) {
    document.getElementById("quizErrorText" + id).innerHTML = "";
    document.getElementById("quizSuccessText" + id).innerHTML = "";
    const quizText = document.getElementById("quizQuestion" + id).value;
    
    if (!quizText) {
        document.getElementById("quizErrorText" + id).innerHTML = "Error! Please enter a question!";
        return;
    }

    let next1;
    let next2;
    let next3;
    let next4;

    if (sessionType == "BRANCHING_QUIZ") {
        next1 = document.getElementById("quizNextOption" + id + "|0").value;
        next2 = document.getElementById("quizNextOption" + id + "|1").value;
        next3 = document.getElementById("quizNextOption" + id + "|2").value;
        next4 = document.getElementById("quizNextOption" + id + "|3").value;
    } else if (sessionType == "GROUP_QUIZ") {
        next1 = document.getElementById("quizNextOption" + id + "|0").checked;
        next2 = document.getElementById("quizNextOption" + id + "|1").checked;
        next3 = document.getElementById("quizNextOption" + id + "|2").checked;
        next4 = document.getElementById("quizNextOption" + id + "|3").checked;
    }

    const answer1 = document.getElementById("quizAnswerText" + id + "|0").value;
    const answer2 = document.getElementById("quizAnswerText" + id + "|1").value;
    const answer3 = document.getElementById("quizAnswerText" + id + "|2").value;
    const answer4 = document.getElementById("quizAnswerText" + id + "|3").value;

    const answers = [];

    let nexts = [];
    if (sessionType == "BRANCHING_QUIZ") {
        if (answer1 || next1)
            if (next1 && answer1) {nexts.push(next1); answers.push(answer1);}
            else {document.getElementById("quizErrorText" + id).innerHTML = "Error! Answers must be paired with a followup question number!"; return;}
        if (answer2 || next2)
            if (next2 && answer2) {nexts.push(next2); answers.push(answer2);}
            else {document.getElementById("quizErrorText" + id).innerHTML = "Error! Answers must be paired with a followup question number!"; return;}
        if (answer3 || next3)
            if (next3 && answer3) {nexts.push(next3); answers.push(answer3);}
            else {document.getElementById("quizErrorText" + id).innerHTML = "Error! Answers must be paired with a followup question number!"; return;}
        if (answer4 || next4)
            if (next4 && answer4) {nexts.push(next4); answers.push(answer4);}
            else {document.getElementById("quizErrorText" + id).innerHTML = "Error! Answers must be paired with a followup question number!"; return;}
        for (let i=0; i<nexts.length; i++) {
            if (isNaN(nexts[i])) {document.getElementById("quizErrorText" + id).innerHTML = "Error! Question numbers must be numbers!"; return;}
        }
    } else if (sessionType == "GROUP_QUIZ") {
        if (answer1) {nexts.push(next1); answers.push(answer1);}
        if (answer2) {nexts.push(next2); answers.push(answer2);}
        if (answer3) {nexts.push(next3); answers.push(answer3);}
        if (answer4) {nexts.push(next4); answers.push(answer4);}
        let checkedAnswer = false;
        for (let i=0; i<nexts.length; i++) {
            if (nexts[i] == true) {
                nexts[i] = (id+1);
                checkedAnswer = true;
            }
            else
                nexts[i] = -1;
        }
        if (!checkedAnswer) {
            document.getElementById("quizErrorText" + id).innerHTML = "Error! No valid correct answer selected!"; return;
        }
    }
    let storedToken = getAccessToken();
    if (quizText) {
        $.ajax({
            type: 'POST',
            url: '/api/createquiz',
            data:{
                text: quizText,
                type: 'MCQ',
                sessionToken: storedToken.sessionToken,
                session_id: sessionId,
                options: answers,
                leading_to: nexts,
                question_number: id
            },
            success:function(data){
                console.log('POST Quiz success!');
                document.getElementById("quizSuccessText" + id).innerHTML = "Success! Question created!";
            },
            error:function(data){
                addMessage('POST Quiz failed - ' + JSON.stringify(data));
            }
        });
    }
}
function updateGroupResponses(data) {
    if (groupNumber == data.groupNumber) {
        console.log("same group!");
        errorMessage = data.errorMessage;
        setBottomBar();
    }
}
function updateQuiz2(id) {
    document.getElementById("quizErrorText" + id).innerHTML = "";
    document.getElementById("quizSuccessText" + id).innerHTML = "";
    const quizText = document.getElementById("quizQuestion" + id).value;
    
    if (!quizText) {
        document.getElementById("quizErrorText" + id).innerHTML = "Error! Please enter a question!";
        return;
    }
    
    let quiz_id=0;
    for (let key in adminQuizData) {
        if (adminQuizData[key].question_number == id) {
            quiz_id=key;
        }
    }

    let next1;
    let next2;
    let next3;
    let next4;

    if (sessionType == "BRANCHING_QUIZ") {
        next1 = document.getElementById("quizNextOption" + id + "|0").value;
        next2 = document.getElementById("quizNextOption" + id + "|1").value;
        next3 = document.getElementById("quizNextOption" + id + "|2").value;
        next4 = document.getElementById("quizNextOption" + id + "|3").value;
    } else if (sessionType == "GROUP_QUIZ") {
        next1 = document.getElementById("quizNextOption" + id + "|0").checked;
        next2 = document.getElementById("quizNextOption" + id + "|1").checked;
        next3 = document.getElementById("quizNextOption" + id + "|2").checked;
        next4 = document.getElementById("quizNextOption" + id + "|3").checked;
    }

    const answer1 = document.getElementById("quizAnswerText" + id + "|0").value;
    const answer2 = document.getElementById("quizAnswerText" + id + "|1").value;
    const answer3 = document.getElementById("quizAnswerText" + id + "|2").value;
    const answer4 = document.getElementById("quizAnswerText" + id + "|3").value;

    const answers = [];

    let nexts = [];
    if (sessionType == "BRANCHING_QUIZ") {
        if (answer1 || next1)
            if (next1 && answer1) {nexts.push(next1); answers.push(answer1);}
            else {document.getElementById("quizErrorText" + id).innerHTML = "Error! Answers must be paired with a followup question number!"; return;}
        if (answer2 || next2)
            if (next2 && answer2) {nexts.push(next2); answers.push(answer2);}
            else {document.getElementById("quizErrorText" + id).innerHTML = "Error! Answers must be paired with a followup question number!"; return;}
        if (answer3 || next3)
            if (next3 && answer3) {nexts.push(next3); answers.push(answer3);}
            else {document.getElementById("quizErrorText" + id).innerHTML = "Error! Answers must be paired with a followup question number!"; return;}
        if (answer4 || next4)
            if (next4 && answer4) {nexts.push(next4); answers.push(answer4);}
            else {document.getElementById("quizErrorText" + id).innerHTML = "Error! Answers must be paired with a followup question number!"; return;}
        for (let i=0; i<nexts.length; i++) {
            if (isNaN(nexts[i])) {document.getElementById("quizErrorText" + id).innerHTML = "Error! Question numbers must be numbers!"; return;}
        }
    } else if (sessionType == "GROUP_QUIZ") {
        if (answer1) {nexts.push(next1); answers.push(answer1);}
        if (answer2) {nexts.push(next2); answers.push(answer2);}
        if (answer3) {nexts.push(next3); answers.push(answer3);}
        if (answer4) {nexts.push(next4); answers.push(answer4);}
        for (let i=0; i<nexts.length; i++) {
            if (nexts[i] == true)
                nexts[i] = (id+1);
            else
                nexts[i] = -1;
        }
    }
    let storedToken = getAccessToken();
    if (quizText) {
        $.ajax({
            type: 'POST',
            url: '/api/updatequiz',
            data:{
                text: quizText,
                type: 'MCQ',
                sessionToken: storedToken.sessionToken,
                session_id: sessionId,
                options: answers,
                leading_to: nexts,
                question_number: id,
                quiz_id: quiz_id
            },
            success:function(data){
                console.log('POST Quiz success!');
                document.getElementById("quizSuccessText" + id).innerHTML = "Success! Question updated!";
            },
            error:function(data){
                addMessage('POST Quiz failed - ' + JSON.stringify(data));
            }
        });
    }
}
function setSessionBar(session, message) {
    let sessionTitle = '';
    let sessionDesc = '';
    if (session) {
        sessionTitle = session.session_name;
        sessionDesc = session.session_description;
        sessionType = session.session_type;
    } else {
        sessionTitle = message;
    }
    document.getElementById('session-title').innerHTML = sessionTitle;
    document.getElementById('session-description').innerHTML = sessionDesc;
}
function setVoiceModal(session) {
    if (session) {
        allowVoicechat = session.voicechat_enabled;
        requireVoiceApproval = session.require_voice_approval;
    }
    document.getElementById('allowVoicechatCheckbox').checked = allowVoicechat;
    document.getElementById('requireApprovalCheckbox').checked = requireVoiceApproval;
    $("#voiceConnectionTable tbody tr").remove();
    for (i in voiceConnections) {
        $('#voiceConnectionTable').append('<tr><td>' + voiceConnections[i].username + '</td><td>' + voiceConnections[i].status + '</td><td>' + voiceConnections[i].button + '</td></tr>');
    }
    if (allowVoicechat && !isAdmin) {
        document.getElementById('form-question').classList.remove('w-100');
        document.getElementById('form-question').classList.add('w-75');
        document.getElementById('voiceChatButton').hidden = false;
    } else if (!allowVoicechat && !isAdmin){
        document.getElementById('form-question').classList.remove('w-75');
        document.getElementById('form-question').classList.add('w-100');
        document.getElementById('voiceChatButton').hidden = true;
    }
}
function closeVoiceConnectionRequest () {
    if (document.getElementById('voiceInfoMessage').classList.contains("error-background")) {
        document.getElementById('voiceInfoMessage').classList.remove("error-background");
        document.getElementById('voiceInfoMessage').classList.add("info");
    }
    if (voiceConnecting == true) {
        let storedToken = getAccessToken();
        const url = '/api/session/closeVoiceConnectionRequest';
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                session_id: sessionId,
                sessionToken: storedToken.sessionToken
            },
            success:function(data){
                document.getElementById('voiceInfoMessage').innerHTML = "Connection Closed.";
                document.getElementById('sendVoiceConnectionRequestButton').innerHTML = "Voicechat with Speaker";
                document.getElementById('sendVoiceConnectionRequestButton').setAttribute("onClick", "sendVoiceConnectionRequest()");
                voiceConnecting = false;
            },
            error:function(data){
                if (document.getElementById('voiceInfoMessage').classList.contains("info")) {
                    document.getElementById('voiceInfoMessage').classList.add("error-background");
                    document.getElementById('voiceInfoMessage').classList.remove("info");
                }
                document.getElementById('voiceInfoMessage').innerHTML = "Unable to close.";
            }
        });
    } else {
        document.getElementById('voiceInfoMessage').innerHTML = "Connection Closed.";
        document.getElementById('sendVoiceConnectionRequestButton').innerHTML = "Voicechat with Speaker";
        document.getElementById('sendVoiceConnectionRequestButton').setAttribute("onClick", "sendVoiceConnectionRequest()");
    }
}
function sendVoiceConnectionRequest () {
    if (document.getElementById('voiceInfoMessage').classList.contains("error-background")) {
        document.getElementById('voiceInfoMessage').classList.remove("error-background");
        document.getElementById('voiceInfoMessage').classList.add("info");
    }
    if (voiceConnecting == false) {
        let storedToken = getAccessToken();
        const url = '/api/session/sendVoiceConnectionRequest';
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                session_id: sessionId,
                sessionToken: storedToken.sessionToken
            },
            success:function(data){
                document.getElementById('voiceInfoMessage').innerHTML = "Connecting...";
                document.getElementById('sendVoiceConnectionRequestButton').innerHTML = "Stop Voicechat";
                document.getElementById('sendVoiceConnectionRequestButton').setAttribute("onClick", "closeVoiceConnectionRequest()");
                voiceConnecting = true;
            },
            error:function(data){
                if (document.getElementById('voiceInfoMessage').classList.contains("info")) {
                    document.getElementById('voiceInfoMessage').classList.add("error-background");
                    document.getElementById('voiceInfoMessage').classList.remove("info");
                }
                document.getElementById('voiceInfoMessage').innerHTML = "Connection rejected.";
            }
        });
    } else {
        document.getElementById('voiceInfoMessage').innerHTML = "Connection Closed.";
        document.getElementById('sendVoiceConnectionRequestButton').innerHTML = "Voicechat with Speaker";
        voiceConnecting = false;
    }
}
function updateUsers(users) {
    let regCount = 0;
    let anonCount = 0;
    let regUsersStr = '<b>Registered Users</b> <br /> ';
    let anonUsersStr = '<b>Anonymous Users</b> <br />';
    for (let key in users) {
        if (users.hasOwnProperty(key)) {
            // console.log('User = ' + JSON.stringify(users[key]));
            if (users[key].isAnonymous) {
                anonUsersStr += '<li>' + users[key].username + ' (' + users[key].user_group + ')</li>';
                anonCount++;
            } else {
                regUsersStr += '<li>' + users[key].username;
                if (!users[key].is_verified) {
                    regUsersStr += ' (Unverified)';
                }
                regUsersStr += ' (' + users[key].user_group + ')</li>';
                regCount++;
            }
        }
    }
    if (regCount === 0) {
        regUsersStr += "-";
    }
    if (anonCount === 0) {
        anonUsersStr += "-";
    }

    document.getElementById('regUsersList').innerHTML = regUsersStr;
    document.getElementById('anonUsersList').innerHTML = anonUsersStr;

    document.getElementById('usersButton').innerHTML = 'Users (' + (regCount + anonCount) + ')';
}
function compareQuestion (a,b) {
    if (a.voteCount > b.voteCount)
        return -1;
    if (a.voteCount < b.voteCount)
        return 1;
    return 0;
}
function submitQuizResponse(quizType) {
    let responseText;
    if (quizType === 'MCQ') {
        for (var i=0; i<currQuiz.options.length; i++) {
            optionId = 'quizResponseOption' + i;
            if ((option = document.getElementById(optionId)).checked == true) {
                responseText = option.value;
            }
        }
    } else if (quizType === 'MULTI_MCQ') {
        var result = [];
        var option;
        var optionId;
        for (var i=0; i<currQuiz.options.length; i++) {
            optionId = 'quizResponseOption' + i;
            if ((option = document.getElementById(optionId)).checked == true) {
                result.push(option.value);
            }
        }
        responseText = result;
        
    } else {
        const openResponseValue = document.getElementById('quizOpenResponseField').value;
        responseText = openResponseValue;
    }

    if (quizType != 'MULTI_MCQ') {
        let storedToken = getAccessToken();
        const url = '/api/quizzes/response';
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                quiz_id: currQuiz.id,
                session_id: sessionId,
                quiz_response: responseText,
                sessionToken: storedToken.sessionToken
            },
            success:function(data){
                console.log('POST QuizResponse success! - ' + JSON.stringify(data));
                setModalVisibility('answerQuestionModal', false);
            },
            error:function(data){
                addMessage('POST QuizResponse failed - ' + JSON.stringify(data));
                setModalVisibility('answerQuestionModal', false);
            }
        });
    } else {
        for (var i=0; i<responseText.length; i++) {
            let storedToken = getAccessToken();
            const url = '/api/quizzes/response';
            $.ajax({
                type: 'POST',
                url: url,
                data:{
                    quiz_id: currQuiz.id,
                    session_id: sessionId,
                    quiz_response: responseText[i],
                    sessionToken: storedToken.sessionToken
                },
                success:function(data){
                    console.log('POST QuizResponse success! - ' + JSON.stringify(data));
                    setModalVisibility('answerQuestionModal', false);
                },
                error:function(data){
                    addMessage('POST QuizResponse failed - ' + JSON.stringify(data));
                    setModalVisibility('answerQuestionModal', false);
                }
            });
        }
    }
}
function inviteUsersByEmail() {
    setElementVisibility('inviteWarning', true);
    const emailsGroupsStr = document.getElementById("inviteUsersByEmailText").value;
    if (!emailsGroupsStr) return;
    document.getElementById("inviteUsersByEmailText").value = '';

    let storedToken = getAccessToken();
    if (storedToken) {
        $.ajax({
            type: 'POST',
            url: '/api/users/invite-by-emails',
            data:{
                emailsGroups: emailsGroupsStr,
                sessionToken: storedToken.sessionToken,
                session_id: sessionId
            },
            success:function(data){
                let str = '<b>Invited Emails</b><br>';
                for (let i = 0; i < data.createdAccountEmails.length; i++) {
                    str += ' ' + data.createdAccountEmails[i];
                }
                if (data.createdAccountEmails.length === 0) {
                    str += '-';
                }
                str += '<br><b>Invalid Emails</b><br>';
                for (let i = 0; i < data.invalidEmails.length; i++) {
                    str += ' ' + data.invalidEmails[i];
                }
                if (data.invalidEmails.length === 0) {
                    str += '-';
                }
                str += '<br><b>Already Existing Emails</b><br>';
                for (let i = 0; i < data.alreadyExistsEmails.length; i++) {
                    str += ' ' + data.alreadyExistsEmails[i];
                }
                if (data.alreadyExistsEmails.length === 0) {
                    str += '-';
                }

                setText('inviteWarningMessage', str);
            },
            error:function(data){
                addMessage('POST Question failed - ' + JSON.stringify(data));
            }
        });
    }
}
function readFile(file, onLoadCallback) {
    var reader = new FileReader();
    reader.onload = onLoadCallback;
    reader.readAsText(file);
}
function inviteUsersByCSV() {
    setElementVisibility('inviteWarning', true);
    let emailsGroupsFile = document.getElementById("inviteUsersByCSV").files;
    emailsGroupsFile = emailsGroupsFile[0];
    
    readFile(emailsGroupsFile, function(e) {
        let emailsGroupsStr = e.target.result;
        emailsGroupsStr = emailsGroupsStr.replace(/\r?\n|\r/g, ",");

        if (!emailsGroupsFile) return;
        document.getElementById("inviteUsersByCSV").value = '';
    
        let storedToken = getAccessToken();
        if (storedToken) {
            $.ajax({
                type: 'POST',
                url: '/api/users/invite-by-emails',
                data:{
                    emailsGroups: emailsGroupsStr,
                    sessionToken: storedToken.sessionToken,
                    session_id: sessionId
                },
                success:function(data){
                    let str = '<b>Invited Emails</b><br>';
                    for (let i = 0; i < data.createdAccountEmails.length; i++) {
                        str += ' ' + data.createdAccountEmails[i];
                    }
                    if (data.createdAccountEmails.length === 0) {
                        str += '-';
                    }
                    str += '<br><b>Invalid Emails</b><br>';
                    for (let i = 0; i < data.invalidEmails.length; i++) {
                        str += ' ' + data.invalidEmails[i];
                    }
                    if (data.invalidEmails.length === 0) {
                        str += '-';
                    }
                    str += '<br><b>Already Existing Emails</b><br>';
                    for (let i = 0; i < data.alreadyExistsEmails.length; i++) {
                        str += ' ' + data.alreadyExistsEmails[i];
                    }
                    if (data.alreadyExistsEmails.length === 0) {
                        str += '-';
                    }
    
                    setText('inviteWarningMessage', str);
                },
                error:function(data){
                    addMessage('POST Question failed - ' + JSON.stringify(data));
                }
            });
        }    
    });
}

function changeSessionDates(start, end) {
    start = JSON.stringify(start);
    end = JSON.stringify(end);

    let storedToken = getAccessToken();
    const url = '/api/sessions/change-dates';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
            session_id: sessionId,
            start: start,
            end: end,
            sessionToken: storedToken.sessionToken
        },
        success:function(data){
            console.log('changeSessionDates success! - ' + JSON.stringify(data));
            setModalVisibility('answerQuestionModal', false);
        },
        error:function(data){
            addMessage('changeSessionDates failed - ' + JSON.stringify(data));
            setModalVisibility('answerQuestionModal', false);
        }
    });
}

function promptQuiz (quiz) {
    if (isAdmin) { return; }
    currQuiz = quiz;
    const type = quiz.type;
    let responseHtml = '';

    //Build MCQ Options
    if (type === 'MCQ') {
        const options = quiz.options;
        for (let i = 0; i < options.length; i++) {
            responseHtml += '<label class="custom-select-multi"><input type="radio" name="mcqChoice" id=\'quizResponseOption' + i + '\' value="' + i + '"> '
                + options[i]
                + '</label><br>';
        }
        responseHtml += '</select>';
    } else if (type === 'MULTI_MCQ') {
        const options = quiz.options;
        for (let i = 0; i < options.length; i++) {
            responseHtml += '<label class="custom-select-multi"><input type="checkbox" id=\'quizResponseOption' + i + '\' value="' + i + '"> '
                + options[i]
                + '</label><br>';
        }
    } else if (type === 'OPEN_ENDED') {
        //Build OpenEnded field
        responseHtml += '<input type="text" class="col-12 col-md-12 form-control bold-border" placeholder="Answer Question" id="quizOpenResponseField">';
    }

    document.getElementById("quizResponseAnswerPortion").innerHTML = responseHtml;

    const quizText = quiz.text;
    document.getElementById("quizResponseText").innerHTML = quizText;
    setModalVisibility('answerQuestionModal', true);
}
function updateAdminQuizzes (quizzesData) {
    if (!isAdmin) {
        return;
    }
    const quizzes = quizzesData.quizzes;
    const quizOptions = quizzesData.quizOptions;
    const quizLeading = quizzesData.quizLeading;
    adminQuizData = {};
    questionNumbers = [];

    for (let i = 0; i < quizzes.length; i++) {
        let q = quizzes[i];
        let id = q.id;
        //Only add quiz into mapping if it doesn't already exist
        if (!adminQuizData[id]) {
            adminQuizData[id] = {};
            adminQuizData[id].text = q.text;
            adminQuizData[id].type = q.type;
            adminQuizData[id].question_number = q.question_number;
            adminQuizData[id].responses = [];

            //If quiz is MCQ
            if ((q.type === 'MCQ' || q.type === 'MULTI_MCQ') && quizOptions[id]) {
                adminQuizData[id].options = quizOptions[id];
                adminQuizData[id].leading = quizLeading[id];
            }
        }
    }
    //Force view last index (latest)
    currViewingQuizResponseIndex = quizzes.length - 1;
    currAskingQuestionModalIndex = quizzes.length;
    updateViewQuizModal();
    updateAskQuizModal();
    if (sessionType != 'QAM') {
        for (let i = 0; i < quizzes.length; i++) {
            let q = quizzes[i];
            let id = q.id;
            let options = quizLeading[id];
            if (!questionNumbers.includes(adminQuizData[id].question_number))
                questionNumbers.push(adminQuizData[id].question_number);
            for (let i = 0; i < options.length; i++) {
                if (!questionNumbers.includes(options[i].lead) && options[i].lead > 0)
                    questionNumbers.push(options[i].lead);
            }
            questionNumbers.sort(function (a, b) {  return a - b;  });
        }
        updateQuizFormat(sessionType);
    }
}
function updateAdminGroupDetails (responses) {
    quizGroupResponses = responses.quizGroupResponses;
    groupIds = [];
    groupIds.push(-1);
    for (let i=0; i<quizGroupResponses.length; i++) {
        if (!groupIds.includes(quizGroupResponses[i].user_group))
            groupIds.push(quizGroupResponses[i].user_group);
    }
    groupIds = groupIds.sort();
}
function updateAdminQuizResponse (responses) {
    if (!isAdmin) {
        return;
    }
    //First group them by their quiz_id
    const responseMapping = {};
    for (let i = 0; i < responses.length; i++) {
        let r = responses[i];
        let quizId = r.quiz_id;
        if (!responseMapping[quizId]) {
            responseMapping[quizId] = [];
        }
        let value = {
          text: r.text,
          count: r.Count
        };
        responseMapping[quizId].push(value);
    }

    for (let quizId in responseMapping) {
        if (!adminQuizData[quizId]) {
            continue;
        }
        adminQuizData[quizId].responses = responseMapping[quizId];
    }
    updateViewQuizModal();
    updateAskQuizModal();
}
function updateViewQuizModal() {
    //set question number
    document.getElementById('viewQuizResponseText').innerHTML = 'Q' + (currViewingQuizResponseIndex + 1);

    //set group number
    if (currViewingQuizGroupIndex == 0)
        document.getElementById('viewQuizGroupText').innerHTML = 'All Users';
    else
        document.getElementById('viewQuizGroupText').innerHTML = 'Group ' + (groupIds[currViewingQuizGroupIndex]);

    //TODO: Refactor this data-formatting
    let keys = [];
    for (let key in adminQuizData) {
        keys.push(key);
    }
    //get quizid and quiz data
    const currQuizId = keys[currViewingQuizResponseIndex];
    const currQuiz = adminQuizData[currQuizId];
    //if no quiz by the id, return
    if (!currQuiz) {
        return;
    }
    
    document.getElementById('quizResponseTitle').innerHTML = currQuiz.text;
    let listStr = '';
    let responses = [];
    if (currViewingQuizGroupIndex == 0)
        responses = currQuiz.responses;
    else {
        for (let i=0; i<quizGroupResponses.length; i++) {
            if (currQuizId == quizGroupResponses[i].quiz_id && groupIds[currViewingQuizGroupIndex] == quizGroupResponses[i].user_group) {
                responses.push({text: quizGroupResponses[i].text, count: quizGroupResponses[i].Count});
            }
        }
    }
    let options;
    let countArray;
    if (currQuiz.type === 'MCQ' || currQuiz.type === 'MULTI_MCQ') {
        options = currQuiz.options;
        countArray = Array.from('0'.repeat(options.length));
        for (let i = 0; i < responses.length; i++) {
            let index = parseInt(responses[i].text);
            countArray[index] = responses[i].count;
        }

        /*for (let j = 0; j < options.length; j++) {
            let optionText = options[j];
            listStr += '<li class="list-group-item">';
            listStr += optionText + ' (' + countArray[j] + ')';
            listStr += '</li>';
        }*/
        
        listStr += '<canvas id="responseChart" width="400" height="200"></canvas>';
    } else {
        for (let i = 0; i < responses.length; i++) {
            listStr += '<li class="list-group-item">';
            listStr += htmlEscape(responses[i].text);
            if (responses[i].count > 1) {
                listStr += ' (' + responses[i].count + ')';
            }
            listStr += '</li>';
        }
        if (responses.length <= 0) {
            listStr += '<li class="list-group-item">No replies... yet!</li>';
        }
    }
    document.getElementById('quizResponseList').innerHTML = listStr;
    if (currQuiz.type === 'MCQ' || currQuiz.type === 'MULTI_MCQ') {
        var ctx = document.getElementById("responseChart");
        if (options.length == 2) {
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [options[0], options[1]],
                    datasets: [{
                        label: '# of Votes',
                        data: [countArray[0], countArray[1]],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        } else if (options.length == 3) {
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [options[0], options[1], options[2]],
                    datasets: [{
                        label: '# of Votes',
                        data: [countArray[0], countArray[1], countArray[2]],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        } else {
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [options[0], options[1], options[2], options[3]],
                    datasets: [{
                        label: '# of Votes',
                        data: [countArray[0], countArray[1], countArray[2], countArray[3]],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        }
    }
}
function updateAskQuizModal() {
    document.getElementById('viewQuizAskText').innerHTML = 'Q' + (currAskingQuestionModalIndex + 1);
    //TODO: Refactor this data-formatting
    let keys = [];
    for (let key in adminQuizData) {
        keys.push(key);
    }

    if (currAskingQuestionModalIndex == keys.length) {
        document.getElementById('viewQuizAskText').innerHTML = 'New Question';
        document.getElementById('quizAskTitle').hidden = true;
        document.getElementById('quizAskList').hidden = true;
        document.getElementById('create-quiz').hidden = false;
        document.getElementById("postQuestionTypeSelect").disabled = false;
        document.getElementById("postQuestionTypeSelect").value = 0;
        document.getElementById("quizText").required = true;
        document.getElementById("createQuiz").hidden = false;
        document.getElementById("submitQuiz").textContent = "Create & Post Question";
        return;
    }
    
    const currQuizId = keys[currAskingQuestionModalIndex];
    const currQuiz = adminQuizData[currQuizId];
    
    if (!currQuiz) {
        return;
    }
    
    document.getElementById('quizAskTitle').hidden = false;
    document.getElementById('quizAskList').hidden = false;
    document.getElementById('create-quiz').hidden = true;
    document.getElementById("postQuestionTypeSelect").disabled = true;
    document.getElementById("quizText").required = false;
    document.getElementById("createQuiz").hidden = true;
    document.getElementById("submitQuiz").innerHTML = "Post Question";
    switch (currQuiz.type) {
        case 'MCQ':document.getElementById("postQuestionTypeSelect").value=0; break;
        case 'MULTI_MCQ':document.getElementById("postQuestionTypeSelect").value=1; break;
        default:document.getElementById("postQuestionTypeSelect").value=2;
    }

    document.getElementById('quizAskTitle').innerHTML = currQuiz.text;
    let listStr = '';
    const responses = currQuiz.responses;
    if (currQuiz.type === 'MCQ' || currQuiz.type === 'MULTI_MCQ') {
        let options = currQuiz.options;

        for (let j = 0; j < options.length; j++) {
            let optionText = options[j];
            listStr += '<li class="list-group-item">';
            listStr += optionText;
            listStr += '</li>';
        }
    } else {
        listStr += '<li class="list-group-item">   </li>';
    }
    document.getElementById('quizAskList').innerHTML = listStr;

}
function updateQuestions (questions) {
    allQuestions = questions;
    showCurrViewingQuestions();
}
function showCurrViewingQuestions() {
    let questions = allQuestions;
    let questionCount = 0;
    let str = '';

    for (let i = 0; i < questions.length; i++) {
        const q = questions[i];
        questions[i].voteCount = q.Votes.length;
    }

    questions.sort(compareQuestion);

    for (let i = 0; i < questions.length; i++) {
        const q = questions[i];
        if (q.state !== viewingOptions[currentViewingIndex]) {
            continue;
        }
        const votes = q.Votes;
        let hasUpvoted = false;
        for (let j = 0; j < votes.length; j++) {
            if (votes[j].user_id === coreUserId) {
                hasUpvoted = true;
            }
        }
        canUpvote = !hasUpvoted;
        let questionCard = generateQuestionCard(q.question_text, q.User.username, q.voteCount, q.id, canUpvote);
        str += questionCard;
        questionCount++;
    }
    if (questionCount === 0) {
        const text = 'No questions have been asked... yet!';
        str += generateNoQuestionsCard(text);
    }
    document.getElementById('container-question-cards').innerHTML = str;
}
function selectQuizType() {
    const index = document.getElementById('postQuestionTypeSelect').value;
    adminCurrSelectedQuizType = quizTypes[index];
    updateAdminAskQuizModal();
}
function onAllowUninvitedCheckboxChange(){
    const isChecked = document.getElementById('allowUninvitedCheckbox').checked;
    allowUninvitedUsers = isChecked;

    let storedToken = getAccessToken();
    const sessionToken = storedToken.sessionToken;
    const url = '/api/sessions/set-invite-only';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
            sessionToken: sessionToken,
            session_id: sessionId,
            invite_only: (!allowUninvitedUsers)
        },
        success:function(data){
            console.log('Change invite_only success - ' + JSON.stringify(data));
        },
        error:function(data){
            console.log('Change invite_only failed - ' + JSON.stringify(data));
        }
    });
}
function onAllowUnverifiedCheckboxChange(){
    const isChecked = document.getElementById('allowUnverifiedCheckbox').checked;
    allowUnverifiedUsers = isChecked;

    let storedToken = getAccessToken();
    const sessionToken = storedToken.sessionToken;
    const url = '/api/sessions/set-require-verification';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
            sessionToken: sessionToken,
            session_id: sessionId,
            require_verification: (!allowUnverifiedUsers)
        },
        success:function(data){
            console.log('Change require_verification success - ' + JSON.stringify(data));
        },
        error:function(data){
            console.log('Change require_verification failed - ' + JSON.stringify(data));
        }
    });
}
function onAllowVoicechatCheckboxChange(){
    const isChecked = document.getElementById('allowVoicechatCheckbox').checked;
    allowVoicechat = isChecked;

    let storedToken = getAccessToken();
    const sessionToken = storedToken.sessionToken;
    const url = '/api/sessions/set-voicechat-enabled';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
            sessionToken: sessionToken,
            session_id: sessionId,
            voicechat_enabled: (allowVoicechat)
        },
        success:function(data){
            console.log('Change voicechat_enabled success - ' + JSON.stringify(data));
        },
        error:function(data){
            console.log('Change voicechat_enabled failed - ' + JSON.stringify(data));
        }
    });
}
function onRequireApprovalCheckboxChange(){
    const isChecked = document.getElementById('requireApprovalCheckbox').checked;
    requireVoiceApproval = isChecked;

    let storedToken = getAccessToken();
    const sessionToken = storedToken.sessionToken;
    const url = '/api/sessions/set-require-voice-approval';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
            sessionToken: sessionToken,
            session_id: sessionId,
            require_voice_approval: (requireVoiceApproval)
        },
        success:function(data){
            console.log('Change require_voice_approval success - ' + JSON.stringify(data));
        },
        error:function(data){
            console.log('Change require_voice_approval failed - ' + JSON.stringify(data));
        }
    });
}
function updateSessionOptionsModal() {
    document.getElementById('allowUnverifiedCheckbox').checked = allowUnverifiedUsers;
    document.getElementById('allowUninvitedCheckbox').checked = allowUninvitedUsers;
}
function updateAdminAskQuizModal() {
    const isMcq = (adminCurrSelectedQuizType === 'MCQ') || (adminCurrSelectedQuizType === 'MULTI_MCQ');
    setElementVisibility('mcqPortion', isMcq);
}
function endSession() {
    let storedToken = getAccessToken();

    //Has logged in storedToken
    if (storedToken) {
        const sessionToken = storedToken.sessionToken;

        const url = '/api/sessions/end-immediately';
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                session_id: sessionId,
                sessionToken: sessionToken,
            },
            success:function(data){
                
            },
            error:function(data){
                addMessage('endSession failed - ' + JSON.stringify(data));
            }
        });
    }
}

//View Generators
function generateNoQuestionsCard(text) {
    let card = '' +
        '<div class="masonry-brick">' +
            '<div class="card text-center">' +
                '<div class="card-body">' +
                    '<h4 class="card-title">' + text + '</h4>' +
                '</div>' +
            '</div>' +
        '</div>';
    return card;
}
function htmlEscape(str) {
    return str
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}
function generateQuestionCard (questionText, username, voteCount, questionId, canUpvote) {
    let card = '' +
        '<div class="masonry-brick">' +
        '<div class="card text-center">';

    if (username) {
        card +=
            '<div class="card-header">' +
            '<span>' + username + '</span>' +
            '</div>';
    }

    card +=
            '<div class="card-body">' +
                '<h4 class="card-title">' + htmlEscape(questionText) + '</h4>';
    if (isAdmin) {
        card +=
            '<ul class="list-group">' +
            '<li class="list-group-item">'  +
            voteCount + ' Votes' +
            '</li>' +
            '</ul>';
    }
    card +=
        '</div>';
    card +=
        '<div class="card-footer text-muted">' +
        '<div class="container-fluid">';

    card += generateUpvoteButton(isAdmin, canUpvote, voteCount, questionId);

    card +=
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return card;
}
function generateUpvoteButton (isAdmin, canUpvote, voteCount, questionId) {
    let card = '';
    if (isAdmin) {
        let buttonOption = (viewingOptions[currentViewingIndex] === 'ANSWERED' ? 'disabled ' : '');
        card += '<div class="row">' +
            '<button class="btn btn-lg btn-block btn-primary" ' +
            buttonOption +
            'onclick="markAnswered(' +
            questionId +
            ')">Mark Answered</button>' +
            '</div>'
    } else {
        let buttonOption = (canUpvote && (viewingOptions[currentViewingIndex] !== 'ANSWERED')
            ? ' onclick="upvote(' + questionId + ')"' : 'disabled');

        card += '<div class="btn-group btn-split-group" data-toggle="buttons">';
        card += '<button type="button" class="btn btn-split-left" ';
        card += buttonOption;
        card += '>+ Vote</button>';

        card += '<button type="button" class="btn btn-split-right"';
        card += buttonOption;
        card += '>' + voteCount + '</button>';
        card += '</div>';
    }
    return card;
}

function markAnswered (questionId) {
    let storedToken = getAccessToken();

    //Has logged in storedToken
    if (storedToken) {
        const sessionToken = storedToken.sessionToken;

        const url = '/api/questions/markAnswered';
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                questionId: questionId,
                session_id: sessionId,
                sessionToken: sessionToken,
            },
            success:function(data){
                console.log('POST Question success! - ' + JSON.stringify(data));
            },
            error:function(data){
                addMessage('POST Question failed - ' + JSON.stringify(data));
            }
        });
    }
}

function upvote (questionId) {
    let storedToken = getAccessToken();
    //Has logged in storedToken
    if (storedToken) {
        const sessionToken = storedToken.sessionToken;

        const url = '/api/questions/' + questionId + '/upvote';
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                questionId: questionId,
                sessionToken: sessionToken,
            },
            success:function(data){
                console.log('POST Question success! - ' + JSON.stringify(data));
            },
            error:function(data){
                addMessage('POST Question failed - ' + JSON.stringify(data));
            }
        });
    }
}
function cycleAskQuestionModalIndex (isRight) {
    let numQuizzes = 0;
    for (let key in adminQuizData) {
        numQuizzes++;
    }
    if (isRight) {
        currAskingQuestionModalIndex++;
        if (currAskingQuestionModalIndex > numQuizzes) {
            currAskingQuestionModalIndex = 0;
        }
    } else {
        currAskingQuestionModalIndex--;
        if (currAskingQuestionModalIndex < 0) {
            currAskingQuestionModalIndex = numQuizzes;
        }
    }
    updateAskQuizModal();
    if (currAskingQuestionModalIndex == numQuizzes)
        document.getElementById('viewQuizAskText').innerHTML = 'New Question';
    else
        document.getElementById('viewQuizAskText').innerHTML = 'Q' + (currAskingQuestionModalIndex + 1);
}
function cycleViewingOption (isRight) {
    if (isRight) {
        currentViewingIndex++;
        if (currentViewingIndex >= viewingOptions.length) {
            currentViewingIndex = 0;
        }
    } else {
        currentViewingIndex--;
        if (currentViewingIndex < 0) {
            currentViewingIndex = viewingOptions.length - 1;
        }
    }
    showCurrViewingQuestions();
    document.getElementById('viewingOptionText').innerHTML = viewingOptions[currentViewingIndex];
}
function cycleViewingQuizResponseIndex (isRight) {
    let numQuizzes = 0;
    for (let key in adminQuizData) {
        numQuizzes++;
    }
    if (isRight) {
        currViewingQuizResponseIndex++;
        if (currViewingQuizResponseIndex >= numQuizzes) {
            currViewingQuizResponseIndex = 0;
        }
    } else {
        currViewingQuizResponseIndex--;
        if (currViewingQuizResponseIndex < 0) {
            currViewingQuizResponseIndex = numQuizzes - 1;
        }
    }
    updateViewQuizModal();
    document.getElementById('viewQuizResponseText').innerHTML = 'Q' + (currViewingQuizResponseIndex + 1);
}

function cycleViewingQuizGroupIndex (isRight) {
    let numQuizzes = 0;
    for (let key in adminQuizData) {
        numQuizzes++;
    }
    if (isRight) {
        currViewingQuizGroupIndex++;
        if (currViewingQuizGroupIndex >= groupIds.length) {
            currViewingQuizGroupIndex = 0;
        }
    } else {
        currViewingQuizGroupIndex--;
        if (currViewingQuizGroupIndex < 0) {
            currViewingQuizGroupIndex = groupIds.length - 1;
        }
    }
    updateViewQuizModal();
}

function getSessionAccessCodeFromUrl() {
    const currentUrl = window.location.href;
    const indexOfLastSlash = currentUrl.lastIndexOf('/')
    const sessionAccessCode = currentUrl.substr(indexOfLastSlash+1, currentUrl.length);
    return sessionAccessCode;
}

//First confirm with server if sessionAccessCode exists
const sessionAccessCode = getSessionAccessCodeFromUrl();
function checkIfSessionExists() {
    $.ajax({
        type: 'GET',
        url: '/api/sessions/exists/' + sessionAccessCode,
        data: {},
        success: function (data) {
            //Set localStorage sessionId
            //TODO: Refactor flow?
            // localStorage.setItem("sessionId", data.id);
            sessionId = data.id;
            sessionEnded = data.has_ended;

            addMessage('[Session Exists! ' + sessionAccessCode + '] ');
            addMessage(JSON.stringify(data));
            authenticate();

            if (!sessionEnded) {
                setSessionBar(data);
                setVoiceModal(data);
            } else {
                let storedToken = localStorage.getItem("qamToken");
                if (storedToken) {        
                    storedToken = JSON.parse(storedToken);
                    const sessionToken = storedToken.sessionToken;
                    $.ajax({
                        type: 'POST',
                        url: '/api/authenticateJwt',
                        data: {
                            sessionToken: sessionToken
                        },
                        success: function (data2) {
                            userId = data2.userId;
                            $.ajax({
                                type: 'POST',
                                url: '/api/sessions/findAdminUserSession',
                                data: {
                                    userId: userId,
                                    sessionId: sessionId
                                },
                                success: function () {
                                    window.location.href = '../closedsession/' + sessionAccessCode;
                                },
                                error: function () {
                                    let message = 'Session (' + data.session_name + ') Has Ended!';
                                    addMessage(message);
                                    setSessionBar(null, message);
                                    socket.close();
                                }
                            })
                        }, error: function (data2) {
                            console.log(data2);
                        }
                    });
                } else {
                    let message = 'Session (' + data.session_name + ') Has Ended!';
                    addMessage(message);
                    setSessionBar(null, message);
                    socket.close();
                }
            }
        },
        error: function (data) {
            let message = 'Session (' + sessionAccessCode + ') Doesn\'t Exist!';
            addMessage(message);
            setSessionBar(null, message);
        }
    });
}

function setBottomBar(){
    if (isAdmin) {
        setElementVisibility('questionBar', false);
        setElementVisibility('adminBar', true);
    } else if (sessionType == "QAM") {
        setElementVisibility('questionBar', true);
        setElementVisibility('adminBar', false);
    } else {
        setElementVisibility('questionBar', false);
        setElementVisibility('adminBar', false);
    }
    if (sessionType != "QAM") {
        let storedToken = getAccessToken();
        const url = '/api/quizzes/getSpecificQuiz';
        if (!isAdmin) {
            $.ajax({
                type: 'POST',
                url: url,
                data:{
                    session_id: sessionId,
                    sessionToken: storedToken.sessionToken
                },
                success:function(data){
                    console.log('POST QuizData success! - ' + JSON.stringify(data));
                    currQuiz = data;
                    updateQuizFormat(sessionType);
                },
                error:function(data){
                    addMessage('POST QuizData failed - ' + JSON.stringify(data));
                }
            });
        }
        else
            updateQuizFormat(sessionType);
    }
}

addOnSuccessfulLoginCallback(function(){
    socket.emit('enterSession', {
        sessionToken: getAccessToken().sessionToken,
        sessionId: sessionId
    });
});

//Calls authenticate() if session does exist
checkIfSessionExists();