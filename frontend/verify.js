authenticate();

//Check localStore if should auto reconnect
function authenticate () {
    const verifyCode = getSessionVerifyCodeFromUrl();
    if (verifyCode) {
        $.ajax({
            type: 'GET',
            url: '/api/verify/' + verifyCode,
            data: {},
            success: function (data) {
                // document.getElementById("verify-box").innerHTML  = 'Email successfully verified.<br><br><br><br><button class="button" onclick="redirect(\'/login\')">LOG IN</button>';
                setElementVisibility('verify-success', true);
                hidePendingBox();
            },
            error: function (data) {
                setElementVisibility('verify-failure', true);
                hidePendingBox();
                setText('verify-failure-message', data.responseText);
                // document.getElementById("verify-box").innerHTML  = data.responseText + '<br><br><br><br><button class="button" onclick="redirect(\'/login\')">LOG IN</button>';
            }
        });
    } else {
        loginWithAnonToken();
    }
}

function hidePendingBox() {
    setElementVisibility('verify-pending', false);
}

function getSessionVerifyCodeFromUrl() {
    const currentUrl = window.location.href;
    const indexOfLastSlash = currentUrl.lastIndexOf('/')
    const sessionAccessCode = currentUrl.substr(indexOfLastSlash+1, currentUrl.length);
    return sessionAccessCode;
}

function redirect (url) {
    window.location.href = url;
}