let onSuccessfulLoginCallbacks = [];

let onSuccessfulAuthLoginCallbacks = [];
let onFailureAuthLoginCallbacks = [];

let authJwtResult;
let failAuthJwtResult;

let coreUsername;
let coreUserId;
let coreAnonymous = true;
let coreVerified = false;

let accessToken;

function onSuccessfulLogin() {
    for (let i in onSuccessfulLoginCallbacks){
        onSuccessfulLoginCallbacks[i]();
    }
}
function addOnSuccessfulLoginCallback(cb) {
    onSuccessfulLoginCallbacks.push(cb);
}

function onSuccessfulAuthLogin() {
    for (let i in onSuccessfulAuthLoginCallbacks){
        onSuccessfulAuthLoginCallbacks[i]();
    }
}
function addOnSuccessfulAuthLoginCallback(cb) {
    onSuccessfulAuthLoginCallbacks.push(cb);
}

function onFailureAuthLogin() {
    console.log('onFailureAuthLogin');
    setElementVisibility('login-button', true);
    // showModal('loginBar');
    for (let i in onFailureAuthLoginCallbacks){
        onFailureAuthLoginCallbacks[i]();
    }
}
function addOnFailureAuthLoginCallback(cb) {
    onFailureAuthLoginCallbacks.push(cb);
}

//Check localStore if should auto reconnect
function authenticate () {
    let storedToken = localStorage.getItem("qamToken");

    //Has logged in storedToken
    if (storedToken) {
        storedToken = JSON.parse(storedToken);
        const sessionToken = storedToken.sessionToken;
        $.ajax({
            type: 'POST',
            url: '/api/authenticateJwt',
            data: {
                sessionToken: sessionToken
            },
            success: function (data) {
                authJwtResult = data;
                coreVerified = data.isVerified;
                coreAnonymous = data.isAnonymous;
                coreUsername = data.username;
                coreUserId = data.userId;

                accessToken = storedToken;

                setUserBar(storedToken.username, storedToken.isAnonymous);
                onSuccessfulLogin();
                onSuccessfulAuthLogin();
            },
            error: function (data) {
                failAuthJwtResult = data;
                onFailureAuthLogin();
                loginWithAnonToken();
            }
        });
    } else {
        onFailureAuthLogin();
        loginWithAnonToken();
    }
}

function loginWithAnonToken() {
    let anonStoredToken = localStorage.getItem("anonQamToken");
    if (anonStoredToken) {
        anonStoredToken = JSON.parse(anonStoredToken);
        const sessionToken = anonStoredToken.sessionToken;
        $.ajax({
            type: 'POST',
            url: '/api/anon',
            data:{
                anonSessionToken: sessionToken
            },
            success:function(data){
                coreVerified = false;
                coreAnonymous = true;
                coreUsername = data.username;
                coreUserId = data.userId;

                accessToken = anonStoredToken;

                setUserBar(anonStoredToken.username, true);
                onSuccessfulLogin();
            },
            error:function(data){
                postAnonUserRequestAndStoreToken();
            }
        });
    } else {
        postAnonUserRequestAndStoreToken();
    }
}

function postAnonUserRequestAndStoreToken() {
    $.ajax({
        type: 'POST',
        url: '/api/anon',
        data:{
        },
        success:function(data){
            coreVerified = false;
            coreAnonymous = true;
            coreUsername = data.username;
            coreUserId = data.userId;

            const anonStoredToken = {
                username: data.username,
                userId: data.userId,
                isAnonymous: data.isAnonymous,
                sessionToken: data.sessionToken
            };
            localStorage.setItem("anonQamToken", JSON.stringify(anonStoredToken));
            accessToken = anonStoredToken;

            setUserBar(anonStoredToken.username, true);
            onSuccessfulLogin();
        },
        error:function(data){
            console.log ('Anon user request failed - ' + JSON.stringify(data));
        }
    });
}

function setUserBar() {
    const loginBar = document.getElementById('loginBar');
    const userStr = '👤 ' + coreUsername + ((coreAnonymous) ? ' (Anonymous)' : '');
    let text = document.createTextNode(userStr);

    if (coreAnonymous) {
        setElementVisibility('account-button', false);
    }

    if (!coreAnonymous) {
        setElementVisibility('account-button', true);
        
        setText('login-button', 'Logout');
    }
    loginBar.appendChild(text);
}

function loginLogoutButtonPressed() {
    if (coreAnonymous) {
        showModal('loginModal');
    } else {
        localStorage.setItem("qamToken", "");
        accessToken = '';
        window.location = "/";
    }
}

function loginLogoutButtonPressed() {
    if (coreAnonymous) {
        showModal('loginModal');
    } else {
        localStorage.setItem("qamToken", "");
        accessToken = '';
        window.location = "/";
    }
}

function accountButtonPressed() {
    if (!coreAnonymous) {
        window.location = "/account-details.html";
    }
}

function getAccessToken() {
    return accessToken;
}

function revokeAuthAndRefresh() {
    console.log('Auth revoked, page refreshing!');
    localStorage.setItem("anonQamToken", '');
    localStorage.setItem("qamToken", '');
    location.reload();
}