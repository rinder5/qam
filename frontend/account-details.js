let notLoggedIn = true;
let currentUsername;
let currentEmail;
let userId;

function redirect (url) {
    window.location.href = url;
}

$('#editDetailsForm').submit(function(event) {
    event.preventDefault();
    editSubmit();
    return false;
});

function editSubmit() {
    return new Promise(function(fulfill, reject) {
        setElementVisibility('warningEditMessageRegister', false);
        const newUsername = document.getElementById("userName").value;
        const newPassword = document.getElementById("userPassword").value;
        const newEmail = document.getElementById("userEmail").value;
        document.getElementById("userPassword").value = '';

        if (!newUsername || !newEmail) {
            addMessage('Please enter a valid username/email');
            document.getElementById("editMessage").innerHTML  = 'Invalid Username/Email';
            return false;
        }

        let storedToken = localStorage.getItem("qamToken");
        //Has logged in storedToken
        if (storedToken) {
            storedToken = JSON.parse(storedToken);
            const sessionToken = storedToken.sessionToken;
            $.ajax({
                type: 'POST',
                url: '/api/authenticateJwt',
                data: {
                    sessionToken: sessionToken
                },
                success: function (data) {
                    $.ajax({
                        type: 'POST',
                        url: '/api/editUser',
                        data:{
                            userId: data.userId,
                            currentUsername: data.username,
                            currentEmail: data.email,
                            newUsername: newUsername,
                            newEmail: newEmail,
                            newPassword: newPassword,
                            user_type: "ADMIN"
                        },
                        success:function(data2){
                            addMessage('POST User success! - ' + JSON.stringify(data));
                
                            const qamToken = {
                                username: data2.username,
                                userId: data2.userId,
                                isAnonymous: data2.isAnonymous,
                                email: data2.email,
                                sessionId: 1,
                                sessionToken: data2.sessionToken
                            };
                            localStorage.setItem("qamToken", JSON.stringify(qamToken));
                
                            setElementVisibility('editSuccessMessageRegister', true);
                            setText('editSuccessMessage', "Successfully Updated!");
                            currentUsername = newUsername;
                            currentEmail = newEmail;
                            disableForm();
                            window.setTimeout(function(){location.reload()}, 2000);
                        },
                        error:function(data){
                            setElementVisibility('warningEditMessageRegister', true);
                            setText('editMessage', "Uh oh!? " + data.responseText);
                        }
                    });
                }
            });
        }
    fulfill();
    })
}

function getDetails() {
    return new Promise(function(fulfill, reject) {
        let storedToken = localStorage.getItem("qamToken");
        //Has logged in storedToken
        if (storedToken) {
            storedToken = JSON.parse(storedToken);
            const sessionToken = storedToken.sessionToken;
            $.ajax({
                type: 'POST',
                url: '/api/authenticateJwt',
                data: {
                    sessionToken: sessionToken
                },
                success: function (data) {
                    userId = data.userId;
                    currentUsername = data.username;
                    currentEmail = data.email;

                    disableForm();
                    if (data.isAnonymous) {
                        document.getElementById('editDetailsBtn').hidden = true;
                    }
                },
                error: function() {
                    disableForm();
                    document.getElementById('editDetailsBtn').hidden = true;
                }
            });
        }
        else {
            disableForm();
            document.getElementById('editDetailsBtn').hidden = true;
        }
        fulfill();
    })
}

function addMessage(message) {
    console.log(message);
}

function disableForm() {
    document.getElementById("userName").value = currentUsername;
    document.getElementById("userName").disabled = true;
    document.getElementById("userEmail").value = currentEmail;
    document.getElementById("userEmail").disabled = true;
    document.getElementById("userPassword").value = "";
    document.getElementById("userPassword").hidden = true;
    document.getElementById('cancelEditDetailsBtn').hidden = true;
    document.getElementById('editDetailsSubmitBtn').hidden = true;
    document.getElementById('editDetailsBtn').hidden = false;
    setElementVisibility('warningEditMessageRegister', false);
}

function enableForm() {
    document.getElementById("userName").disabled = false;
    document.getElementById("userEmail").disabled = false;
    document.getElementById("userPassword").hidden = false;
    document.getElementById('cancelEditDetailsBtn').hidden = false;
    document.getElementById('editDetailsSubmitBtn').hidden = false;
    document.getElementById('editDetailsBtn').hidden = true;
}

getDetails();
authenticate();