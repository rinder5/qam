$('.message a').click(function(){
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});

$('#registerForm').submit(function(event) {
    event.preventDefault();
    registerSubmit();
    return false;
});
$('#loginForm').submit(function() {
    loginSubmit();
    return false;
});
$('#forgotPasswordForm').submit(function() {
    forgotPasswordSubmit();
    return false;
});

function registerSubmit() {
    addMessage('Creating account... please wait');

    const username = document.getElementById("regUsername").value;
    const password = document.getElementById("regPassword").value;
    const email = document.getElementById("regEmail").value;
    document.getElementById("regUsername").value = '';
    document.getElementById("regPassword").value = '';
    document.getElementById("regEmail").value = '';

    if (!username || !password || !email) {
        addMessage('Please enter a valid username/password/email');
        document.getElementById("regMessage").innerHTML  = 'Invalid Username/Password';
        return false;
    }

    $.ajax({
        type: 'POST',
        url: '/api/users',
        data:{
            username: username,
            password: password,
            email: email,
            user_type: "ADMIN"
        },
        success:function(data){
            addMessage('POST User success! - ' + JSON.stringify(data));

            const qamToken = {
                username: data.username,
                userId: data.userId,
                isAnonymous: data.isAnonymous,
                email: data.email,
                sessionId: 1,
                sessionToken: data.sessionToken
            };
            localStorage.setItem("qamToken", JSON.stringify(qamToken));

            location.reload();
        },
        error:function(data){
            setElementVisibility('warningMessageRegister', true);
            setText('registerMessage', "Uh oh!? " + data.responseText);
        }
    });
}
function loginSubmit() {
    localStorage.setItem("qamToken", '');
    $.ajax({
        type: 'POST',
        url: '/api/login',
        data:$('#loginForm').serialize(),
        success:function(data){
            const qamToken = {
                username: data.username,
                userId: data.userId,
                isAnonymous: data.isAnonymous,
                email: data.email,
                sessionId: 1,
                sessionToken: data.sessionToken
            };
            localStorage.setItem("qamToken", JSON.stringify(qamToken));
            addMessage('Token = ' + JSON.stringify(data.sessionToken));
            // authenticate();
            location.reload();
        },
        error:function(data){
            setElementVisibility('warningMessage', true);
            setText('loginMessage', "Uh oh!? Invalid login credentials");
        }
    });
    return false;
}
function forgotPasswordSubmit() {
    localStorage.setItem("qamToken", '');
    const email = document.getElementById("forgotPasswordEmail").value;
    document.getElementById("forgotPasswordEmail").value = '';
    $.ajax({
        type: 'POST',
        url: '/api/reset-password',
        data:{
            email: email
        },
        success:function(data){
            setElementVisibility('forgotPasswordWarningMessage', true);
            setText('forgotPasswordMessage', "Woohoo! If the email is valid, a new password will be sent shortly!");
        },
        error:function(data){
            setElementVisibility('forgotPasswordWarningMessage', true);
            setText('forgotPasswordMessage', "Error submitting reset password request!");
        }
    });
    return false;
}