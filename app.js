'use strict';
/**
 * QAM API server.
 * This is the main application.
 */

const express = require('express');
const app = express();

const config = require('./config/config.js');
const bodyParser = require('body-parser');
const path = require('path');
const urlencodedParser = bodyParser.urlencoded({extended : false});

const passport = require('passport');
const masterController = require('./controllers/masterController');
masterController.injectDependencies();
const socketManager = require('./controllers/socketManager');

const cronManager = require('./controllers/cronManager');
cronManager.start();

app.set('port', process.env.PORT || config.port);

//Parsing application as JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

//Passport middleware
app.use(require('serve-static')(__dirname + '/../../public'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());

//Frontend and static routes
app.use(require('./routes/frontend'));
app.use(express.static(path.join(__dirname, 'frontend')));

//User routes
app.use(require('./routes/users'));

//Session routes
app.use(require('./routes/sessions'));

//Question routes
app.use(require('./routes/questions'));

//Quiz routes
app.use(require('./routes/quizzes'));

//Socket.io
const io = require('socket.io').listen(
    //Startup listen sequence
    app.listen(app.get('port'), function() {
        console.log("QAM API server has started running on port " + app.get('port'));
    })
);
//Initialize socketManager with socket.io object
socketManager.initialize(io);

module.exports = app;
